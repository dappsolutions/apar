import 'dart:convert';
import 'dart:io';

import 'package:apar/app/modules/daftarapar/models/apar.models.dart';
import 'package:apar/app/modules/daftarapar/models/aparfull.models.dart';
import 'package:apar/app/modules/daftarapar/models/oustandingapar.models.dart';
import 'package:apar/app/modules/daftarapar/services/daftarapar.services.dart';
import 'package:apar/config/api.config.dart';
import 'package:apar/config/modules.config.dart';
import 'package:apar/config/routes.config.dart';
import 'package:apar/config/session.config.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:syncfusion_flutter_xlsio/xlsio.dart' hide Column, Alignment;

class DaftarAparControllers extends GetxController {
  List<AparModels> dataDocument = [];
  List<OustandingAparModels> dataDocumentOustanding = [];
  List<AparFullModels> dataFullDocument = [];
  bool loadingProses = false;
  String tanggal_start = "";
  String tanggal_end = "";

  Future getUserIdSession() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    SessionConfig.user_id = pref.getString("user_id")!;
    SessionConfig.upt_id = pref.getString("upt_id")!;
    SessionConfig.wilayah = pref.getString("wilayah")!;
    SessionConfig.approval_id = pref.getString("approval_id")!;
    this.update();
  }

  Future getData() async {
    this.loadingProses = true;
    update();

    await getUserIdSession();

    Map<String, dynamic> params = {};
    params["last_id"] = "";
    params["user_id"] = SessionConfig.user_id;
    params["upt_id"] = SessionConfig.upt_id;
    params["wilayah"] = SessionConfig.wilayah;

    List<AparModels> data =
        await DaftarAparServices.getData(params, this.dataDocument);
    this.dataDocument = data;
    this.loadingProses = false;
    this.update();
  }
  
  Future getDataOutstadingApproval() async {
    this.loadingProses = true;
    update();

    await getUserIdSession();

    Map<String, dynamic> params = {};
    params["last_id"] = "";
    params["user_id"] = SessionConfig.user_id;
    params["upt_id"] = SessionConfig.upt_id;
    params["wilayah"] = SessionConfig.wilayah;
    params["approval_id"] = SessionConfig.approval_id;

    List<OustandingAparModels> data =
        await DaftarAparServices.getDataOutstadingApproval(params, this.dataDocumentOustanding);
    this.dataDocumentOustanding = data;
    this.loadingProses = false;
    this.update();
  }

  Future getFullData() async {
    this.loadingProses = true;
    update();

    await getUserIdSession();

    Map<String, dynamic> params = {};
    params["last_id"] = "";
    params["user_id"] = SessionConfig.user_id;
    params["upt_id"] = SessionConfig.upt_id;
    params["tanggal_awal"] = this.tanggal_start;
    params["tanggal_akhir"] = this.tanggal_end;
    params["wilayah"] = SessionConfig.wilayah;

    List<AparFullModels> data =
        await DaftarAparServices.getFullData(params, this.dataFullDocument);
    this.dataFullDocument = data;
    this.loadingProses = false;
    this.update();
  }

  Future getFullDataRab() async {
    this.loadingProses = true;
    update();

    await getUserIdSession();

    Map<String, dynamic> params = {};
    params["last_id"] = "";
    params["user_id"] = SessionConfig.user_id;
    params["upt_id"] = SessionConfig.upt_id;
    params["tanggal_awal"] = this.tanggal_start;
    params["tanggal_akhir"] = this.tanggal_end;
    params["wilayah"] = SessionConfig.wilayah;

    List<AparFullModels> data =
        await DaftarAparServices.getFullDataRab(params, this.dataFullDocument);
    this.dataFullDocument = data;
    this.loadingProses = false;
    this.update();
  }

  Future getDataLoadMore() async {
    this.loadingProses = true;
    update();

    await getUserIdSession();

    Map<String, dynamic> params = {};
    params["last_id"] = this.dataDocument.length > 0
        ? this.dataDocument[this.dataDocument.length - 1].id
        : "";
    params["user_id"] = SessionConfig.user_id;
    params["upt_id"] = SessionConfig.upt_id;
    params["tanggal_awal"] = this.tanggal_start;
    params["tanggal_akhir"] = this.tanggal_end;
    params["wilayah"] = SessionConfig.wilayah;
    List<AparModels> data =
        await DaftarAparServices.getDataLoadMore(params, this.dataDocument);
    this.dataDocument = data;
    this.loadingProses = false;
    this.update();
  }

  Future getDataLoadMoreFull() async {
    this.loadingProses = true;
    update();

    await getUserIdSession();

    Map<String, dynamic> params = {};
    params["last_id"] = this.dataFullDocument.length > 0
        ? this.dataFullDocument[this.dataFullDocument.length - 1].id
        : "";
    params["user_id"] = SessionConfig.user_id;
    params["upt_id"] = SessionConfig.upt_id;
    params["wilayah"] = SessionConfig.wilayah;
    params["tanggal_awal"] = this.tanggal_start;
    params["tanggal_akhir"] = this.tanggal_end;
    List<AparFullModels> data =
        await DaftarAparServices.getDataLoadMoreFull(params, dataFullDocument);
    this.dataFullDocument = data;
    this.loadingProses = false;
    this.update();
  }

  Future getDataLoadMoreFullRab() async {
    this.loadingProses = true;
    update();

    await getUserIdSession();

    Map<String, dynamic> params = {};
    params["last_id"] = this.dataFullDocument.length > 0
        ? this.dataFullDocument[this.dataFullDocument.length - 1].id
        : "";
    params["user_id"] = SessionConfig.user_id;
    params["upt_id"] = SessionConfig.upt_id;
    params["wilayah"] = SessionConfig.wilayah;
    List<AparFullModels> data = await DaftarAparServices.getDataLoadMoreFullRab(
        params, dataFullDocument);
    this.dataFullDocument = data;
    this.loadingProses = false;
    this.update();
  }

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    print(directory.path);
    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/counter.txt');
  }

  void selectedDate(DateRangePickerSelectionChangedArgs args) {
    /// The argument value will return the changed date as [DateTime] when the
    /// widget [SfDateRangeSelectionMode] set as single.
    ///
    /// The argument value will return the changed dates as [List<DateTime>]
    /// when the widget [SfDateRangeSelectionMode] set as multiple.
    ///
    /// The argument value will return the changed range as [PickerDateRange]
    /// when the widget [SfDateRangeSelectionMode] set as range.
    ///
    /// The argument value will return the changed ranges as
    /// [List<PickerDateRange] when the widget [SfDateRangeSelectionMode] set as
    /// multi range.
    if (args.value is PickerDateRange) {
      this.tanggal_start = args.value.startDate.toString();
      this.tanggal_end = args.value.endDate.toString();
    }
    this.update();
  }

  Future<void> generateExcel() async {
    //Create a Excel document.

    //Creating a workbook.
    final Workbook workbook = Workbook();
    //Accessing via index
    final Worksheet sheet = workbook.worksheets[0];
    //Creating a new style with all properties.
    final Style style = workbook.styles.add('Style1');
    // set back color by hexa decimal.
    style.backColor = '#00a2b9';
    // set font size.
    style.fontSize = 14;
    // set horizontal alignment.
    style.hAlign = HAlignType.center;
    // set veritical alignment.
    style.vAlign = VAlignType.bottom;
    // set indent value.
    style.indent = 1;
    // set top bordera line style.
    style.borders.top.lineStyle = LineStyle.double;
    // set right bordera line style.
    style.borders.right.lineStyle = LineStyle.thin;
    // set wrap text.
    style.wrapText = true;
    // set number format to cell.
    style.numberFormat = '_(\$* #,##0_)';

    final Style styleContent = workbook.styles.add('Style2');
    // set font size.
    styleContent.fontSize = 14;
    // set horizontal alignment.
    styleContent.hAlign = HAlignType.center;
    // set veritical alignment.
    styleContent.vAlign = VAlignType.bottom;
    // set indent value.
    styleContent.indent = 1;
    // set top bordera line styleContent.
    styleContent.borders.top.lineStyle = LineStyle.double;
    // set right bordera line styleContent.
    styleContent.borders.right.lineStyle = LineStyle.thin;
    // set wrap text.
    styleContent.wrapText = true;
    // set number format to cell.
    styleContent.numberFormat = '_(\$* #,##0_)';

// Add style.
// workbook.styles.addStyle(style);

    sheet.getRangeByName("A1").setText("No");
    // sheet.getRangeByName("A1").cellStyle.backColor = "#00a2b9";
    sheet.getRangeByName("A1").cellStyle = style;
    sheet.getRangeByName("B1").setText("No Apar");
    sheet.getRangeByName("B1").cellStyle = style;
    sheet.getRangeByName("C1").setText("Lokasi");
    sheet.getRangeByName("C1").cellStyle = style;
    sheet.getRangeByName("D1").setText("Lokasi Penempatan");
    sheet.getRangeByName("D1").cellStyle = style;
    sheet.getRangeByName("E1").setText("Jenis");
    sheet.getRangeByName("E1").cellStyle = style;
    sheet.getRangeByName("F1").setText("Merk/Tipe");
    sheet.getRangeByName("F1").cellStyle = style;
    sheet.getRangeByName("G1").setText("Berat");
    sheet.getRangeByName("G1").cellStyle = style;
    sheet.getRangeByName("H1").setText("Status");
    sheet.getRangeByName("H1").cellStyle = style;
    sheet.getRangeByName("I1").setText("Tgl/Bln/Tahun");
    sheet.getRangeByName("I1").cellStyle = style;
    sheet.getRangeByName("J1").setText("Tgl/Bln/Tahun");
    sheet.getRangeByName("J1").cellStyle = style;

    int counter = 2;
    int numberData = 1;
    for (int i = 0; i < this.dataFullDocument.length; i++) {
      sheet.getRangeByName("A${counter}").setText(numberData.toString());
      sheet.getRangeByName("A${counter}").cellStyle = styleContent;
      sheet
          .getRangeByName("B${counter}")
          .setText(this.dataFullDocument[i].no_alat);
      sheet.getRangeByName("B${counter}").cellStyle = styleContent;
      sheet
          .getRangeByName("C${counter}")
          .setText(this.dataFullDocument[i].nama_lokasi);
      sheet.getRangeByName("C${counter}").cellStyle = styleContent;
      sheet
          .getRangeByName("D${counter}")
          .setText(this.dataFullDocument[i].tempat);
      sheet.getRangeByName("D${counter}").cellStyle = styleContent;
      sheet
          .getRangeByName("E${counter}")
          .setText(this.dataFullDocument[i].jenis);
      sheet.getRangeByName("E${counter}").cellStyle = styleContent;
      sheet
          .getRangeByName("F${counter}")
          .setText(this.dataFullDocument[i].nama_merk);
      sheet.getRangeByName("F${counter}").cellStyle = styleContent;
      sheet
          .getRangeByName("G${counter}")
          .setText(this.dataFullDocument[i].berat);
      sheet.getRangeByName("G${counter}").cellStyle = styleContent;
      sheet
          .getRangeByName("H${counter}")
          .setText(this.dataFullDocument[i].tekanan);
      sheet.getRangeByName("H${counter}").cellStyle = styleContent;
      sheet
          .getRangeByName("I${counter}")
          .setText(this.dataFullDocument[i].tanggal);
      sheet.getRangeByName("I${counter}").cellStyle = styleContent;
      sheet
          .getRangeByName("J${counter}")
          .setText(this.dataFullDocument[i].tanggal);
      sheet.getRangeByName("J${counter}").cellStyle = styleContent;

      counter += 1;
      numberData += 1;
    }

    // Enable calculation for worksheet.
    // sheet.enableSheetCalculations();

    //Set data in the worksheet.
    // sheet.getRangeByName('A1').columnWidth = 4.82;
    // sheet.getRangeByName('B1:C1').columnWidth = 13.82;
    // sheet.getRangeByName('D1').columnWidth = 13.20;
    // sheet.getRangeByName('E1').columnWidth = 7.50;
    // sheet.getRangeByName('F1').columnWidth = 9.73;
    // sheet.getRangeByName('G1').columnWidth = 8.82;
    // sheet.getRangeByName('H1').columnWidth = 4.46;

    // sheet.getRangeByName('A1:H1').cellStyle.backColor = '#333F4F';
    // sheet.getRangeByName('A1:H1').merge();
    // sheet.getRangeByName('B4:D6').merge();

    // sheet.getRangeByName('B4').setText('Invoice');
    // sheet.getRangeByName('B4').cellStyle.fontSize = 32;

    // sheet.getRangeByName('B8').setText('BILL TO:');
    // sheet.getRangeByName('B8').cellStyle.fontSize = 9;
    // sheet.getRangeByName('B8').cellStyle.bold = true;

    // sheet.getRangeByName('B9').setText('Abraham Swearegin');
    // sheet.getRangeByName('B9').cellStyle.fontSize = 12;

    // sheet
    //     .getRangeByName('B10')
    //     .setText('United States, California, San Mateo,');
    // sheet.getRangeByName('B10').cellStyle.fontSize = 9;

    // sheet.getRangeByName('B11').setText('9920 BridgePointe Parkway,');
    // sheet.getRangeByName('B11').cellStyle.fontSize = 9;

    // sheet.getRangeByName('B12').setNumber(9365550136);
    // sheet.getRangeByName('B12').cellStyle.fontSize = 9;
    // sheet.getRangeByName('B12').cellStyle.hAlign = HAlignType.left;

    // final Range range1 = sheet.getRangeByName('F8:G8');
    // final Range range2 = sheet.getRangeByName('F9:G9');
    // final Range range3 = sheet.getRangeByName('F10:G10');
    // final Range range4 = sheet.getRangeByName('F11:G11');
    // final Range range5 = sheet.getRangeByName('F12:G12');

    // range1.merge();
    // range2.merge();
    // range3.merge();
    // range4.merge();
    // range5.merge();

    // sheet.getRangeByName('F8').setText('INVOICE#');
    // range1.cellStyle.fontSize = 8;
    // range1.cellStyle.bold = true;
    // range1.cellStyle.hAlign = HAlignType.right;

    // sheet.getRangeByName('F9').setNumber(2058557939);
    // range2.cellStyle.fontSize = 9;
    // range2.cellStyle.hAlign = HAlignType.right;

    // sheet.getRangeByName('F10').setText('DATE');
    // range3.cellStyle.fontSize = 8;
    // range3.cellStyle.bold = true;
    // range3.cellStyle.hAlign = HAlignType.right;

    // sheet.getRangeByName('F11').dateTime = DateTime(2020, 08, 31);
    // sheet.getRangeByName('F11').numberFormat =
    //     r'[$-x-sysdate]dddd, mmmm dd, yyyy';
    // range4.cellStyle.fontSize = 9;
    // range4.cellStyle.hAlign = HAlignType.right;

    // range5.cellStyle.fontSize = 8;
    // range5.cellStyle.bold = true;
    // range5.cellStyle.hAlign = HAlignType.right;

    // final Range range6 = sheet.getRangeByName('B15:G15');
    // range6.cellStyle.fontSize = 10;
    // range6.cellStyle.bold = true;

    // sheet.getRangeByIndex(15, 2).setText('Code');
    // sheet.getRangeByIndex(16, 2).setText('CA-1098');
    // sheet.getRangeByIndex(17, 2).setText('LJ-0192');
    // sheet.getRangeByIndex(18, 2).setText('So-B909-M');
    // sheet.getRangeByIndex(19, 2).setText('FK-5136');
    // sheet.getRangeByIndex(20, 2).setText('HL-U509');

    // sheet.getRangeByIndex(15, 3).setText('Description');
    // sheet.getRangeByIndex(16, 3).setText('AWC Logo Cap');
    // sheet.getRangeByIndex(17, 3).setText('Long-Sleeve Logo Jersey, M');
    // sheet.getRangeByIndex(18, 3).setText('Mountain Bike Socks, M');
    // sheet.getRangeByIndex(19, 3).setText('ML Fork');
    // sheet.getRangeByIndex(20, 3).setText('Sports-100 Helmet, Black');

    // sheet.getRangeByIndex(15, 3, 15, 4).merge();
    // sheet.getRangeByIndex(16, 3, 16, 4).merge();
    // sheet.getRangeByIndex(17, 3, 17, 4).merge();
    // sheet.getRangeByIndex(18, 3, 18, 4).merge();
    // sheet.getRangeByIndex(19, 3, 19, 4).merge();
    // sheet.getRangeByIndex(20, 3, 20, 4).merge();

    // sheet.getRangeByIndex(15, 5).setText('Quantity');
    // sheet.getRangeByIndex(16, 5).setNumber(2);
    // sheet.getRangeByIndex(17, 5).setNumber(3);
    // sheet.getRangeByIndex(18, 5).setNumber(2);
    // sheet.getRangeByIndex(19, 5).setNumber(6);
    // sheet.getRangeByIndex(20, 5).setNumber(1);

    // sheet.getRangeByIndex(15, 6).setText('Price');
    // sheet.getRangeByIndex(16, 6).setNumber(8.99);
    // sheet.getRangeByIndex(17, 6).setNumber(49.99);
    // sheet.getRangeByIndex(18, 6).setNumber(9.50);
    // sheet.getRangeByIndex(19, 6).setNumber(175.49);
    // sheet.getRangeByIndex(20, 6).setNumber(34.99);

    // sheet.getRangeByIndex(15, 7).setText('Total');
    // sheet.getRangeByIndex(16, 7).setFormula('=E16*F16+(E16*F16)');
    // sheet.getRangeByIndex(17, 7).setFormula('=E17*F17+(E17*F17)');
    // sheet.getRangeByIndex(18, 7).setFormula('=E18*F18+(E18*F18)');
    // sheet.getRangeByIndex(19, 7).setFormula('=E19*F19+(E19*F19)');
    // sheet.getRangeByIndex(20, 7).setFormula('=E20*F20+(E20*F20)');
    // sheet.getRangeByIndex(15, 6, 20, 7).numberFormat = r'$#,##0.00';

    // sheet.getRangeByName('E15:G15').cellStyle.hAlign = HAlignType.right;
    // sheet.getRangeByName('B15:G15').cellStyle.fontSize = 10;
    // sheet.getRangeByName('B15:G15').cellStyle.bold = true;
    // sheet.getRangeByName('B16:G20').cellStyle.fontSize = 9;

    // sheet.getRangeByName('E22:G22').merge();
    // sheet.getRangeByName('E22:G22').cellStyle.hAlign = HAlignType.right;
    // sheet.getRangeByName('E23:G24').merge();

    // final Range range7 = sheet.getRangeByName('E22');
    // final Range range8 = sheet.getRangeByName('E23');
    // range7.setText('TOTAL');
    // range7.cellStyle.fontSize = 8;
    // range8.setFormula('=SUM(G16:G20)');
    // range8.numberFormat = r'$#,##0.00';
    // range8.cellStyle.fontSize = 24;
    // range8.cellStyle.hAlign = HAlignType.right;
    // range8.cellStyle.bold = true;

    // sheet.getRangeByIndex(26, 1).text =
    //     '800 Interchange Blvd, Suite 2501, Austin, TX 78721 | support@adventure-works.com';
    // sheet.getRangeByIndex(26, 1).cellStyle.fontSize = 8;

    // final Range range9 = sheet.getRangeByName('A26:H27');
    // range9.cellStyle.backColor = '#ACB9CA';
    // range9.merge();
    // range9.cellStyle.hAlign = HAlignType.center;
    // range9.cellStyle.vAlign = VAlignType.center;

    //Save and launch the excel.
    final List<int> bytes = workbook.saveAsStream();
    workbook.dispose();

    final path = await _localPath;
    final String filename = '${path}/DataApar.xlsx';
    File file = File(filename);
    await file.writeAsBytes(bytes);

    OpenFile.open(filename);
    //Dispose the document.

    //Save and launch the file.
    // await saveAndLaunchFile(bytes, 'Invoice.xlsx');
  }

  Future<void> generateExcelRab() async {
    //Create a Excel document.

    //Creating a workbook.
    final Workbook workbook = Workbook();
    //Accessing via index
    final Worksheet sheet = workbook.worksheets[0];
    //Creating a new style with all properties.
    final Style style = workbook.styles.add('Style1');
    // set back color by hexa decimal.
    style.backColor = '#00a2b9';
    // set font size.
    style.fontSize = 14;
    // set horizontal alignment.
    style.hAlign = HAlignType.center;
    // set veritical alignment.
    style.vAlign = VAlignType.bottom;
    // set indent value.
    style.indent = 1;
    // set top bordera line style.
    style.borders.top.lineStyle = LineStyle.double;
    // set right bordera line style.
    style.borders.right.lineStyle = LineStyle.thin;
    // set wrap text.
    style.wrapText = true;
    // set number format to cell.
    style.numberFormat = '_(\$* #,##0_)';

    final Style styleContent = workbook.styles.add('Style2');
    // set font size.
    styleContent.fontSize = 14;
    // set horizontal alignment.
    styleContent.hAlign = HAlignType.center;
    // set veritical alignment.
    styleContent.vAlign = VAlignType.bottom;
    // set indent value.
    styleContent.indent = 1;
    // set top bordera line styleContent.
    styleContent.borders.top.lineStyle = LineStyle.double;
    // set right bordera line styleContent.
    styleContent.borders.right.lineStyle = LineStyle.thin;
    // set wrap text.
    styleContent.wrapText = true;
    // set number format to cell.
    styleContent.numberFormat = '_(\$* #,##0_)';

// Add style.
// workbook.styles.addStyle(style);

    sheet.getRangeByName("C1:H1").setText("RENCANA ANGGARAN BIAYA (RAB)");
    sheet.getRangeByName("C1:H1").merge();
    sheet.getRangeByName("F2").setText("Nomor");
    sheet.getRangeByName("G2").setText(":");
    sheet.getRangeByName("F3").setText("Tanggal");
    sheet.getRangeByName("G3").setText(":");

    sheet.getRangeByName("A4").setText("Nama Pekerjaan");
    sheet.getRangeByName("B4").setText(":");
    sheet.getRangeByName("A5").setText("Pos Anggaran 53");
    sheet.getRangeByName("B5").setText(": Rutin");
    sheet.getRangeByName("F5").setText("Tahun");
    sheet.getRangeByName("G5").setText(":");

    sheet.getRangeByName("A7").setText("No");
    sheet.getRangeByName("B7").setText("NAMA BARANG");
    sheet.getRangeByName("C7:H7").setText("PENGAJUAN");
    sheet.getRangeByName("C7:H7").merge();
    sheet.getRangeByName("J7:K7").setText("DIEVALUASI");
    sheet.getRangeByName("J7:K7").merge();

    sheet.getRangeByName("C8").setText("Satuan");
    sheet.getRangeByName("D8").setText("Kebutuhan");
    sheet.getRangeByName("E8").setText("Persediaan");
    sheet.getRangeByName("F8").setText("Volume");
    sheet.getRangeByName("G8").setText("Harga Satuan (Rp)");
    sheet.getRangeByName("H8").setText("Jumlah (Rp)");

    sheet.getRangeByName("I9").setText("Volume");
    sheet.getRangeByName("J9").setText("Harga Satuan (Rp)");
    sheet.getRangeByName("K9").setText("Jumlah (Rp)");

    sheet.getRangeByName("A10").setText("1");
    sheet.getRangeByName("B10").setText("2");
    sheet.getRangeByName("C10").setText("3");
    sheet.getRangeByName("D10").setText("4");
    sheet.getRangeByName("E10").setText("5");
    sheet.getRangeByName("F10").setText("6");
    sheet.getRangeByName("G10").setText("7");
    sheet.getRangeByName("H10").setText("8 = 6 x 7");
    sheet.getRangeByName("I10").setText("9");
    sheet.getRangeByName("J10").setText("10");
    sheet.getRangeByName("K10").setText("11 = 9 x 10");

    sheet.getRangeByName("A11").setText("I");
    sheet.getRangeByName("B11").setText("Material");

    sheet.getRangeByName("A15").setText("II");
    sheet.getRangeByName("B15").setText("Jasa");

    int counterIsian = 16;
    int numberData = 1;
    List<String> dataTemp = [];
    for (int i = 0; i < this.dataFullDocument.length; i++) {
      String foreignKey = this.dataFullDocument[i].nama_lokasi;
      if (dataTemp.contains(foreignKey) == false) {
        // print('foreignKey ${foreignKey}');
        String pathNum = "";
        if (dataTemp.contains(foreignKey) == false ||
            dataTemp.isEmpty == true) {
          print("MASUK path");
          pathNum = "II.";
        }
        sheet
            .getRangeByName("A${counterIsian}")
            .setText("${pathNum}${numberData.toString()}");
        sheet.getRangeByName("B${counterIsian}").setText("${foreignKey}");
        //get detail item penempatan by lokasi
        int numberListTempat = 1;
        for (int x = 0; x < this.dataFullDocument.length; x++) {
          String namaLokasi = this.dataFullDocument[x].nama_lokasi;
          if (namaLokasi == foreignKey) {
            counterIsian += 1;
            sheet
                .getRangeByName("A${counterIsian}")
                .setText("${numberListTempat.toString()}");
            sheet
                .getRangeByName("B${counterIsian}")
                .setText("${this.dataFullDocument[x].tempat}");
            sheet.getRangeByName("C${counterIsian}").setText("kg");
            sheet
                .getRangeByName("D${counterIsian}")
                .setText("${this.dataFullDocument[x].berat}");
            sheet
                .getRangeByName("E${counterIsian}")
                .setText("${this.dataFullDocument[x].nama_merk}");
            sheet
                .getRangeByName("F${counterIsian}")
                .setText("${this.dataFullDocument[x].berat}");
            numberListTempat += 1;
          }
        }
        dataTemp.add(foreignKey);
        counterIsian += 1;
        numberData += 1;
      }
    }

    int counter = counterIsian + 1;
    // int numberData = 1;
    sheet.getRangeByName("A${counter}").setText("Jumlah");
    counter += 1;
    sheet.getRangeByName("A${counter}").setText("Pembulatan");
    counter += 1;
    sheet.getRangeByName("A${counter}").setText("PPN 10%");
    counter += 1;
    sheet.getRangeByName("A${counter}").setText("Jumlah total");
    counter += 1;
    sheet.getRangeByName("A${counter}").setText("Terbilang    : ");
    sheet.getRangeByName("K${counter}").setText("Terbilang");
    counter += 2;

    sheet.getRangeByName("D${counter}").setText("Setuju / Tidak Setuju");
    sheet.getRangeByName("H${counter}").setText("Diusulkan oleh :");
    sheet.getRangeByName("K${counter}").setText("Dievaluasi oleh :");
    counter += 1;
    sheet.getRangeByName("D${counter}").setText("MANAJER UPT");
    sheet.getRangeByName("H${counter}").setText("MANAJER RENEV");
    counter += 4;
    sheet.getRangeByName("D${counter}").setText("HENDRIK MARYONO");
    sheet.getRangeByName("H${counter}").setText("DODY KURNIAWAN");

    //Save and launch the excel.
    final List<int> bytes = workbook.saveAsStream();
    workbook.dispose();

    final path = await _localPath;
    final String filename = '${path}/DataRabApar.xlsx';
    File file = File(filename);
    await file.writeAsBytes(bytes);

    OpenFile.open(filename);
  }

  Future getDataNotifikasi() async {
    this.loadingProses = true;
    update();

    await getUserIdSession();

    Map<String, dynamic> params = {};
    params["last_id"] = "";
    params["user_id"] = SessionConfig.user_id;
    params["upt_id"] = SessionConfig.upt_id;
    params["wilayah"] = SessionConfig.wilayah;

    List<AparModels> data =
        await DaftarAparServices.getDataNotifikasi(params, this.dataDocument);
    this.dataDocument = data;
    this.loadingProses = false;
    this.update();
  }

  Future getDataLoadMoreNotifikasi() async {
    this.loadingProses = true;
    update();

    await getUserIdSession();

    Map<String, dynamic> params = {};
    params["last_id"] = this.dataDocument.length > 0
        ? this.dataDocument[this.dataDocument.length - 1].id
        : "";
    params["user_id"] = SessionConfig.user_id;
    params["upt_id"] = SessionConfig.upt_id;
    params["tanggal_awal"] = this.tanggal_start;
    params["tanggal_akhir"] = this.tanggal_end;
    params["wilayah"] = SessionConfig.wilayah;

    List<AparModels> data = await DaftarAparServices.getDataLoadMoreNotifikasi(
        params, this.dataDocument);
    this.dataDocument = data;
    this.loadingProses = false;
    this.update();
  }
}
