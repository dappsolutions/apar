import 'package:apar/app/modules/daftarapar/models/wilayah.models.dart';
import 'package:apar/app/modules/daftarapar/services/daftarwilayahapar.services.dart';
import 'package:apar/config/session.config.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DaftarWilayahAparControllers extends GetxController {
  List<WilayahModels> data = [];
  bool isLoading = false;

Future getUserIdSession() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    SessionConfig.user_id = pref.getString("user_id")!;
    SessionConfig.upt_id = pref.getString("upt_id")!;
    SessionConfig.wilayah = pref.getString("wilayah")!;
    SessionConfig.id_tujuan = pref.getString("id_tujuan")!;
    this.update();
  }
  
  Future<void> getData(String wilayah) async {
    this.isLoading = true;
    update();

    await getUserIdSession();

    Map<String, dynamic> params = {};
    params["last_id"] = "";
    params["user_id"] = SessionConfig.user_id;
    params["upt_id"] = SessionConfig.upt_id;
    params["wilayah"] =wilayah;
    params["id_tujuan"] =SessionConfig.id_tujuan;

    List<WilayahModels> data =
        await DaftarWilayahAparServices.getData(params, this.data);
    this.data = data;
    this.isLoading = false;
    this.update();
  }
}
