import 'package:apar/app/modules/daftarapar/services/scanqr.services.dart';
import 'package:apar/app/modules/main/models/tempat.models.dart';
import 'package:apar/config/session.config.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ScanQrControllers extends GetxController {
  List<TempatModels> data = [];
  bool loadingProses = false;
  Future<void> getUserIdSession() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    SessionConfig.user_id = pref.getString("user_id")!;
    SessionConfig.upt_id = pref.getString("upt_id")!;
    SessionConfig.wilayah = pref.getString("wilayah")!;
    this.update();
  }

  Future<List<TempatModels>> getDetailDataApar(
      String wilayah, String id_wilayah, String qrcode) async {
    this.loadingProses = true;
    update();

    await getUserIdSession();

    Map<String, dynamic> params = {};
    params["user_id"] = SessionConfig.user_id;
    params["upt_id"] = SessionConfig.upt_id;
    params["wilayah"] = wilayah;
    params["id_lokasi"] = id_wilayah;
    params["qrcode"] = qrcode;

    List<TempatModels> data =
        await ScanQrServices.getDetailDataApar(params, this.data);
    this.loadingProses = true;
    return data;
  }
}
