
import 'dart:convert';

import 'package:apar/app/modules/daftarapar/models/apar.models.dart';
import 'package:apar/app/modules/daftarapar/services/searchapar.services.dart';
import 'package:apar/config/api.config.dart';
import 'package:apar/config/modules.config.dart';
import 'package:apar/config/routes.config.dart';
import 'package:apar/config/session.config.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class SearchAparControllers extends GetxController{
  List<AparModels> dataDocument = [];
  bool loadingProses = false;
  String keyword = "";

  Future getUserIdSession() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    SessionConfig.user_id = pref.getString("user_id")!;
    SessionConfig.upt_id = pref.getString("upt_id")!;
    SessionConfig.wilayah = pref.getString("wilayah")!;
    this.update();
  }

  
  Future getData() async {
    this.loadingProses = true;
    update();

    await getUserIdSession();

    Map<String, dynamic> params = {};
    params["last_id"] = "";
    params["keyword"] = this.keyword;
    params["user_id"] = SessionConfig.user_id;
    params["upt_id"] = SessionConfig.upt_id;
    params["wilayah"] = SessionConfig.wilayah;

    List<AparModels> data = await SearchAparServices.getData(params, this.dataDocument);
    this.dataDocument = data;
    this.loadingProses = false;
    this.update();
  }
  
  Future getDataLoadMore() async {
    this.loadingProses = true;
    update();

    await getUserIdSession();

    Map<String, dynamic> params = {};
    params["last_id"] = this.dataDocument.length > 0
        ? this.dataDocument[this.dataDocument.length - 1].id
        : "";
    params["user_id"] = SessionConfig.user_id;
    params["upt_id"] = SessionConfig.upt_id;
    params["keyword"] = this.keyword;
    params["wilayah"] = SessionConfig.wilayah;
    List<AparModels> data= await SearchAparServices.getDataLoadMore(params, this.dataDocument);
    this.dataDocument = data;
    this.loadingProses = false;
    this.update();
  } 

  Future getDataNotifikasi() async {
    this.loadingProses = true;
    update();

    await getUserIdSession();

    Map<String, dynamic> params = {};
    params["last_id"] = "";
    params["keyword"] = this.keyword;
    params["user_id"] = SessionConfig.user_id;
    params["upt_id"] = SessionConfig.upt_id;
    params["wilayah"] = SessionConfig.wilayah;

    List<AparModels> data = await SearchAparServices.getDataNotifikasi(params, this.dataDocument);
    this.dataDocument = data;
    this.loadingProses = false;
    this.update();
  }

  Future getDataLoadMoreNotifikasi() async {
    this.loadingProses = true;
    update();

    await getUserIdSession();

    Map<String, dynamic> params = {};
    params["last_id"] = this.dataDocument.length > 0
        ? this.dataDocument[this.dataDocument.length - 1].id
        : "";
    params["user_id"] = SessionConfig.user_id;
    params["upt_id"] = SessionConfig.upt_id;
    params["keyword"] = this.keyword;
    params["wilayah"] = SessionConfig.wilayah;
    List<AparModels> data= await SearchAparServices.getDataLoadMoreNotifikasi(params, this.dataDocument);
    this.dataDocument = data;
    this.loadingProses = false;
    this.update();
  } 
}