


class AparModels{
  late String id;
  late String no_alat;
  late String nama_wilayah;
  late String nama_lokasi;
  late String tempat;
  late String lokasi_tujuan;

  AparModels({required this.id, required this.no_alat, required this.nama_wilayah, required this.nama_lokasi, required this.tempat, required this.lokasi_tujuan});

  AparModels.fromJson(Map<String, dynamic> json){
    this.id = json['id'].toString();
    this.no_alat = json['no_alat'];
    this.nama_wilayah = json['nama_wilayah'];
    this.nama_lokasi = json['nama_lokasi'];
    this.tempat = json['tempat'];
    this.lokasi_tujuan = json['lokasi_tujuan'].toString();
  }
}