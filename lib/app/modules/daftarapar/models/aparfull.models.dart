



class AparFullModels{
  late String id;
  late String no_alat;
  late String nama_wilayah;
  late String nama_lokasi;
  late String tempat;
  late String jenis;
  late String nama_merk;
  late String berat;
  late String tanggal;
  late String pembersihan;
  late String tekanan;

  AparFullModels({required this.id, required this.no_alat, required this.nama_wilayah, required this.nama_lokasi, required this.tempat, required this.jenis, required this.nama_merk, required this.berat, required this.tanggal, required this.pembersihan, required this.tekanan});

  AparFullModels.fromJson(Map<String, dynamic> json){
    this.id = json['id'].toString();
    this.no_alat = json['no_alat'];
    this.nama_wilayah = json['nama_wilayah'];
    this.nama_lokasi = json['nama_lokasi'];
    this.tempat = json['tempat'];
    this.jenis = json['jenis'];
    this.nama_merk = json['nama_merk'];
    this.berat = json['berat'].toString();
    this.tanggal = json['tanggal'].toString();
    this.pembersihan = json['pembersihan'].toString();
    this.tekanan = json['tekanan'].toString();
  }
}