



class WilayahModels{
  late String id_lokasi;
  late String nama_lokasi;

  WilayahModels({required this.id_lokasi, required this.nama_lokasi});

  WilayahModels.fromJson(Map<String, dynamic> json){
    this.nama_lokasi = json['nama_lokasi'].toString();
    this.id_lokasi = json['id_lokasi'];
  }
}