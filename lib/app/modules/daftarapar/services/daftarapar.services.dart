import 'dart:convert';

import 'package:apar/app/modules/daftarapar/models/apar.models.dart';
import 'package:apar/app/modules/daftarapar/models/aparfull.models.dart';
import 'package:apar/app/modules/daftarapar/models/oustandingapar.models.dart';
import 'package:apar/config/api.config.dart';
import 'package:apar/config/modules.config.dart';
import 'package:apar/config/routes.config.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class DaftarAparServices {
  static Future<List<AparModels>> getData(
      Map<String, dynamic> params, List<AparModels> dataDocument) async {
    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_ALAT][Routes.GETDATA]),
        body: params);

// print("DAFTAR APAR ${request.body}");
    if (request.statusCode == 200) {
      dataDocument.clear();
      var data = json.decode(request.body);
      for (var val in data["data"]) {
        dataDocument.add(AparModels.fromJson(val));
      }
      return dataDocument;
    } else {
      Get.snackbar("Informasi", "Gagal Memuat Data",
          backgroundColor: Colors.redAccent, colorText: Colors.white);
    }
    return [];
  }
  
  static Future<List<OustandingAparModels>> getDataOutstadingApproval(
      Map<String, dynamic> params, List<OustandingAparModels> dataDocument) async {
    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_ALAT][Routes.GETDATAAPPROVAL]),
        body: params);
    print("URL API : ${Api.route[ModulesConfig.MODULE_ALAT][Routes.GETDATAAPPROVAL]}");
    print("PARAMS : ${params}");
    print("REQUEST : ${request.body}");
    if (request.statusCode == 200) {
      dataDocument.clear();
      var data = json.decode(request.body);
      for (var val in data["data"]) {
        dataDocument.add(OustandingAparModels.fromJson(val));
      }
      return dataDocument;
    } else {
      Get.snackbar("Informasi", "Gagal Memuat Data",
          backgroundColor: Colors.redAccent, colorText: Colors.white);
    }
    return [];
  }

  static Future<List<AparFullModels>> getFullData(Map<String, dynamic> params,
      List<AparFullModels> dataFullDocument) async {
    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_ALAT][Routes.GETDATAFILTER]),
        body: params);

    if (request.statusCode == 200) {
      dataFullDocument.clear();
      var data = json.decode(request.body);
      for (var val in data["data"]) {
        dataFullDocument.add(AparFullModels.fromJson(val));
      }

      return dataFullDocument;
    } else {
      Get.snackbar("Informasi", "Gagal Memuat Data",
          backgroundColor: Colors.redAccent, colorText: Colors.white);
    }
    return [];
  }
  
  static Future<List<AparFullModels>> getFullDataRab(Map<String, dynamic> params,
      List<AparFullModels> dataFullDocument) async {
    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_ALAT][Routes.GETDATAFILTERRAB]),
        body: params);

        print("FULL DOCUMENT ${request.body}");
        

    if (request.statusCode == 200) {
      dataFullDocument.clear();
      var data = json.decode(request.body);
      for (var val in data["data"]) {
        dataFullDocument.add(AparFullModels.fromJson(val));
      }

      return dataFullDocument;
    } else {
      Get.snackbar("Informasi", "Gagal Memuat Data",
          backgroundColor: Colors.redAccent, colorText: Colors.white);
    }
    return [];
  }

  static Future<List<AparModels>> getDataLoadMore(
      Map<String, dynamic> params, List<AparModels> dataDocument) async {
    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_ALAT][Routes.GETDATAFILTER]),
        body: params);

// print("DATA LOAD MORE ${request.body}");
    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      for (var val in data["data"]) {
        dataDocument.add(AparModels.fromJson(val));
      }
      return dataDocument;
    } else {
      Get.snackbar("Informasi", "Gagal Memuat Data",
          backgroundColor: Colors.redAccent, colorText: Colors.white);
    }
    return [];
  }

  static Future<List<AparFullModels>> getDataLoadMoreFull(Map<String, dynamic> params,
      List<AparFullModels> dataFullDocument) async {
    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_ALAT][Routes.GETDATAFILTER]),
        body: params);

    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      for (var val in data["data"]) {
        dataFullDocument.add(AparFullModels.fromJson(val));
      }

      return dataFullDocument;
    } else {
      Get.snackbar("Informasi", "Gagal Memuat Data",
          backgroundColor: Colors.redAccent, colorText: Colors.white);
    }
    return [];
  }
  
  static Future<List<AparFullModels>> getDataLoadMoreFullRab(Map<String, dynamic> params,
      List<AparFullModels> dataFullDocument) async {
    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_ALAT][Routes.GETDATAFILTERRAB]),
        body: params);

    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      for (var val in data["data"]) {
        dataFullDocument.add(AparFullModels.fromJson(val));
      }

      return dataFullDocument;
    } else {
      Get.snackbar("Informasi", "Gagal Memuat Data",
          backgroundColor: Colors.redAccent, colorText: Colors.white);
    }
    return [];
  }

  static Future<List<AparModels>> getDataNotifikasi(
      Map<String, dynamic> params, List<AparModels> dataDocument) async {
    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_ALAT][Routes.GETDATANOTIF]),
        body: params);

// print("REQUEST ${request.body}");
    if (request.statusCode == 200) {
      dataDocument.clear();
      var data = json.decode(request.body);
      for (var val in data["data"]) {
        dataDocument.add(AparModels.fromJson(val));
      }
      return dataDocument;
    } else {
      Get.snackbar("Informasi", "Gagal Memuat Data",
          backgroundColor: Colors.redAccent, colorText: Colors.white);
    }
    return [];
  }

  static Future<List<AparModels>> getDataLoadMoreNotifikasi(
      Map<String, dynamic> params, List<AparModels> dataDocument) async {
    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_ALAT][Routes.GETDATANOTIF]),
        body: params);

    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      for (var val in data["data"]) {
        dataDocument.add(AparModels.fromJson(val));
      }
      return dataDocument;
    } else {
      Get.snackbar("Informasi", "Gagal Memuat Data",
          backgroundColor: Colors.redAccent, colorText: Colors.white);
    }
    return [];
  }
}
