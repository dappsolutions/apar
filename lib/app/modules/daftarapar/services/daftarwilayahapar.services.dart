

import 'dart:convert';

import 'package:apar/app/modules/daftarapar/models/wilayah.models.dart';
import 'package:apar/config/api.config.dart';
import 'package:apar/config/modules.config.dart';
import 'package:apar/config/routes.config.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class DaftarWilayahAparServices {
  static Future<List<WilayahModels>> getData(Map<String, dynamic> params, List<WilayahModels> dataWilayah) async{
     var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_ALAT][Routes.GETDATAWILAYAH]),
        body: params);

// print("SQL ${request.body}");  
    if (request.statusCode == 200) {      
      dataWilayah.clear();
      var data = json.decode(request.body);
      for (var val in data["data"]) {
        dataWilayah.add(WilayahModels.fromJson(val));
      }

      return dataWilayah;
    } else {
      Get.snackbar("Informasi", "Gagal Memuat Data",
          backgroundColor: Colors.redAccent, colorText: Colors.white);
    }
    return [];
  }
}