import 'dart:convert';

import 'package:apar/app/modules/daftarapar/models/apar.models.dart';
import 'package:apar/config/api.config.dart';
import 'package:apar/config/modules.config.dart';
import 'package:apar/config/routes.config.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class SearchAparServices {
  static Future<List<AparModels>> getData(
      Map<String, dynamic> params, List<AparModels> dataDocument) async {
    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_ALAT][Routes.GETDATA]),
        body: params);

    if (request.statusCode == 200) {
      dataDocument.clear();
      var data = json.decode(request.body);
      for (var val in data["data"]) {
        dataDocument.add(AparModels.fromJson(val));
      }
      return dataDocument;
    } else {
      Get.snackbar("Informasi", "Gagal Memuat Data",
          backgroundColor: Colors.redAccent, colorText: Colors.white);
    }
    return [];
  }

  static Future<List<AparModels>> getDataLoadMore(
      Map<String, dynamic> params, List<AparModels> dataDocument) async {
    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_ALAT][Routes.GETDATA]),
        body: params);

    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      for (var val in data["data"]) {
        dataDocument.add(AparModels.fromJson(val));
      }
      return dataDocument;
    } else {
      Get.snackbar("Informasi", "Gagal Memuat Data",
          backgroundColor: Colors.redAccent, colorText: Colors.white);
    }
    return [];
  }


   static Future<List<AparModels>> getDataNotifikasi(
      Map<String, dynamic> params, List<AparModels> dataDocument) async {
    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_ALAT][Routes.GETDATANOTIF]),
        body: params);

    if (request.statusCode == 200) {
      dataDocument.clear();
      var data = json.decode(request.body);
      for (var val in data["data"]) {
        dataDocument.add(AparModels.fromJson(val));
      }
      return dataDocument;
    } else {
      Get.snackbar("Informasi", "Gagal Memuat Data",
          backgroundColor: Colors.redAccent, colorText: Colors.white);
    }
    return [];
  }

  static Future<List<AparModels>> getDataLoadMoreNotifikasi(
      Map<String, dynamic> params, List<AparModels> dataDocument) async {
    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_ALAT][Routes.GETDATA]),
        body: params);

    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      for (var val in data["data"]) {
        dataDocument.add(AparModels.fromJson(val));
      }
      return dataDocument;
    } else {
      Get.snackbar("Informasi", "Gagal Memuat Data",
          backgroundColor: Colors.redAccent, colorText: Colors.white);
    }
    return [];
  }
}
