import 'package:apar/app/modules/daftarapar/controllers/daftarapar.controllers.dart';
import 'package:apar/app/modules/daftarapar/views/serachapar.views.dart';
import 'package:apar/app/modules/inspeksi/views/addinspeksi.views.dart';
import 'package:apar/ui/Uicolor.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DaftarAparViews extends StatefulWidget {
  const DaftarAparViews({Key? key}) : super(key: key);

  @override
  _DaftarAparViewsState createState() => _DaftarAparViewsState();
}

class _DaftarAparViewsState extends State<DaftarAparViews> {
  late DaftarAparControllers controllers;

  @override
  void initState() {
    super.initState();
    controllers = new DaftarAparControllers();
    // print("DAFTAR APAR");
    controllers.getData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("DAFTAR APAR"),
        backgroundColor: Uicolor.hexToColor(Uicolor.greenPln),
        elevation: 0,
      ),
      body: SingleChildScrollView(
          child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
                child: GetBuilder<DaftarAparControllers>(
              init: controllers,
              builder: (params) {
                if (!params.loadingProses) {
                  if (params.dataDocument.length > 0) {
                    return ListView.builder(
                      itemCount: params.dataDocument.length,
                      shrinkWrap: true,
                      physics: ClampingScrollPhysics(),
                      // physics: const BouncingScrollPhysics(
                      //     parent: AlwaysScrollableScrollPhysics()),
                      itemBuilder: (context, index) {
                        var data = params.dataDocument[index];

                        return Container(
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.all(3),
                          child: GestureDetector(
                            child: Card(
                              elevation: 0,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Container(
                                    alignment: Alignment.topCenter,
                                    padding: EdgeInsets.all(8),
                                    child: Text("Peralatan",
                                        style: TextStyle(
                                            color: Uicolor.hexToColor(
                                                Uicolor.grey),
                                            fontWeight: FontWeight.bold,
                                            fontSize: 18)),
                                  ),
                                  Divider(),
                                  Container(
                                      padding: EdgeInsets.all(8),
                                      child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Container(
                                              // width: 100,
                                              // color: Colors.blue,
                                              child: Text(
                                                "APAR : ${data.no_alat}",
                                                style: TextStyle(
                                                    color: Colors.green,
                                                    fontSize: 28,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ),
                                            Divider(),
                                            Container(
                                              // color: Colors.red,
                                              // width: 100,
                                              alignment: Alignment.topLeft,
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  Text(
                                                    "${data.nama_wilayah}",
                                                    style: TextStyle(
                                                        color:
                                                            Uicolor.hexToColor(
                                                                Uicolor.grey)),
                                                  ),
                                                  Text(
                                                    "${data.nama_lokasi}",
                                                    style: TextStyle(
                                                        color:
                                                            Uicolor.hexToColor(
                                                                Uicolor.grey)),
                                                  ),
                                                  Text(
                                                    "${data.tempat}",
                                                    style: TextStyle(
                                                        color:
                                                            Uicolor.hexToColor(
                                                                Uicolor.grey)),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ])),
                                ],
                              ),
                            ),
                            onTap: () {
                              Get.to(AddInspeksiViews(
                                no_apar: data.no_alat,
                                id_apar: data.id,
                                lokasi_tujuan: data.lokasi_tujuan,
                              ));
                            },
                          ),
                        );
                      },
                    );
                  } else {
                    return Center(
                      child: Text("Tidak ada data ditemukan"),
                    );
                  }
                }

                return Center(
                  child: CircularProgressIndicator(),
                );
              },
            )),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.only(left: 24, right: 24),
              child: RaisedButton(
                // child: Icon(Icons.refresh, color: Colors.white,),
                child: Text(
                  "Tampilkan Lebih Banyak",
                  style: TextStyle(color: Colors.black),
                ),
                color: Uicolor.hexToColor(Uicolor.grey_young),
                onPressed: () {
                  controllers.getDataLoadMore();
                },
                elevation: 0,
                // shape:CircleBorder(),
              ),
            ),
            SizedBox(
              height: 8,
            ),
            Divider(),
          ],
        ),
      )),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Get.to(SearchAparViews());
        },
        backgroundColor: Colors.orangeAccent,
        child: Icon(Icons.search),
      ),
    );
  }
}
