import 'package:apar/app/modules/daftarapar/controllers/daftarapar.controllers.dart';
import 'package:apar/app/modules/inspeksi/views/detailinspeksi.views.dart';
import 'package:apar/app/modules/inspeksi/views/documentpdf.views.dart';
import 'package:apar/ui/Uicolor.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DaftarOutStandingAparViews extends StatefulWidget {
  const DaftarOutStandingAparViews({Key? key}) : super(key: key);

  @override
  State<DaftarOutStandingAparViews> createState() =>
      _DaftarOutStandingAparViewsState();
}

class _DaftarOutStandingAparViewsState
    extends State<DaftarOutStandingAparViews> {
  late DaftarAparControllers controllers;

  @override
  void initState() {
    super.initState();
    controllers = new DaftarAparControllers();
    controllers.getDataOutstadingApproval();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("DAFTAR OUTSTANDING INSPEKSI"),
        backgroundColor: Uicolor.hexToColor(Uicolor.greenPln),
        elevation: 0,
      ),
      body: SingleChildScrollView(
          child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
                child: GetBuilder<DaftarAparControllers>(
              init: controllers,
              builder: (params) {
                if (!params.loadingProses) {
                  if (params.dataDocumentOustanding.length > 0) {
                    return ListView.builder(
                      itemCount: params.dataDocumentOustanding.length,
                      shrinkWrap: true,
                      physics: ClampingScrollPhysics(),
                      // physics: const BouncingScrollPhysics(
                      //     parent: AlwaysScrollableScrollPhysics()),
                      itemBuilder: (context, index) {
                        var data = params.dataDocumentOustanding[index];

                        return Container(
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.all(3),
                          child: GestureDetector(
                            child: Card(
                              elevation: 0,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Container(
                                    alignment: Alignment.topCenter,
                                    padding: EdgeInsets.all(8),
                                    child: Text(data.no_document,
                                        style: TextStyle(
                                            color: Uicolor.hexToColor(
                                                Uicolor.grey),
                                            fontWeight: FontWeight.bold,
                                            fontSize: 18)),
                                  ),
                                  Divider(),
                                  Container(
                                      padding: EdgeInsets.all(8),
                                      child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Container(
                                              // width: 100,
                                              // color: Colors.blue,
                                              child: Text(
                                                "APAR : ${data.no_alat}",
                                                style: TextStyle(
                                                    color: Colors.green,
                                                    fontSize: 28,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ),
                                            Divider(),
                                            Container(
                                              // color: Colors.red,
                                              // width: 100,
                                              alignment: Alignment.topLeft,
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  Text(
                                                    "${data.nama_wilayah}",
                                                    style: TextStyle(
                                                        color:
                                                            Uicolor.hexToColor(
                                                                Uicolor.grey)),
                                                  ),
                                                  Text(
                                                    "${data.nama_lokasi}",
                                                    style: TextStyle(
                                                        color:
                                                            Uicolor.hexToColor(
                                                                Uicolor.grey)),
                                                  ),
                                                  Text(
                                                    "${data.tempat}",
                                                    style: TextStyle(
                                                        color:
                                                            Uicolor.hexToColor(
                                                                Uicolor.grey)),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ])),
                                ],
                              ),
                            ),
                            onTap: () {
                              // goto deetail inspeksi
                              // Get.to(DetailInspeksiViews(
                              //     no_apar: data.no_document,
                              //     status: 'READY_APPROVED',
                              //     no_alat: data.no_alat,
                              //     id_alat: data.id,
                              //     lokasi_tujuan: data.lokasi_tujuan));

                              Get.to(DocumentPdfViews(
                                no_apar: data.no_document,
                                status: 'READY_APPROVED',
                              ));
                            },
                          ),
                        );
                      },
                    );
                  } else {
                    return Center(
                      child: Text("Tidak ada data ditemukan"),
                    );
                  }
                }

                return Center(
                  child: CircularProgressIndicator(),
                );
              },
            )),
            SizedBox(
              height: 8,
            ),
            Divider(),
          ],
        ),
      )),
    );
  }
}
