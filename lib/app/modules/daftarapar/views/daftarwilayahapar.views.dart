import 'package:apar/app/modules/daftarapar/controllers/daftarwilayahapar.controllers.dart';
import 'package:apar/app/modules/main/views/floorapar.views.dart';
import 'package:apar/ui/Uicolor.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DaftarWilayahAparViews extends StatefulWidget {
  String wilayah;
  DaftarWilayahAparViews({required this.wilayah});

  @override
  _DaftarWilayahAparViewsState createState() => _DaftarWilayahAparViewsState();
}

class _DaftarWilayahAparViewsState extends State<DaftarWilayahAparViews> {
  late DaftarWilayahAparControllers controllers;

  @override
  void initState() {    
    super.initState();
    controllers = new DaftarWilayahAparControllers();
    controllers.getData(widget.wilayah);
  }  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Uicolor.hexToColor(Uicolor.whiteSmooth),
      appBar: AppBar(
        title: Text(
          "WILAYAH ${widget.wilayah}",
          style: TextStyle(color: Uicolor.hexToColor(Uicolor.whiteSmooth)),
        ),
        backgroundColor: Uicolor.hexToColor(Uicolor.greenPln),
        elevation: 0,
      ),
      body: SingleChildScrollView(
          child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            RefreshIndicator(
              child: Container(
                  child: GetBuilder<DaftarWilayahAparControllers>(
                init: controllers,
                builder: (params) {
                  if (!params.isLoading) {
                    if (params.data.length > 0) {
                      return ListView.builder(
                        itemCount: params.data.length,
                        shrinkWrap: true,
                        physics: ClampingScrollPhysics(),
                        itemBuilder: (context, index) {
                          var data = params.data[index];

                          return Container(
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.all(6),
                              margin: EdgeInsets.only(left: 16, right: 16),
                              child: GestureDetector(
                                child: Card(
                                  elevation: 0,
                                  color: Colors.white,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Container(
                                        alignment: Alignment.topRight,
                                        padding: EdgeInsets.all(8),
                                        child: Text("Wilayah",
                                            style: TextStyle(
                                                color: Uicolor.hexToColor(
                                                    Uicolor.grey),
                                                fontWeight: FontWeight.bold,
                                                fontSize: 18)),
                                      ),
                                      Divider(),
                                      Container(
                                          padding: EdgeInsets.all(8),
                                          child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Container(
                                                  // width: 100,
                                                  // color: Colors.blue,
                                                  alignment:
                                                      Alignment.topCenter,
                                                  child: Text(
                                                    "${data.nama_lokasi.toUpperCase()}",
                                                    style: TextStyle(
                                                      color: Colors.green,
                                                      fontSize: 28,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  ),
                                                ),
                                                Divider(),
                                              ])),
                                    ],
                                  ),
                                ),
                                onTap: () {
                                  Get.to(FloorAparViews(
                                    wilayah: widget.wilayah,
                                    nama_wilayah: data.nama_lokasi.toUpperCase(),
                                    id_lokasi: data.id_lokasi,
                                  ));
                                },
                              ));
                        },
                      );
                    } else {
                      return Center(
                        child: Text("Tidak ada data ditemukan"),
                      );
                    }
                  }

                  return Center(
                    child: CircularProgressIndicator(),
                  );
                },
              )),
              onRefresh: () async {
                controllers.getData(widget.wilayah);
              },
            ),
            SizedBox(
              height: 8,
            ),
            Divider(),
          ],
        ),
      )),
    );
  }
}
