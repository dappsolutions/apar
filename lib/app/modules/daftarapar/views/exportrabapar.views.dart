import 'package:apar/app/modules/daftarapar/controllers/daftarapar.controllers.dart';
import 'package:apar/app/modules/daftarapar/controllers/exportrabapar.controllers.dart';
import 'package:apar/ui/Uicolor.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

class ExportRabAparViews extends StatefulWidget {
  const ExportRabAparViews({ Key? key }) : super(key: key);

  @override
  _ExportRabAparViewsState createState() => _ExportRabAparViewsState();
}

class _ExportRabAparViewsState extends State<ExportRabAparViews> {
  late DaftarAparControllers controllers;

  @override
  void initState() {    
    super.initState();
    controllers = new DaftarAparControllers();
    controllers.getFullDataRab();
  }

  @override
  Widget build(BuildContext context) {
     return Scaffold(
      appBar: AppBar(
        title: Text("EXPORT DAFTAR APAR"),
        backgroundColor: Uicolor.hexToColor(Uicolor.greenPln),
        elevation: 0,
      ),
      body: SingleChildScrollView(
          child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
                child: SfDateRangePicker(
              onSelectionChanged: controllers.selectedDate,
              selectionMode: DateRangePickerSelectionMode.range,
              initialSelectedRange:
                  PickerDateRange(DateTime.now(), DateTime.now()),
            )),
             Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                height: MediaQuery.of(context).size.height * 0.07,
                width: double.infinity,
                // height: 50,
                child: RaisedButton(
                  child: Text(
                    "FILTER",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.orangeAccent,
                  onPressed: () async {
                    controllers.getFullData();
                    // print(controllers.tanggal_start+" dan "+controllers.tanggal_end);
                  },
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(3.0),
                  ),
                ),
              ),
              Divider(),
              Container(
                alignment: Alignment.topLeft,
                padding: EdgeInsets.all(16),
                child: Text("Daftar Peralatan", style: TextStyle(fontSize: 20),),
              ),
            RefreshIndicator(
              child: Container(
                  child: GetBuilder<DaftarAparControllers>(
                init: controllers,
                builder: (params) {
                  if (!params.loadingProses) {
                    if (params.dataFullDocument.length > 0) {
                      return ListView.builder(
                        itemCount: params.dataFullDocument.length,
                        shrinkWrap: true,
                        physics: ClampingScrollPhysics(),
                        itemBuilder: (context, index) {
                          var data = params.dataFullDocument[index];

                          return Container(
                            width: MediaQuery.of(context).size.width,
                            padding: EdgeInsets.all(3),
                            child: GestureDetector(
                              child: Card(
                                elevation: 0,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Container(
                                      alignment: Alignment.topCenter,
                                      padding: EdgeInsets.all(8),
                                      child: Text("Peralatan",
                                          style: TextStyle(
                                              color: Uicolor.hexToColor(
                                                  Uicolor.grey),
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18)),
                                    ),
                                    Divider(),
                                    Container(
                                        padding: EdgeInsets.all(8),
                                        child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Container(
                                                // width: 100,
                                                // color: Colors.blue,
                                                child: Text(
                                                  "APAR : ${data.no_alat}",
                                                  style: TextStyle(
                                                      color: Colors.green,
                                                      fontSize: 28,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                              ),
                                              Divider(),
                                              Container(
                                                // color: Colors.red,
                                                // width: 100,
                                                alignment: Alignment.topLeft,
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: [
                                                    Text(
                                                      "${data.nama_wilayah}",
                                                      style: TextStyle(
                                                          color: Uicolor
                                                              .hexToColor(
                                                                  Uicolor
                                                                      .grey)),
                                                    ),
                                                    Text(
                                                      "${data.nama_lokasi}",
                                                      style: TextStyle(
                                                          color: Uicolor
                                                              .hexToColor(
                                                                  Uicolor
                                                                      .grey)),
                                                    ),
                                                    Text(
                                                      "${data.tempat}",
                                                      style: TextStyle(
                                                          color: Uicolor
                                                              .hexToColor(
                                                                  Uicolor
                                                                      .grey)),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ])),
                                  ],
                                ),
                              ),
                              onTap: () {},
                            ),
                          );
                        },
                      );
                    } else {
                      return Center(
                        child: Text("Tidak ada data ditemukan"),
                      );
                    }
                  }

                  return Center(
                    child: CircularProgressIndicator(),
                  );
                },
              )),
              onRefresh: () async {
                controllers.getData();
              },
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.only(left: 24, right: 24),
              child: RaisedButton(
                // child: Icon(Icons.refresh, color: Colors.white,),
                child: Text(
                  "Tampilkan Lebih Banyak",
                  style: TextStyle(color: Colors.black),
                ),
                color: Uicolor.hexToColor(Uicolor.grey_young),
                onPressed: () {
                  controllers.getDataLoadMoreFullRab();
                },
                elevation: 0,
                // shape:CircleBorder(),
              ),
            ),
            SizedBox(
              height: 8,
            ),
            Divider(),
          ],
        ),
      )),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          controllers.generateExcelRab();
        },
        backgroundColor: Colors.green,
        child: Icon(Icons.cancel_sharp),
      ),
    );
  }
}