import 'package:apar/app/modules/daftarapar/controllers/listapar.controllers.dart';
import 'package:apar/app/modules/inspeksi/views/addinspeksi.views.dart';
import 'package:apar/app/modules/inspeksi/views/detailinspeksi.views.dart';
import 'package:apar/app/modules/main/models/apardetail.models.dart';
import 'package:apar/app/modules/main/models/tempat.models.dart';
import 'package:apar/ui/Uicolor.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ListDetailApar extends StatefulWidget {
  List<AparDetailModels> data;
  String wilayah;
  String nama_wilayah;
  String tempat;
  ListDetailApar({required this.data, required this.wilayah, required this.tempat, required this.nama_wilayah});

  @override
  _ListDetailAparState createState() => _ListDetailAparState();
}

class _ListDetailAparState extends State<ListDetailApar> {
  ListAparControllers controllers = Get.put(ListAparControllers());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Uicolor.hexToColor(Uicolor.whiteSmooth),
      appBar: AppBar(
        title: Text(
          "${widget.nama_wilayah}, ${widget.tempat}",
          style: TextStyle(color: Uicolor.hexToColor(Uicolor.whiteSmooth)),
        ),
        backgroundColor: Uicolor.hexToColor(Uicolor.greenPln),
        elevation: 0,
      ),
      body: SingleChildScrollView(
          child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            RefreshIndicator(
              child: Container(
                  child: ListView.builder(
                itemCount: widget.data.length,
                shrinkWrap: true,
                physics: ClampingScrollPhysics(),
                itemBuilder: (context, index) {
                  var data = widget.data[index];

                  return Container(
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.all(6),
                      margin: EdgeInsets.only(left: 16, right: 16),
                      child: GestureDetector(
                        child: Card(
                          elevation: 0,
                          color: Colors.white,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                height: 50,
                                alignment: Alignment.topRight,
                                color: data.has_pemeliharaan == '1'
                                    ? Colors.green[200]
                                    : data.has_pemeliharaan == '-1'
                                        ? Colors.yellow[50]
                                        : Colors.orange[200],
                                padding: EdgeInsets.all(8),
                                child: Text(
                                    "APAR ${data.no_alat.toUpperCase()}",
                                    style: TextStyle(
                                        color:
                                            Uicolor.hexToColor(Uicolor.black),
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18)),
                              ),
                            ],
                          ),
                        ),
                        onTap: () {
                          if (widget.wilayah == "") {
                            Get.snackbar('Informasi',
                                "Anda Tidak Punya Akses untuk Fitur Ini");
                          } else {
                            if (data.no_document != '') {
                              // goto deetail inspeksi
                              Get.to(DetailInspeksiViews(
                                  no_apar: data.no_document,
                                  status: data.status,
                                  no_alat: data.no_alat,
                                  id_alat: data.id_alat,
                                  lokasi_tujuan: data.lokasi_tujuan));
                            } else {
                              Get.to(AddInspeksiViews(
                                no_apar: data.no_alat,
                                id_apar: data.id_alat,
                                lokasi_tujuan: data.lokasi_tujuan,
                              ));
                            }
                          }
                        },
                      ));
                },
              )),
              onRefresh: () async {},
            ),
            SizedBox(
              height: 8,
            ),
            Divider(),
          ],
        ),
      )),
    );
  }
}
