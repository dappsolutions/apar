import 'dart:convert';

import 'package:apar/app/modules/inspeksi/models/inspeksi.models.dart';
import 'package:apar/app/modules/inspeksi/models/kelengkapan.models.dart';
import 'package:apar/app/modules/inspeksi/services/addinspeksi.services.dart';
import 'package:apar/app/modules/response/models/message.models.dart';
import 'package:apar/app/modules/storage/inspeksi.storage.dart';
import 'package:apar/app/modules/storage/kelengkapan.storage.dart';
import 'package:apar/config/api.config.dart';
import 'package:apar/config/modules.config.dart';
import 'package:apar/config/routes.config.dart';
import 'package:apar/config/session.config.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:http/http.dart' as http;

class AddInspeksiControllers extends GetxController {
  List<InspeksiModels> inspeksiData = <InspeksiModels>[];
  List<KelengkapanModels> kelengkapanData = <KelengkapanModels>[];
  late InspeksiDataSource inspeksiDataSource;
  late KelengkapanDataSource kelengkapanDataSource;
  late InspeksiModels inspeksi;
  late KelengkapanModels kelengkapan;
  List<String> dataHanger = ['-','Ada', 'Tidak Ada'];
  String defaultHanger = "-";
  List<String> dataKetinggianApar = ['-','Sesuai', 'Tidak Sesuai'];
  String defaultKetinggianApar = "-";
  List<String> dataSegitigaApar = ['-','Ada', 'Tidak Ada'];
  String defaultSegitigaApar = "-";
  List<String> dataIndikatorTekanan = ['-','OK', 'Tidak OK'];
  String defaultIndikatorTekanan = "-";
  List<String> dataHose = ['-','OK', 'Tidak OK'];
  String defaultHose = "-";
  List<String> dataTabung = ['-','OK', 'Tidak OK'];
  String defaultTabung = "-";  
  List<String> dataPembersihan = ['Belum', 'Sudah'];
  String defaultPembersihan = "Belum";

  String tanggal = '';

  String pembersihan = '';

  String berat = '';

  String tekanan = '';
  String hanger = '';
  String ketinggian_apar = '';
  String segitiga_apar = '';
  String indikator_tekanan = '';
  String hose = '';
  String tabung = '';

  String petugas = '';
  String keterangan = '';
  String expired_date = '';

  String no_transaksi = "";
  bool loadingProses = false;
  DateTime dateChoosed = DateTime.now();

  void selectedDate(DateRangePickerSelectionChangedArgs args) {
    /// The argument value will return the changed date as [DateTime] when the
    /// widget [SfDateRangeSelectionMode] set as single.
    ///
    /// The argument value will return the changed dates as [List<DateTime>]
    /// when the widget [SfDateRangeSelectionMode] set as multiple.
    ///
    /// The argument value will return the changed range as [PickerDateRange]
    /// when the widget [SfDateRangeSelectionMode] set as range.
    ///
    /// The argument value will return the changed ranges as
    /// [List<PickerDateRange] when the widget [SfDateRangeSelectionMode] set as
    /// multi range.
    if (args.value is PickerDateRange) {
      this.tanggal = args.value.startDate.toString();
    }
    this.update();
  }

  void init() {
    this.inspeksiDataSource = InspeksiDataSource(sourceData: this.inspeksiData);
    this.kelengkapanDataSource = KelengkapanDataSource(sourceData: this.kelengkapanData);
    clearForm();
    getUserIdSession();
    this.update();
  }

  void addDataFormToDataInspeksi() {
    String tgl = this.tanggal;
    String bersih = this.pembersihan;
    String brt = this.berat;
    String teknan = this.tekanan;
    String ptgs = this.petugas;
    String expired_date_param = this.expired_date;
    int id = this.inspeksiData.length + 1;
    this.inspeksiData.add(InspeksiModels(id, tgl, bersih, brt, teknan, ptgs, expired_date_param));
    this.inspeksiDataSource = InspeksiDataSource(sourceData: this.inspeksiData);
    this.update();
  }
  
  void addDataFormToDataKelengkapanInspeksi() {
    String tgl = this.tanggal;
    String hanger = this.hanger;
    String ketinggian_apar = this.ketinggian_apar;
    String segitiga_apar = this.segitiga_apar;
    String indikator_tekanan = this.indikator_tekanan;
    String hose = this.hose;
    String tabung = this.tabung;
    int id = this.kelengkapanData.length + 1;
    this.kelengkapanData.add(KelengkapanModels(id, tgl, hanger, ketinggian_apar, segitiga_apar, indikator_tekanan, hose, tabung));
    this.kelengkapanDataSource = KelengkapanDataSource(sourceData: this.kelengkapanData);
    this.update();
  }

  void clearForm() {
    this.tanggal = "";
    this.pembersihan = "";
    this.berat = "";
    this.tekanan = "";
    this.petugas = "";
  }

  void getUserIdSession() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    SessionConfig.user_id = pref.getString("user_id")!;
    SessionConfig.upt_id = pref.getString("upt_id")!;
    this.update();
  }

  Future<int> simpanAll(String no_apar, String id_apar, String lokasi_tujuan) async {
    int result = 0;
    this.loadingProses = true;
    this.update();

    String inspeksiDataStr = InspeksiModels.inspeksiToJson(this.inspeksiData);
    String kelengkapanDataStr = KelengkapanModels.inspeksiToJson(this.kelengkapanData);

    Map<String, dynamic> params = {};
    params["user_id"] = SessionConfig.user_id;
    params["upt_id"] = SessionConfig.upt_id;
    params["no_apar"] = no_apar;
    params["id_apar"] = id_apar;
    params["lokasi_tujuan"] = lokasi_tujuan;
    params["keterangan"] = this.keterangan;
    params["data_inspeksi"] = inspeksiDataStr;
    params["data_kelengkapan"] = kelengkapanDataStr;

    Map<String, dynamic> resultServices =
        await AddInspeksiServices.simpanAll(params);
    result = resultServices['is_valid'];
    this.no_transaksi = resultServices['no_trans'];
    if (result != 1) {
      Get.snackbar("Informasi", resultServices['message'],
          backgroundColor: Colors.redAccent, colorText: Colors.white);
    }

    return result;
  }

  Future<int> simpanPerubahan(String id) async {
    int result = 0;
    this.loadingProses = true;
    this.update();

    Map<String, dynamic> params = {};
    params["user_id"] = SessionConfig.user_id;
    params["upt_id"] = SessionConfig.upt_id;
    params["id"] = id;
    params["pembersihan"] = this.pembersihan;
    params["berat"] = this.berat;
    params["tekanan"] = this.tekanan;
    params["petugas"] = this.petugas;
    params["tanggal"] = this.tanggal;
    params["hanger"] = this.hanger;
    params["ketinggian_apar"] = this.ketinggian_apar;
    params["segitiga_apar"] = this.segitiga_apar;
    params["indikator_tekanan"] = this.indikator_tekanan;
    params["hose"] = this.hose;
    params["tabung"] = this.tabung;

    Map<String, dynamic> resultServices =
        await AddInspeksiServices.simpanPerubahan(params);
    result = resultServices['is_valid'];
    if (result != 1) {
      Get.snackbar("Informasi", resultServices['message'],
          backgroundColor: Colors.redAccent, colorText: Colors.white);
    }

    return result;
  }

  Future<void> getDetailData(String no_apar, String id) async {
    this.loadingProses = true;
    this.update();

    Map<String, dynamic> params = {};
    params["no_apar"] = no_apar;
    params["id"] = id;

    InspeksiModels resultService =
        await AddInspeksiServices.getDetailData(params);
    this.loadingProses = false;
    if (resultService.id != 0) {
      this.inspeksi = resultService;
      this.tanggal = resultService.tanggal;
      this.pembersihan = resultService.pembersihan;
      this.berat = resultService.berat;
      this.tekanan = resultService.tekanan;
      this.petugas = resultService.petugas;
      this.dateChoosed = DateTime.parse(this.tanggal);

      if(this.pembersihan != 'Sudah' && this.pembersihan != 'Belum'){
        this.defaultPembersihan = 'Belum';
      }else{
        this.defaultPembersihan = this.pembersihan;
      }
    }
    this.update();
  }
  
  Future<void> getDetailDataKelengkapan(String no_apar, String id) async {
    this.loadingProses = true;
    this.update();

    Map<String, dynamic> params = {};
    params["no_apar"] = no_apar;
    params["id"] = id;

    KelengkapanModels resultService =
        await AddInspeksiServices.getDetailDataKelengkapan(params);
    this.loadingProses = false;
    if (resultService.id != 0) {
      this.kelengkapan = resultService;
      this.defaultHanger = resultService.hanger == '' ? '-' : resultService.hanger;
      this.hanger = resultService.hanger;
      this.defaultKetinggianApar = resultService.ketinggian_apar == '' ? '-' : resultService.ketinggian_apar;
      this.ketinggian_apar = resultService.ketinggian_apar;
      this.defaultSegitigaApar = resultService.segitiga_apar == '' ? '-' : resultService.segitiga_apar;
      this.segitiga_apar = resultService.segitiga_apar;
      this.defaultIndikatorTekanan = resultService.indikator_tekanan == '' ? '-' : resultService.indikator_tekanan;
      this.indikator_tekanan = resultService.indikator_tekanan;
      this.defaultHose = resultService.hose == '' ? '-' : resultService.hose;
      this.hose = resultService.hose;
      this.defaultTabung = resultService.tabung == '' ? '-' : resultService.tabung;
      this.tabung = resultService.tabung;
    }
    this.update();
  }
}
