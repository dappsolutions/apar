import 'dart:convert';
import 'dart:io';

import 'package:apar/app/modules/inspeksi/models/image.models.dart';
import 'package:apar/app/modules/inspeksi/models/imagedetail.models.dart';
import 'package:apar/app/modules/inspeksi/models/inspeksi.models.dart';
import 'package:apar/app/modules/inspeksi/services/image.services.dart';
import 'package:apar/app/modules/inspeksi/storage/image.store.dart';
import 'package:apar/app/modules/response/models/message.models.dart';
import 'package:apar/config/api.config.dart';
import 'package:apar/config/modules.config.dart';
import 'package:apar/config/routes.config.dart';
import 'package:apar/config/session.config.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart' as path_provider;
import 'package:http/http.dart' as http;

class DetailImageControllers extends GetxController {
  List<ImageModel> data_img = [];
  List<ImageDetailModel> data_img_all = [];
  List<ImageStorage> data_img_store = [];

  bool loadingProses = false;
  bool loadingSimpan = false;

  int jumlahGambarDiupload = 0;

  void clearDataImg() {
    this.data_img = [];
    this.data_img_all = [];
    this.data_img_store = [];
    this.update();
  }

  void clearDataImgAt(int item) {
    this.data_img.removeAt(item);
    this.data_img_store.removeAt(item);
    this.update();
  }

  Widget widgetImgPick() {
    return Container(
        padding: EdgeInsets.all(16),
        child: DottedBorder(
          dashPattern: [8, 4],
          strokeWidth: 2,
          color: Colors.grey,
          child: Container(
            height: 200,
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(left: 16, right: 16, top: 80),
                  child: Center(
                      child: GestureDetector(
                    child: Icon(
                      Icons.photo_camera,
                      color: Colors.grey,
                    ),
                    onTap: () {
                      this.pickImg(true);
                      // this.pickImg();
                    },
                  )),
                ),
                SizedBox(
                  height: 5,
                ),
                Container(
                  margin: EdgeInsets.only(top: 6),
                  child: Center(
                    child: Text("Ambil Gambar"),
                  ),
                ),
              ],
            ),
          ),
        ));
  }

  void pickImg(bool camera) async {
    // this.data_img.add("TES");
    // this.update();
    final pickedFile =
        // await ImagePicker().getImage(source: ImageSource.gallery);
        camera
            ? await ImagePicker().getImage(source: ImageSource.camera)
            : await ImagePicker().getImage(source: ImageSource.gallery);    
    if (pickedFile != null) {
      File img_from = File(pickedFile.path);

      final dir = await path_provider.getTemporaryDirectory();
      List<String> dataPath = pickedFile.path.split('/');

      String temporaryName =
          dataPath[dataPath.length - 1].replaceAll('.jpg', '');
      temporaryName = temporaryName.replaceAll('.png', '');
      temporaryName = temporaryName.replaceAll('.jpeg', '');
      temporaryName = temporaryName + "-temp.jpg";
      final targetPath = dir.absolute.path + "/${temporaryName}";

  print("TARGET PATh ${targetPath}");
      img_from = (await FlutterImageCompress.compressAndGetFile(
        pickedFile.path,
        targetPath,
        quality: 20,
        minWidth: 300,
        minHeight: 300,
        // rotate: 90,
      ))!;

      final bytes = await img_from.readAsBytes();
      this
          .data_img
          .add(ImageModel(file: img_from, encodeImages: base64.encode(bytes)));
      this.data_img_store.add(ImageStorage(encodeImages: base64.encode(bytes)));
    }

    this.update();
  }

  Future<int> doUpload(String no_transaksi, int item) async {
    int result = 0;

    this.loadingSimpan = true;
    this.update();

    Map<String, dynamic> params = {};
    params["user_id"] = SessionConfig.user_id;
    params["upt_id"] = SessionConfig.upt_id;
    params["no_trans"] = no_transaksi;
    params["image"] = this.data_img_store[item].encodeImages;

    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_SIDAK][Routes.SIMPANGAMBAR]),
        body: params);
    Map<String, dynamic> responeService = await ImageServices.doUpload(params);
    this.loadingSimpan = false;
    result = responeService['is_valid'];
    if (result != 1) {
      Get.snackbar("Informasi", responeService['message'],
          backgroundColor: Colors.redAccent, colorText: Colors.white);
    }

    this.update();

    return result;
  }

  Future<int> batalPengajuan(String no_transaksi) async {
    int result = 0;
    this.loadingSimpan = false;
    this.update();

    Map<String, dynamic> params = {};
    params["user_id"] = SessionConfig.user_id;
    params["no_trans"] = no_transaksi;

    Map<String, dynamic> responseServices = await ImageServices.batalPengajuan(params);
    this.loadingSimpan = false;
    result = responseServices['is_valid'];
    this.update();
    if(result != 1){
      Get.snackbar("Informasi", responseServices['message'],
          backgroundColor: Colors.redAccent, colorText: Colors.white);
    }


    return result;
  }

  Future<void> getDetailImage(String no_document) async {
    this.loadingProses = true;
    update();

    Map<String, dynamic> params = {};
    params["no_document"] = no_document;

    List<ImageDetailModel> responseData = await ImageServices.getDetailImage(params, this.data_img_all);
    this.data_img_all = responseData;
    this.loadingProses = false;
    this.update();
  }
}
