

import 'dart:convert';

import 'package:apar/app/modules/inspeksi/models/inspeksi.models.dart';
import 'package:apar/app/modules/inspeksi/models/kelengkapan.models.dart';
import 'package:apar/app/modules/response/models/message.models.dart';
import 'package:apar/app/modules/storage/inspeksi.storage.dart';
import 'package:apar/app/modules/storage/kelengkapan.storage.dart';
import 'package:apar/config/api.config.dart';
import 'package:apar/config/modules.config.dart';
import 'package:apar/config/routes.config.dart';
import 'package:apar/config/session.config.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class DetailInspeksiControllers extends GetxController{
  List<InspeksiModels> inspeksiData = <InspeksiModels>[];
  List<KelengkapanModels> kelengkapanData = <KelengkapanModels>[];
  late InspeksiDataSource inspeksiDataSource;
  late KelengkapanDataSource kelengkapanDataSource;
  bool loadingProses = false;
  String keterangan = '';
  bool edited = false;

   void init() {
    this.inspeksiDataSource = InspeksiDataSource(sourceData: this.inspeksiData);
    this.kelengkapanDataSource = KelengkapanDataSource(sourceData: this.kelengkapanData);
    this.update();
  }

   Future getListData(String no_document) async {
    this.loadingProses = true;
    update();

    Map<String, dynamic> params = {};
    params["no_document"] = no_document;

    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_SIDAK][Routes.GET_LIST_DATA]),
        body: params);

        print("DETAIL ${request.body}");

    if (request.statusCode == 200) {
      this.loadingProses = false;
      update();
      this.inspeksiData.clear();
      this.kelengkapanData.clear();

      var data = json.decode(request.body);
      for (var val in data["data"]) {
        this.inspeksiData.add(InspeksiModels.fromJson(val));        
        this.keterangan = val['verbatim'];
      }
      
      for (var val in data["data_kelengkapan"]) {
        this.kelengkapanData.add(KelengkapanModels.fromJson(val));        
      }

      this.inspeksiDataSource = InspeksiDataSource(sourceData: this.inspeksiData);      
      this.kelengkapanDataSource = KelengkapanDataSource(sourceData: this.kelengkapanData);      
    } else {
      this.loadingProses = false;
      update();

      Get.snackbar("Informasi", "Gagal Memuat Data",
          backgroundColor: Colors.redAccent, colorText: Colors.white);
    }
    this.update();
  }

  Future<int> ubahKeterangan(String no_apar) async{
      int result = 0;
    this.loadingProses = true;
    this.update();

    Map<String, dynamic> params = {};
    params["user_id"] = SessionConfig.user_id;
    params["upt_id"] = SessionConfig.upt_id;
    params["keterangan"] = this.keterangan;
    params["no_trans"] = no_apar;

    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_SIDAK][Routes.UBAHKETERANGAN]),
        body: params);

// print("DATA REQUEST ${request.body}");


    if (request.statusCode == 200) {
      this.loadingProses = false;
      update();
      var data = json.decode(request.body);
      MessageModel respMessage = MessageModel.fromJson(data);
      if (respMessage.is_valid == "1") {
        this.update();
        result = 1;
      } else {
        Get.snackbar("Informasi", "Gagal Simpan Data",
            backgroundColor: Colors.redAccent, colorText: Colors.white);
        result = 0;
      }
    } else {
      this.loadingProses = false;
      update();

      if (request.statusCode == 500) {
        Get.snackbar("Informasi", "Gagal Memuat Data ${request.body}",
            backgroundColor: Colors.redAccent, colorText: Colors.white);
      } else {
        Get.snackbar("Informasi", "Gagal Memuat Data",
            backgroundColor: Colors.redAccent, colorText: Colors.white);
      }
      result = 0;
    }

    return result;
  }
}