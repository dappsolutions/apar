

import 'dart:convert';

import 'package:apar/app/modules/response/models/message.models.dart';
import 'package:apar/config/api.config.dart';
import 'package:apar/config/modules.config.dart';
import 'package:apar/config/routes.config.dart';
import 'package:apar/config/session.config.dart';
import 'package:flutter/material.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';
import 'package:get/state_manager.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class DocumentPdfControllers extends GetxController{
  bool loadingProses = false;
  
   Future<void> getUserIdSession() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    SessionConfig.user_id = pref.getString("user_id")!;
    SessionConfig.upt_id = pref.getString("upt_id")!;
    SessionConfig.wilayah = pref.getString("wilayah")!;
    SessionConfig.id_tujuan = pref.getString("id_tujuan")!;
    SessionConfig.approval_id = pref.getString("approval_id")!;
    this.update();
  }
  
  
  Future<int> approve(String no_apar) async{
      int result = 0;
    this.loadingProses = true;
    this.update();

    Map<String, dynamic> params = {};
    params["user_id"] = SessionConfig.user_id;
    params["upt_id"] = SessionConfig.upt_id;
    params["no_trans"] = no_apar;

    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_SIDAK][Routes.APPROVE]),
        body: params);

    if (request.statusCode == 200) {
      this.loadingProses = false;
      update();
      var data = json.decode(request.body);
      MessageModel respMessage = MessageModel.fromJson(data);
      if (respMessage.is_valid == "1") {
        this.update();
        result = 1;
      } else {
        Get.snackbar("Informasi", "Gagal Simpan Data",
            backgroundColor: Colors.redAccent, colorText: Colors.white);
        result = 0;
      }
    } else {
      this.loadingProses = false;
      update();

      if (request.statusCode == 500) {
        Get.snackbar("Informasi", "Gagal Memuat Data ${request.body}",
            backgroundColor: Colors.redAccent, colorText: Colors.white);
      } else {
        Get.snackbar("Informasi", "Gagal Memuat Data",
            backgroundColor: Colors.redAccent, colorText: Colors.white);
      }
      result = 0;
    }

    return result;
  }
}