import 'dart:convert';

/// Custom business object class which contains properties to hold the detailed
/// information about the employee which will be rendered in datagrid.
class InspeksiModels {
  InspeksiModels(
      this.id, this.tanggal, this.pembersihan, this.berat, this.tekanan, this.petugas, this.expired_date);

  late final int id;
  late final String tanggal;
  late final String pembersihan;
  late final String berat;
  late final String tekanan;
  late final String petugas;
  late final String expired_date;

  InspeksiModels.fromJson(Map<String, dynamic> json) {
    this.id = int.parse(json['id'].toString());
    this.tanggal = json['tanggal'].toString();
    this.pembersihan = json['pembersihan'].toString();
    this.berat = json['berat'].toString();
    this.tekanan = json['tekanan'].toString();
    this.petugas = json['petugas'].toString();
    this.expired_date = json['expired_date'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['tanggal'] = this.tanggal;
    data['pembersihan'] = this.pembersihan;
    data['berat'] = this.berat;
    data['tekanan'] = this.tekanan;
    data['petugas'] = this.petugas;
    data['expired_date'] = this.expired_date;

    return data;
  }

  static String inspeksiToJson(List<InspeksiModels> data) =>
      json.encode(List<dynamic>.from(data.map((x) => x.toJson())));
}
