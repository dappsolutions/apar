import 'dart:convert';

/// Custom business object class which contains properties to hold the detailed
/// information about the employee which will be rendered in datagrid.
class KelengkapanModels {
  KelengkapanModels(
      this.id, this.tanggal, this.hanger, this.ketinggian_apar, this.segitiga_apar, this.indikator_tekanan, this.hose, this.tabung);

  late final int id;
  late final String tanggal;
  late final String hanger;
  late final String ketinggian_apar;
  late final String segitiga_apar;
  late final String indikator_tekanan;
  late final String hose;
  late final String tabung;

  KelengkapanModels.fromJson(Map<String, dynamic> json) {
    this.id = int.parse(json['id'].toString());
    this.tanggal = json['tanggal'].toString();
    this.hanger = json['hanger'].toString();
    this.ketinggian_apar = json['ketinggian_apar'].toString();
    this.segitiga_apar = json['segitiga_apar'].toString();
    this.indikator_tekanan = json['indikator_tekanan'].toString();
    this.hose = json['hose'].toString();
    this.tabung = json['tabung'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['tanggal'] = this.tanggal;
    data['hanger'] = this.hanger;
    data['ketinggian_apar'] = this.ketinggian_apar;
    data['segitiga_apar'] = this.segitiga_apar;
    data['indikator_tekanan'] = this.indikator_tekanan;
    data['hose'] = this.hose;
    data['tabung'] = this.tabung;

    return data;
  }

  static String inspeksiToJson(List<KelengkapanModels> data) =>
      json.encode(List<dynamic>.from(data.map((x) => x.toJson())));
}
