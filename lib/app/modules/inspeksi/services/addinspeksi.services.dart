import 'dart:convert';

import 'package:apar/app/modules/inspeksi/models/inspeksi.models.dart';
import 'package:apar/app/modules/inspeksi/models/kelengkapan.models.dart';
import 'package:apar/app/modules/response/models/message.models.dart';
import 'package:apar/config/api.config.dart';
import 'package:apar/config/modules.config.dart';
import 'package:apar/config/routes.config.dart';
import 'package:http/http.dart' as http;

class AddInspeksiServices {
  static Future<Map<String, dynamic>> simpanAll(
      Map<String, dynamic> params) async {
    Map<String, dynamic> result = {};
    result['is_valid'] = 0;
    result['no_trans'] = '';
    result['message'] = '';

    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_SIDAK][Routes.SIMPAN]),
        body: params);

    // print("SIMPAN ${request.body}");
    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      MessageModel respMessage = MessageModel.fromJson(data);
      if (respMessage.is_valid == "1") {
        result['no_trans'] = data['no_trans'];
        result['is_valid'] = 1;
        result['message'] = 'Berhasil Simpan Data';
      } else {
        result['message'] = 'Gagal Simpan Data';
        result['is_valid'] = 0;
      }
    } else {
      if (request.statusCode == 500) {
        result['message'] = 'Gagal Memuat Data 500';
      } else {
        result['message'] = 'Gagal Memuat Data';
      }
    }

    return result;
  }

  static Future<InspeksiModels> getDetailData(
      Map<String, dynamic> params) async {
    InspeksiModels dataObject = new InspeksiModels(0, '', '', '', '', '', '');
    var request = await http.post(
        Uri.parse(
            Api.route[ModulesConfig.MODULE_SIDAK][Routes.GET_DETAIL_DATA]),
        body: params);
    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      dataObject = InspeksiModels.fromJson(data['data']);
      return dataObject;
    }

    return dataObject;
  }

  static Future<KelengkapanModels> getDetailDataKelengkapan(
      Map<String, dynamic> params) async {
    KelengkapanModels dataObject = new KelengkapanModels(0, '', '', '', '', '', '', '');
    var request = await http.post(
        Uri.parse(
            Api.route[ModulesConfig.MODULE_SIDAK][Routes.GET_DETAIL_DATA]),
        body: params);
        print("REQUESt ${request.body}");
    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      if(data['data_kelengkapan'].length > 0){
        dataObject = KelengkapanModels.fromJson(data['data_kelengkapan']);
        return dataObject;
      }
    }

    return dataObject;
  }

  static Future<Map<String, dynamic>> simpanPerubahan(
      Map<String, dynamic> params) async {
    Map<String, dynamic> result = {};
    result['is_valid'] = 0;
    result['no_trans'] = '';
    result['message'] = '';

    var request = await http.post(
        Uri.parse(
            Api.route[ModulesConfig.MODULE_SIDAK][Routes.SIMPANPERUBAHAN]),
        body: params);

    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      MessageModel respMessage = MessageModel.fromJson(data);
      if (respMessage.is_valid == "1") {
        result['no_trans'] = '';
        result['is_valid'] = 1;
        result['message'] = 'Berhasil Simpan Data';
      } else {
        result['message'] = 'Gagal Simpan Data';
        result['is_valid'] = 0;
      }
    } else {
      if (request.statusCode == 500) {
        result['message'] = 'Gagal Memuat Data 500';
      } else {
        result['message'] = 'Gagal Memuat Data';
      }
    }

    return result;
  }
}
