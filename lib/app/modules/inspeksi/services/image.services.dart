import 'dart:convert';

import 'package:apar/app/modules/inspeksi/models/imagedetail.models.dart';
import 'package:apar/app/modules/response/models/message.models.dart';
import 'package:apar/config/api.config.dart';
import 'package:apar/config/modules.config.dart';
import 'package:apar/config/routes.config.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class ImageServices {
  static Future<Map<String, dynamic>> doUpload(
      Map<String, dynamic> params) async {
    Map<String, dynamic> result = {};
    result["is_valid"] = 0;
    result["message"] = '';

    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_SIDAK][Routes.SIMPANGAMBAR]),
        body: params);

    // print("DATa GAMBAR ${request.body}");
    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      MessageModel respMessage = MessageModel.fromJson(data);
      if (respMessage.is_valid == "1") {
        result["is_valid"] = 1;
        result["message"] = 'Berhasil Diupload';
      } else {
        result["message"] = 'Data Gagal diupload';
      }
    } else {
      if (request.statusCode == 500) {
        result["message"] = 'Data Gagal diupload 500';
      } else {
        result["message"] = 'Data Gagal diupload error';
      }
    }

    return result;
  }

  static Future<Map<String, dynamic>> batalPengajuan(
      Map<String, dynamic> params) async {
    Map<String, dynamic> result = {};
    result['is_valid'] = 0;
    result['message'] = '';
    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_SIDAK][Routes.BATAL]),
        body: params);

    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      MessageModel respMessage = MessageModel.fromJson(data);
      if (respMessage.is_valid == "1") {
        result['is_valid'] = 1;
        result['message'] = "Berhasil Dibatalkan";
      } else {
        result['message'] = "Gagal Dibatalkan";
      }
    } else {
      if (request.statusCode == 500) {
        result['message'] = "Gagal Dibatalkan 500";
      } else {
        result['message'] = "Gagal Dibatalkan Error";
      }
    }

    return result;
  }

  static Future<List<ImageDetailModel>> getDetailImage(
      Map<String, dynamic> params, List<ImageDetailModel> data_image) async {
    var request = await http.post(
        Uri.parse(
            Api.route[ModulesConfig.MODULE_SIDAK][Routes.GET_DETAIL_IMAGE]),
        body: params);
    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      // print("DATA IMAGE ${data['data']["id"]}");
      for (var item in data['data']) {
        data_image.add(ImageDetailModel.fromJson(item));
      }
      return data_image;
    } else {
      Get.snackbar("Informasi", "Gagal Memuat Data",
          backgroundColor: Colors.redAccent, colorText: Colors.white);
      return [];
    }
  }
}
