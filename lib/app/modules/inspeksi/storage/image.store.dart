

import 'dart:convert';
import 'dart:io';

class ImageStorage {
  late String encodeImages;
  ImageStorage({required this.encodeImages});

 Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['encodeImages'] = this.encodeImages;

    return data;
  }

  static String inspeksiToJson(List<ImageStorage> data) =>
      json.encode(List<dynamic>.from(data.map((x) => x.toJson())));
}