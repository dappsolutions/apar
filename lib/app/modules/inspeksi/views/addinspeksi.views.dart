import 'package:apar/app/modules/inspeksi/controllers/addinspeksi.controllers.dart';
import 'package:apar/app/modules/inspeksi/models/inspeksi.models.dart';
import 'package:apar/app/modules/inspeksi/views/ambilgambar.views.dart';
import 'package:apar/component/inputs.dart';
import 'package:apar/ui/Uicolor.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

class AddInspeksiViews extends StatefulWidget {
  String no_apar;
  String id_apar;
  String lokasi_tujuan;
  AddInspeksiViews(
      {required this.no_apar,
      required this.id_apar,
      required this.lokasi_tujuan});

  @override
  _AddInspeksiViewsState createState() => _AddInspeksiViewsState();
}

class _AddInspeksiViewsState extends State<AddInspeksiViews> {
  late AddInspeksiControllers controllers;
  String _selectedDate = '';

  @override
  void initState() {
    super.initState();
    controllers = new AddInspeksiControllers();
    controllers.init();
  }

  void _onSelectionChanged(DateRangePickerSelectionChangedArgs args) {
    /// The argument value will return the changed date as [DateTime] when the
    /// widget [SfDateRangeSelectionMode] set as single.
    ///
    /// The argument value will return the changed dates as [List<DateTime>]
    /// when the widget [SfDateRangeSelectionMode] set as multiple.
    ///
    /// The argument value will return the changed range as [PickerDateRange]
    /// when the widget [SfDateRangeSelectionMode] set as range.
    ///
    /// The argument value will return the changed ranges as
    /// [List<PickerDateRange] when the widget [SfDateRangeSelectionMode] set as
    /// multi range.
    setState(() {
      if (args.value is PickerDateRange) {
        _selectedDate = args.value.startDate.toString();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Form Inspeksi APAR"),
        backgroundColor: Uicolor.hexToColor(Uicolor.greenPln),
        elevation: 0,
      ),
      body: Container(
          color: Uicolor.hexToColor(Uicolor.whiteSmooth),
          child: SingleChildScrollView(
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: <
                    Widget>[
              Container(
                color: Uicolor.hexToColor(Uicolor.grey),
                alignment: Alignment.topLeft,
                padding: EdgeInsets.all(16),
                child: Text("Form Inspeksi : APAR ${widget.no_apar}",
                    style: TextStyle(
                        color: Uicolor.hexToColor(Uicolor.black_young),
                        fontSize: 16,
                        fontWeight: FontWeight.bold)),
              ),
              GetBuilder<AddInspeksiControllers>(
                init: controllers,
                builder: (params) {
                  return Container(
                    child: SfDateRangePicker(
                      onSelectionChanged: params.selectedDate,
                      selectionMode: DateRangePickerSelectionMode.range,
                    ),
                  );
                },
              ),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16),
                alignment: Alignment.topLeft,
                child: Text("Pembersihan",
                    style: TextStyle(
                        color: Uicolor.hexToColor(Uicolor.black_young),
                        fontSize: 14)),
              ),
              GetBuilder<AddInspeksiControllers>(
                init: controllers,
                builder: (params) {
                  return Container(
                      height: MediaQuery.of(context).size.height * 0.08,
                      // width: 300,
                      margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                      child: InputDecorator(
                        decoration:
                            const InputDecoration(border: OutlineInputBorder()),
                        child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                          items: params.dataPembersihan.map((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                          value: params.defaultPembersihan,
                          hint: Text("Pilih Kondisi"),
                          onChanged: (val) {
                            // setState(() {
                            controllers.defaultPembersihan = val.toString();
                            controllers.pembersihan = val.toString();
                            controllers.update();
                            // });
                          },
                        )),
                      ));
                },
              ),
              // Container(
              //     padding: EdgeInsets.only(left: 16, right: 16, top: 8),
              //     height: MediaQuery.of(context).size.height * 0.08,
              //     child: Material(
              //         borderRadius: BorderRadius.circular(3),
              //         shadowColor: Colors.grey,
              //         elevation: 0,
              //         child: TextFormField(
              //           initialValue: "",
              //           autocorrect: true,
              //           // controller: _controllerDua,
              //           decoration: InputDecoration(
              //             // prefixIcon: Icon(Icons.search),
              //             hintText: 'Pembersihan',
              //             prefixIcon: Icon(
              //               Icons.book,
              //               color: Uicolor.hexToColor(Uicolor.greenPln),
              //             ),
              //             hintStyle: TextStyle(
              //                 color: Uicolor.hexToColor(Uicolor.grey)),
              //             filled: true,
              //             fillColor: Uicolor.hexToColor(Uicolor.white),
              //             //
              //             enabledBorder: OutlineInputBorder(
              //               borderRadius:
              //                   BorderRadius.all(Radius.circular(8.0)),
              //               borderSide:
              //                   BorderSide(color: Colors.white, width: 1),
              //             ),
              //             focusedBorder: OutlineInputBorder(
              //               borderRadius:
              //                   BorderRadius.all(Radius.circular(8.0)),
              //               borderSide: BorderSide(color: Colors.white),
              //             ),
              //           ),
              //           onFieldSubmitted: (value) async {},
              //           onChanged: (value) {
              //             controllers.pembersihan = value.toString();
              //             controllers.update();
              //           },
              //           onTap: () async {},
              //         ))),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                alignment: Alignment.topLeft,
                child: Text("Berat Apar (Kg)",
                    style: TextStyle(
                        color: Uicolor.hexToColor(Uicolor.black_young),
                        fontSize: 14)),
              ),
              Container(
                  padding: EdgeInsets.only(left: 16, right: 16, top: 8),
                  height: MediaQuery.of(context).size.height * 0.08,
                  child: Material(
                      borderRadius: BorderRadius.circular(3),
                      shadowColor: Colors.grey,
                      elevation: 0,
                      child: TextFormField(
                        initialValue: "",
                        autocorrect: true,
                        keyboardType: TextInputType.number,
                        // controller: _controllerDua,
                        decoration: InputDecoration(
                          // prefixIcon: Icon(Icons.search),
                          hintText: 'Berat Apar',
                          prefixIcon: Icon(
                            Icons.book,
                            color: Uicolor.hexToColor(Uicolor.greenPln),
                          ),
                          hintStyle: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.grey)),
                          filled: true,
                          fillColor: Uicolor.hexToColor(Uicolor.white),
                          //
                          enabledBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            borderSide:
                                BorderSide(color: Colors.white, width: 1),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            borderSide: BorderSide(color: Colors.white),
                          ),
                        ),
                        onFieldSubmitted: (value) async {},
                        onChanged: (value) {
                          controllers.berat = value.toString();
                          controllers.update();
                        },
                        onTap: () async {},
                      ))),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                alignment: Alignment.topLeft,
                child: Text("Tekanan Tabung",
                    style: TextStyle(
                        color: Uicolor.hexToColor(Uicolor.black_young),
                        fontSize: 14)),
              ),
              Container(
                  padding: EdgeInsets.only(left: 16, right: 16, top: 8),
                  height: MediaQuery.of(context).size.height * 0.08,
                  child: Material(
                      borderRadius: BorderRadius.circular(3),
                      shadowColor: Colors.grey,
                      elevation: 0,
                      child: TextFormField(
                        initialValue: "",
                        autocorrect: true,
                        // controller: _controllerDua,
                        decoration: InputDecoration(
                          // prefixIcon: Icon(Icons.search),
                          hintText: 'Tekanan Tabung',
                          prefixIcon: Icon(
                            Icons.book,
                            color: Uicolor.hexToColor(Uicolor.greenPln),
                          ),
                          hintStyle: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.grey)),
                          filled: true,
                          fillColor: Uicolor.hexToColor(Uicolor.white),
                          //
                          enabledBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            borderSide:
                                BorderSide(color: Colors.white, width: 1),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            borderSide: BorderSide(color: Colors.white),
                          ),
                        ),
                        onFieldSubmitted: (value) async {},
                        onChanged: (value) {
                          controllers.tekanan = value.toString();
                          controllers.update();
                        },
                        onTap: () async {},
                      ))),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                alignment: Alignment.topLeft,
                child: Text("Hanger",
                    style: TextStyle(
                        color: Uicolor.hexToColor(Uicolor.black_young),
                        fontSize: 14)),
              ),
              GetBuilder<AddInspeksiControllers>(
                init: controllers,
                builder: (params) {
                  return Container(
                      height: MediaQuery.of(context).size.height * 0.08,
                      // width: 300,
                      margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                      child: InputDecorator(
                        decoration:
                            const InputDecoration(border: OutlineInputBorder()),
                        child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                          items: params.dataHanger.map((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                          value: params.defaultHanger,
                          hint: Text("Pilih Kondisi"),
                          onChanged: (val) {
                            // setState(() {
                            controllers.defaultHanger = val.toString();
                            controllers.hanger = val.toString();
                            controllers.update();
                            // });
                          },
                        )),
                      ));
                },
              ),

              // Container(
              //     padding: EdgeInsets.only(left: 16, right: 16, top: 8),
              //     height: MediaQuery.of(context).size.height * 0.08,
              //     child: Material(
              //         borderRadius: BorderRadius.circular(3),
              //         shadowColor: Colors.grey,
              //         elevation: 0,
              //         child: TextFormField(
              //           initialValue: "",
              //           autocorrect: true,
              //           // controller: _controllerDua,
              //           decoration: InputDecoration(
              //             // prefixIcon: Icon(Icons.search),
              //             hintText: 'Hanger',
              //             prefixIcon: Icon(
              //               Icons.book,
              //               color: Uicolor.hexToColor(Uicolor.greenPln),
              //             ),
              //             hintStyle: TextStyle(
              //                 color: Uicolor.hexToColor(Uicolor.grey)),
              //             filled: true,
              //             fillColor: Uicolor.hexToColor(Uicolor.white),
              //             //
              //             enabledBorder: OutlineInputBorder(
              //               borderRadius:
              //                   BorderRadius.all(Radius.circular(8.0)),
              //               borderSide:
              //                   BorderSide(color: Colors.white, width: 1),
              //             ),
              //             focusedBorder: OutlineInputBorder(
              //               borderRadius:
              //                   BorderRadius.all(Radius.circular(8.0)),
              //               borderSide: BorderSide(color: Colors.white),
              //             ),
              //           ),
              //           onFieldSubmitted: (value) async {},
              //           onChanged: (value) {
              //             controllers.hanger = value.toString();
              //             controllers.update();
              //           },
              //           onTap: () async {},
              //         ))),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                alignment: Alignment.topLeft,
                child: Text("Ketinggian Apar",
                    style: TextStyle(
                        color: Uicolor.hexToColor(Uicolor.black_young),
                        fontSize: 14)),
              ),
              GetBuilder<AddInspeksiControllers>(
                init: controllers,
                builder: (params) {
                  return Container(
                      height: MediaQuery.of(context).size.height * 0.08,
                      // width: 300,
                      margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                      child: InputDecorator(
                        decoration:
                            const InputDecoration(border: OutlineInputBorder()),
                        child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                          items: params.dataKetinggianApar.map((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                          value: params.defaultKetinggianApar,
                          hint: Text("Pilih Kondisi"),
                          onChanged: (val) {
                            // setState(() {
                            controllers.defaultKetinggianApar = val.toString();
                            controllers.ketinggian_apar = val.toString();
                            controllers.update();
                            // });
                          },
                        )),
                      ));
                },
              ),
              // Container(
              //     padding: EdgeInsets.only(left: 16, right: 16, top: 8),
              //     height: MediaQuery.of(context).size.height * 0.08,
              //     child: Material(
              //         borderRadius: BorderRadius.circular(3),
              //         shadowColor: Colors.grey,
              //         elevation: 0,
              //         child: TextFormField(
              //           initialValue: "",
              //           autocorrect: true,
              //           // controller: _controllerDua,
              //           decoration: InputDecoration(
              //             // prefixIcon: Icon(Icons.search),
              //             hintText: 'Ketinggian Apar',
              //             prefixIcon: Icon(
              //               Icons.book,
              //               color: Uicolor.hexToColor(Uicolor.greenPln),
              //             ),
              //             hintStyle: TextStyle(
              //                 color: Uicolor.hexToColor(Uicolor.grey)),
              //             filled: true,
              //             fillColor: Uicolor.hexToColor(Uicolor.white),
              //             //
              //             enabledBorder: OutlineInputBorder(
              //               borderRadius:
              //                   BorderRadius.all(Radius.circular(8.0)),
              //               borderSide:
              //                   BorderSide(color: Colors.white, width: 1),
              //             ),
              //             focusedBorder: OutlineInputBorder(
              //               borderRadius:
              //                   BorderRadius.all(Radius.circular(8.0)),
              //               borderSide: BorderSide(color: Colors.white),
              //             ),
              //           ),
              //           onFieldSubmitted: (value) async {},
              //           onChanged: (value) {
              //             controllers.ketinggian_apar = value.toString();
              //             controllers.update();
              //           },
              //           onTap: () async {},
              //         ))),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                alignment: Alignment.topLeft,
                child: Text("Segitiga Apar",
                    style: TextStyle(
                        color: Uicolor.hexToColor(Uicolor.black_young),
                        fontSize: 14)),
              ),
              GetBuilder<AddInspeksiControllers>(
                init: controllers,
                builder: (params) {
                  return Container(
                      height: MediaQuery.of(context).size.height * 0.08,
                      // width: 300,
                      margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                      child: InputDecorator(
                        decoration:
                            const InputDecoration(border: OutlineInputBorder()),
                        child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                          items: params.dataSegitigaApar.map((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                          value: params.defaultSegitigaApar,
                          hint: Text("Pilih Kondisi"),
                          onChanged: (val) {
                            // setState(() {
                            controllers.defaultSegitigaApar = val.toString();
                            controllers.segitiga_apar = val.toString();
                            controllers.update();
                            // });
                          },
                        )),
                      ));
                },
              ),
              // Container(
              //     padding: EdgeInsets.only(left: 16, right: 16, top: 8),
              //     height: MediaQuery.of(context).size.height * 0.08,
              //     child: Material(
              //         borderRadius: BorderRadius.circular(3),
              //         shadowColor: Colors.grey,
              //         elevation: 0,
              //         child: TextFormField(
              //           initialValue: "",
              //           autocorrect: true,
              //           // controller: _controllerDua,
              //           decoration: InputDecoration(
              //             // prefixIcon: Icon(Icons.search),
              //             hintText: 'Segitiga Apar',
              //             prefixIcon: Icon(
              //               Icons.book,
              //               color: Uicolor.hexToColor(Uicolor.greenPln),
              //             ),
              //             hintStyle: TextStyle(
              //                 color: Uicolor.hexToColor(Uicolor.grey)),
              //             filled: true,
              //             fillColor: Uicolor.hexToColor(Uicolor.white),
              //             //
              //             enabledBorder: OutlineInputBorder(
              //               borderRadius:
              //                   BorderRadius.all(Radius.circular(8.0)),
              //               borderSide:
              //                   BorderSide(color: Colors.white, width: 1),
              //             ),
              //             focusedBorder: OutlineInputBorder(
              //               borderRadius:
              //                   BorderRadius.all(Radius.circular(8.0)),
              //               borderSide: BorderSide(color: Colors.white),
              //             ),
              //           ),
              //           onFieldSubmitted: (value) async {},
              //           onChanged: (value) {
              //             controllers.segitiga_apar = value.toString();
              //             controllers.update();
              //           },
              //           onTap: () async {},
              //         ))),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                alignment: Alignment.topLeft,
                child: Text("Indikator Tekanan",
                    style: TextStyle(
                        color: Uicolor.hexToColor(Uicolor.black_young),
                        fontSize: 14)),
              ),
              GetBuilder<AddInspeksiControllers>(
                init: controllers,
                builder: (params) {
                  return Container(
                      height: MediaQuery.of(context).size.height * 0.08,
                      // width: 300,
                      margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                      child: InputDecorator(
                        decoration:
                            const InputDecoration(border: OutlineInputBorder()),
                        child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                          items:
                              params.dataIndikatorTekanan.map((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                          value: params.defaultIndikatorTekanan,
                          hint: Text("Pilih Kondisi"),
                          onChanged: (val) {
                            // setState(() {
                            controllers.defaultIndikatorTekanan =
                                val.toString();
                            controllers.indikator_tekanan = val.toString();
                            controllers.update();
                            // });
                          },
                        )),
                      ));
                },
              ),
              // Container(
              //     padding: EdgeInsets.only(left: 16, right: 16, top: 8),
              //     height: MediaQuery.of(context).size.height * 0.08,
              //     child: Material(
              //         borderRadius: BorderRadius.circular(3),
              //         shadowColor: Colors.grey,
              //         elevation: 0,
              //         child: TextFormField(
              //           initialValue: "",
              //           autocorrect: true,
              //           // controller: _controllerDua,
              //           decoration: InputDecoration(
              //             // prefixIcon: Icon(Icons.search),
              //             hintText: 'Indikator Tekanan',
              //             prefixIcon: Icon(
              //               Icons.book,
              //               color: Uicolor.hexToColor(Uicolor.greenPln),
              //             ),
              //             hintStyle: TextStyle(
              //                 color: Uicolor.hexToColor(Uicolor.grey)),
              //             filled: true,
              //             fillColor: Uicolor.hexToColor(Uicolor.white),
              //             //
              //             enabledBorder: OutlineInputBorder(
              //               borderRadius:
              //                   BorderRadius.all(Radius.circular(8.0)),
              //               borderSide:
              //                   BorderSide(color: Colors.white, width: 1),
              //             ),
              //             focusedBorder: OutlineInputBorder(
              //               borderRadius:
              //                   BorderRadius.all(Radius.circular(8.0)),
              //               borderSide: BorderSide(color: Colors.white),
              //             ),
              //           ),
              //           onFieldSubmitted: (value) async {},
              //           onChanged: (value) {
              //             controllers.indikator_tekanan = value.toString();
              //             controllers.update();
              //           },
              //           onTap: () async {},
              //         ))),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                alignment: Alignment.topLeft,
                child: Text("Hose",
                    style: TextStyle(
                        color: Uicolor.hexToColor(Uicolor.black_young),
                        fontSize: 14)),
              ),
              GetBuilder<AddInspeksiControllers>(
                init: controllers,
                builder: (params) {
                  return Container(
                      height: MediaQuery.of(context).size.height * 0.08,
                      // width: 300,
                      margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                      child: InputDecorator(
                        decoration:
                            const InputDecoration(border: OutlineInputBorder()),
                        child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                          items: params.dataHose.map((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                          value: params.defaultHose,
                          hint: Text("Pilih Kondisi"),
                          onChanged: (val) {
                            // setState(() {
                            controllers.defaultHose = val.toString();
                            controllers.hose = val.toString();
                            controllers.update();
                            // });
                          },
                        )),
                      ));
                },
              ),
              // Container(
              //     padding: EdgeInsets.only(left: 16, right: 16, top: 8),
              //     height: MediaQuery.of(context).size.height * 0.08,
              //     child: Material(
              //         borderRadius: BorderRadius.circular(3),
              //         shadowColor: Colors.grey,
              //         elevation: 0,
              //         child: TextFormField(
              //           initialValue: "",
              //           autocorrect: true,
              //           // controller: _controllerDua,
              //           decoration: InputDecoration(
              //             // prefixIcon: Icon(Icons.search),
              //             hintText: 'Hose',
              //             prefixIcon: Icon(
              //               Icons.book,
              //               color: Uicolor.hexToColor(Uicolor.greenPln),
              //             ),
              //             hintStyle: TextStyle(
              //                 color: Uicolor.hexToColor(Uicolor.grey)),
              //             filled: true,
              //             fillColor: Uicolor.hexToColor(Uicolor.white),
              //             //
              //             enabledBorder: OutlineInputBorder(
              //               borderRadius:
              //                   BorderRadius.all(Radius.circular(8.0)),
              //               borderSide:
              //                   BorderSide(color: Colors.white, width: 1),
              //             ),
              //             focusedBorder: OutlineInputBorder(
              //               borderRadius:
              //                   BorderRadius.all(Radius.circular(8.0)),
              //               borderSide: BorderSide(color: Colors.white),
              //             ),
              //           ),
              //           onFieldSubmitted: (value) async {},
              //           onChanged: (value) {
              //             controllers.hose = value.toString();
              //             controllers.update();
              //           },
              //           onTap: () async {},
              //         ))),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                alignment: Alignment.topLeft,
                child: Text("Tabung",
                    style: TextStyle(
                        color: Uicolor.hexToColor(Uicolor.black_young),
                        fontSize: 14)),
              ),
              GetBuilder<AddInspeksiControllers>(
                init: controllers,
                builder: (params) {
                  return Container(
                      height: MediaQuery.of(context).size.height * 0.08,
                      // width: 300,
                      margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                      child: InputDecorator(
                        decoration:
                            const InputDecoration(border: OutlineInputBorder()),
                        child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                          items: params.dataTabung.map((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                          value: params.defaultTabung,
                          hint: Text("Pilih Kondisi"),
                          onChanged: (val) {
                            // setState(() {
                            controllers.defaultTabung = val.toString();
                            controllers.tabung = val.toString();
                            controllers.update();
                            // });
                          },
                        )),
                      ));
                },
              ),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                alignment: Alignment.topLeft,
                child: Text("Petugas",
                    style: TextStyle(
                        color: Uicolor.hexToColor(Uicolor.black_young),
                        fontSize: 14)),
              ),
              Container(
                  padding: EdgeInsets.only(left: 16, right: 16, top: 8),
                  height: MediaQuery.of(context).size.height * 0.08,
                  child: Material(
                      borderRadius: BorderRadius.circular(3),
                      shadowColor: Colors.grey,
                      elevation: 0,
                      child: TextFormField(
                        initialValue: "",
                        autocorrect: true,
                        // controller: _controllerDua,
                        decoration: InputDecoration(
                          // prefixIcon: Icon(Icons.search),
                          hintText: 'Petugas',
                          prefixIcon: Icon(
                            Icons.person,
                            color: Uicolor.hexToColor(Uicolor.greenPln),
                          ),
                          hintStyle: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.grey)),
                          filled: true,
                          fillColor: Uicolor.hexToColor(Uicolor.white),
                          //
                          enabledBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            borderSide:
                                BorderSide(color: Colors.white, width: 1),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            borderSide: BorderSide(color: Colors.white),
                          ),
                        ),
                        onFieldSubmitted: (value) async {},
                        onChanged: (value) {
                          controllers.petugas = value.toString();
                          controllers.update();
                        },
                        onTap: () async {},
                      ))),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                alignment: Alignment.topLeft,
                child: Text("Expired Date",
                    style: TextStyle(
                        color: Uicolor.hexToColor(Uicolor.black_young),
                        fontSize: 14)),
              ),
              Container(
                padding: EdgeInsets.only(top: 8),
                height: MediaQuery.of(context).size.height * 0.08,
                child: GetBuilder<AddInspeksiControllers>(
                  init: controllers,
                  builder: (params) {
                    return InputComponent.textDatePicker((date) {
                      params.expired_date = date.toString();
                      params.update();
                    },
                        context: context,
                        hintText: params.expired_date,
                        initVal: params.expired_date);
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                height: MediaQuery.of(context).size.height * 0.07,
                width: double.infinity,
                // height: 50,
                child: RaisedButton(
                  child: Text(
                    "SUBMIT",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.orangeAccent,
                  onPressed: () async {
                    //submit add to datagrid
                    if (controllers.tanggal == "") {
                      Get.snackbar("Informasi", "Tanggal Belum Dipilih",
                          backgroundColor: Colors.redAccent,
                          colorText: Colors.white);
                      return;
                    }
                    controllers.addDataFormToDataInspeksi();
                    controllers.addDataFormToDataKelengkapanInspeksi();
                  },
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(3.0),
                  ),
                ),
              ),
              SizedBox(
                height: 24,
              ),
              Container(
                color: Uicolor.hexToColor(Uicolor.grey),
                alignment: Alignment.topLeft,
                padding: EdgeInsets.all(16),
                child: Text("Data Inspeksi Masuk",
                    style: TextStyle(
                        color: Uicolor.hexToColor(Uicolor.black_young),
                        fontSize: 16,
                        fontWeight: FontWeight.bold)),
              ),
              Divider(),
              GetBuilder<AddInspeksiControllers>(
                init: controllers,
                builder: (params) {
                  return Container(
                      margin: EdgeInsets.only(top: 16),
                      height: 200,
                      child: SfDataGrid(
                        source: params.inspeksiDataSource,
                        columnWidthMode: ColumnWidthMode.fill,
                        columns: <GridColumn>[
                          GridColumn(
                              columnName: 'id',
                              label: Container(
                                  padding: EdgeInsets.all(16.0),
                                  alignment: Alignment.center,
                                  child: Text(
                                    'No',
                                    style: TextStyle(fontSize: 12),
                                  ))),
                          GridColumn(
                              columnName: 'tanggal',
                              label: Container(
                                  padding: EdgeInsets.all(16.0),
                                  alignment: Alignment.center,
                                  child: Text('Tanggal',
                                      style: TextStyle(fontSize: 12)))),
                          GridColumn(
                              columnName: 'pembersihan',
                              label: Container(
                                  padding: EdgeInsets.all(8.0),
                                  alignment: Alignment.center,
                                  child: Text('Pembersihan',
                                      style: TextStyle(fontSize: 12)))),
                          GridColumn(
                              columnName: 'berat',
                              label: Container(
                                  padding: EdgeInsets.all(8.0),
                                  alignment: Alignment.center,
                                  child: Text(
                                    'Berat Apar',
                                    style: TextStyle(fontSize: 12),
                                  ))),
                          GridColumn(
                              columnName: 'tekanan',
                              label: Container(
                                  padding: EdgeInsets.all(8.0),
                                  alignment: Alignment.center,
                                  child: Text('Tekanan Tabung',
                                      style: TextStyle(fontSize: 12)))),
                        ],
                      ));
                },
              ),
              Container(
                color: Uicolor.hexToColor(Uicolor.grey),
                alignment: Alignment.topLeft,
                padding: EdgeInsets.all(16),
                child: Text("Data Kelengkapan Inspeksi",
                    style: TextStyle(
                        color: Uicolor.hexToColor(Uicolor.black_young),
                        fontSize: 16,
                        fontWeight: FontWeight.bold)),
              ),
              GetBuilder<AddInspeksiControllers>(
                init: controllers,
                builder: (params) {
                  return Container(
                      margin: EdgeInsets.only(top: 16),
                      height: 200,
                      child: SfDataGrid(
                        source: params.kelengkapanDataSource,
                        columnWidthMode: ColumnWidthMode.fill,
                        columns: <GridColumn>[
                          GridColumn(
                              columnName: 'id',
                              label: Container(
                                  padding: EdgeInsets.all(16.0),
                                  alignment: Alignment.center,
                                  child: Text(
                                    'No',
                                    style: TextStyle(fontSize: 12),
                                    overflow: TextOverflow.fade,
                                  ))),
                          GridColumn(
                              columnName: 'tanggal',
                              label: Container(
                                  padding: EdgeInsets.all(16.0),
                                  alignment: Alignment.center,
                                  child: Text(
                                    'Tanggal',
                                    style: TextStyle(fontSize: 12),
                                    overflow: TextOverflow.fade,
                                  ))),
                          GridColumn(
                              columnName: 'hanger',
                              label: Container(
                                  padding: EdgeInsets.all(8.0),
                                  alignment: Alignment.center,
                                  child: Text(
                                    'Hanger',
                                    style: TextStyle(fontSize: 12),
                                    overflow: TextOverflow.fade,
                                  ))),
                          GridColumn(
                              columnName: 'ketinggian apar',
                              label: Container(
                                  padding: EdgeInsets.all(8.0),
                                  alignment: Alignment.center,
                                  child: Text(
                                    'Ketinggian Apar',
                                    style: TextStyle(fontSize: 12),
                                    overflow: TextOverflow.fade,
                                  ))),
                          GridColumn(
                              columnName: 'segitiga apar',
                              label: Container(
                                  padding: EdgeInsets.all(8.0),
                                  alignment: Alignment.center,
                                  child: Text(
                                    'Segitiga Apar',
                                    style: TextStyle(fontSize: 12),
                                    overflow: TextOverflow.fade,
                                  ))),
                          GridColumn(
                              columnName: 'indikator tekanan',
                              label: Container(
                                  padding: EdgeInsets.all(8.0),
                                  alignment: Alignment.center,
                                  child: Text(
                                    'Indikator Tekanan',
                                    style: TextStyle(fontSize: 12),
                                    overflow: TextOverflow.fade,
                                  ))),
                          GridColumn(
                              columnName: 'hose',
                              label: Container(
                                  padding: EdgeInsets.all(8.0),
                                  alignment: Alignment.center,
                                  child: Text(
                                    'Hose',
                                    style: TextStyle(fontSize: 12),
                                    overflow: TextOverflow.fade,
                                  ))),
                          GridColumn(
                              columnName: 'tabung',
                              label: Container(
                                  padding: EdgeInsets.all(8.0),
                                  alignment: Alignment.center,
                                  child: Text(
                                    'Tabung',
                                    style: TextStyle(fontSize: 12),
                                    overflow: TextOverflow.fade,
                                  ))),
                        ],
                      ));
                },
              ),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                alignment: Alignment.topLeft,
                child: Text("Keterangan",
                    style: TextStyle(
                        color: Uicolor.hexToColor(Uicolor.black_young),
                        fontSize: 14)),
              ),
              Container(
                  padding: EdgeInsets.only(left: 16, right: 16, top: 8),
                  height: 250,
                  child: Material(
                      borderRadius: BorderRadius.circular(3),
                      shadowColor: Colors.grey,
                      elevation: 0,
                      child: TextFormField(
                        initialValue: "",
                        autocorrect: true,
                        keyboardType: TextInputType.multiline,
                        maxLines: null,
                        // controller: _controllerDua,
                        decoration: InputDecoration(
                          // prefixIcon: Icon(Icons.search),
                          hintText: 'Keterangan',
                          prefixIcon: Icon(
                            Icons.book,
                            color: Uicolor.hexToColor(Uicolor.greenPln),
                          ),
                          hintStyle: TextStyle(
                              color: Uicolor.hexToColor(Uicolor.grey)),
                          filled: true,
                          fillColor: Uicolor.hexToColor(Uicolor.white),
                          //
                          enabledBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            borderSide:
                                BorderSide(color: Colors.white, width: 1),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                            borderSide: BorderSide(color: Colors.white),
                          ),
                          contentPadding: new EdgeInsets.symmetric(
                              vertical: 35.0, horizontal: 10.0),
                        ),
                        onFieldSubmitted: (value) async {},
                        onChanged: (value) {
                          controllers.keterangan = value.toString();
                          controllers.update();
                        },
                        onTap: () async {},
                      ))),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                height: MediaQuery.of(context).size.height * 0.07,
                width: double.infinity,
                // height: 50,
                child: RaisedButton(
                  child: Text(
                    "LANJUT PROSES",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.orangeAccent,
                  onPressed: () async {
                    if (controllers.keterangan == '') {
                      Get.snackbar("Informasi", "Keterangan Harus Ada",
                          backgroundColor: Colors.redAccent,
                          colorText: Colors.white);
                      return;
                    }
                    if (controllers.inspeksiData.length > 0 &&
                        controllers.kelengkapanData.length > 0) {
                      int status = await controllers.simpanAll(
                          widget.no_apar, widget.id_apar, widget.lokasi_tujuan);
                      if (status == 1) {
                        Get.to(AmbilGambarViews(
                          dataInspeksi: controllers.inspeksiData,
                          no_transaksi: controllers.no_transaksi,
                          dataKelengkapan: controllers.kelengkapanData,
                        ));
                      }
                    } else {
                      if (controllers.inspeksiData.length == 0) {
                        Get.snackbar("Informasi", "Data Inspeksi Harus Ada",
                            backgroundColor: Colors.redAccent,
                            colorText: Colors.white);
                      }
                      if (controllers.kelengkapanData.length == 0) {
                        Get.snackbar("Informasi", "Data Kelengkapan Harus Ada",
                            backgroundColor: Colors.redAccent,
                            colorText: Colors.white);
                      }
                    }
                  },
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(3.0),
                  ),
                ),
              ),
            ]),
          )),
    );
  }
}
