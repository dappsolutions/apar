import 'package:apar/app/modules/inspeksi/controllers/image.controllers.dart';
import 'package:apar/app/modules/inspeksi/models/inspeksi.models.dart';
import 'package:apar/app/modules/inspeksi/models/kelengkapan.models.dart';
import 'package:apar/app/modules/main/views/dashboard.views.dart';
import 'package:apar/ui/Uicolor.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AmbilGambarViews extends StatefulWidget {
  List<InspeksiModels> dataInspeksi;
  List<KelengkapanModels> dataKelengkapan;
  String no_transaksi;
  AmbilGambarViews({required this.dataInspeksi , required this.no_transaksi, required this.dataKelengkapan});


  @override
  _AmbilGambarViewsState createState() => _AmbilGambarViewsState();
}

class _AmbilGambarViewsState extends State<AmbilGambarViews> {
  late ImageControllers controllers;
  
  @override
  void initState() {    
    super.initState();
    controllers = new ImageControllers();
    controllers.clearDataImg();
  }

   @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async{
          Get.defaultDialog(
              title: "Konfirmasi",
              middleText: "Apakah anda yakin akan membuang data ini ?",
              textConfirm: "ya",
              textCancel: "Tidak",
              confirmTextColor: Colors.white,
              radius: 6,
              onConfirm: () async {
                Navigator.pop(context);
                int result = await controllers.batalPengajuan(
                    widget.no_transaksi);
                if (result == 1) {
                  Get.snackbar("Informasi", "Data Berhasil Dibatalkan",
                      backgroundColor: Colors.green, colorText: Colors.white);
                  await Future.delayed(Duration(seconds: 2));
                  Get.offAll(DashboardViews());
                } else {
                  Get.snackbar("Informasi", "Simpan Gagal",
                      backgroundColor: Colors.red, colorText: Colors.white);
                }
              });
          return false;
        },
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            title: Text("GAMBAR INSPEKSI APAR"),
            backgroundColor: Uicolor.hexToColor(Uicolor.greenPln),
            elevation: 0,
            centerTitle: true,
            // iconTheme: IconThemeData(
            //   color: Uicolor.hexToColor(Uicolor.black)
            // ),
          ),
          body: Container(
              child: SingleChildScrollView(
                  child: Column(
            children: <Widget>[
               GetBuilder<ImageControllers>(
                init: controllers,
                builder: (params){
                  return Container(
                    alignment: Alignment.topLeft,
                    padding: EdgeInsets.all(16),
                    child: Text("Jumlah Foto Terupload : ${params.jumlahGambarDiupload}", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                  );
                }
              ),
              GetBuilder<ImageControllers>(
                init: controllers,
                builder: (params) {
                  print("PARAMS LOADING SIMPAn${params.loadingSimpan}");
                  if (params.loadingSimpan) {
                    return Container(
                        margin: EdgeInsets.only(top: 16),
                        child: Center(
                          child: CircularProgressIndicator(),
                        ));
                  } else {
                    if (params.data_img.length > 0) {
                      return Container(
                          child: Column(
                        children: <Widget>[
                          ListView.builder(
                            shrinkWrap: true,
                            itemCount: params.data_img.length,
                            physics: ClampingScrollPhysics(),
                            itemBuilder: (context, item) {
                              // return Container(
                              //     child:Text(params.data_img[item])
                              // );
                              var data = params.data_img[item];
                              return Container(
                                  margin: EdgeInsets.only(
                                      left: 16, right: 16, top: 8),
                                  child: Column(
                                    children: <Widget>[
                                      Container(
                                        child: Center(
                                            child: Image.file(data.file)),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(top: 8),
                                        height:
                                            MediaQuery.of(context).size.height *
                                                0.07,
                                        width: double.infinity,
                                        // height: 50,
                                        child: RaisedButton(
                                          child: Text(
                                            "UPLOAD",
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                          color: Colors.orangeAccent,
                                          onPressed: () async {
                                            int doUpload =
                                                await controllers.doUpload(
                                                    widget.no_transaksi,
                                                    item);
                                            if (doUpload == 1) {
                                              Get.snackbar("Informasi",
                                                  "Image ${item + 1} Berhasil Hasil Di Upload",
                                                  backgroundColor:
                                                      Colors.greenAccent,
                                                  colorText: Colors.white);
                                              controllers
                                                  .jumlahGambarDiupload += 1;
                                              controllers.update();
                                              controllers.clearDataImgAt(item);
                                            }
                                          },
                                          elevation: 0,
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(3.0),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(top: 6),
                                        child: GestureDetector(
                                          child: Icon(
                                            Icons.close,
                                            color: Uicolor.hexToColor(
                                                Uicolor.grey),
                                          ),
                                          onTap: () {
                                            //hapus
                                            controllers.clearDataImgAt(item);
                                          },
                                        ),
                                      )
                                    ],
                                  ));
                            },
                          ),
                          controllers.widgetImgPick()
                        ],
                      ));
                    }
                  }

                  return controllers.widgetImgPick();
                },
              ),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 8),
                height: MediaQuery.of(context).size.height * 0.07,
                width: double.infinity,
                // height: 50,
                child: RaisedButton(
                  child: Text(
                    "SELESAI",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.orangeAccent,
                  elevation: 0,
                  onPressed: () async {
                    // print("DATA IMAGE ${controllers.data_img_store}");
                    if(controllers.jumlahGambarDiupload > 0){
                       Get.snackbar("Informasi", "Proses Inspeksi Sukses",
                              backgroundColor: Colors.greenAccent,
                              colorText: Colors.white);
                      await Future.delayed(Duration(seconds: 2));
                      Get.offAll(DashboardViews());
                    }else{
                      Get.snackbar("Informasi", "Gambar Inspeksi Belum Ada yang Di Upload",
                              backgroundColor: Colors.redAccent,
                              colorText: Colors.white);
                    }
                  },
                  // elevation: 3,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(3.0),
                  ),
                ),
              ),
            ],
          ))),
        ));
  }
}