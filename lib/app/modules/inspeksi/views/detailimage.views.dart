import 'package:apar/app/modules/inspeksi/controllers/detailimage.controllers.dart';
import 'package:apar/app/modules/inspeksi/controllers/image.controllers.dart';
import 'package:apar/app/modules/inspeksi/views/addinspeksi.views.dart';
import 'package:apar/app/modules/inspeksi/views/documentpdf.views.dart';
import 'package:apar/ui/Uicolor.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DetailImageViews extends StatefulWidget {
  String no_apar;
  String no_alat;
  String id_alat;
  String lokasi_tujuan;
  DetailImageViews(
      {required this.no_apar,
      required this.no_alat,
      required this.id_alat,
      required this.lokasi_tujuan});

  @override
  _DetailImageViewsState createState() => _DetailImageViewsState();
}

class _DetailImageViewsState extends State<DetailImageViews> {
  late DetailImageControllers controllers;

  @override
  void initState() {
    super.initState();
    controllers = new DetailImageControllers();
    controllers.clearDataImg();
    controllers.getDetailImage(widget.no_apar);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          Get.back();
          return false;
        },
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            title: Text("DETAIL GAMBAR INSPEKSI APAR"),
            backgroundColor: Uicolor.hexToColor(Uicolor.greenPln),
            elevation: 0,
            centerTitle: true,
            // iconTheme: IconThemeData(
            //   color: Uicolor.hexToColor(Uicolor.black)
            // ),
          ),
          body: Container(
              child: SingleChildScrollView(
                  child: Column(
            children: <Widget>[
              GetBuilder<DetailImageControllers>(
                init: controllers,
                builder: (params) {
                  if (!params.loadingProses) {
                    if (params.data_img_all.length > 0) {
                      return Container(
                          child: Column(
                        children: <Widget>[
                          ListView.builder(
                            shrinkWrap: true,
                            itemCount: params.data_img_all.length,
                            physics: ClampingScrollPhysics(),
                            itemBuilder: (context, item) {
                              // return Container(
                              //     child:Text(params.data_img[item])
                              // );
                              var data = params.data_img_all[item];
                              return Container(
                                  margin: EdgeInsets.only(
                                      left: 16, right: 16, top: 8),
                                  child: Column(
                                    children: <Widget>[
                                      Container(
                                        child: Center(
                                            child: Image.memory(
                                                data.encoddecodeBase64Image)),
                                      ),
                                    ],
                                  ));
                            },
                          ),
                          Container(
                            margin:
                                EdgeInsets.only(left: 24, right: 24, top: 16),
                            height: MediaQuery.of(context).size.height * 0.07,
                            width: double.infinity,
                            // height: 50,
                            child: RaisedButton(
                              child: Text(
                                "LAKUKAN PEMELIHARAAN KEMBALI",
                                style: TextStyle(color: Colors.white),
                              ),
                              color: Colors.orangeAccent,
                              onPressed: () async {
                                Get.to(AddInspeksiViews(
                                  id_apar: widget.id_alat,
                                  no_apar: widget.no_alat,
                                  lokasi_tujuan: widget.lokasi_tujuan,
                                ));
                              },
                              elevation: 0,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(3.0),
                              ),
                            ),
                          ),
                        ],
                      ));
                    }
                  }

                  if (params.loadingProses) {
                    return Container(
                      margin: EdgeInsets.only(top: 120),
                      child: Center(
                        child: CircularProgressIndicator(),
                      ),
                    );
                  }

                  return Container(
                    margin: EdgeInsets.only(left: 24, right: 24, top: 16),
                    height: MediaQuery.of(context).size.height * 0.07,
                    width: double.infinity,
                    // height: 50,
                    child: RaisedButton(
                      child: Text(
                        "LAKUKAN PEMELIHARAAN KEMBALI",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.orangeAccent,
                      onPressed: () async {
                        Get.to(AddInspeksiViews(
                          id_apar: widget.id_alat,
                          no_apar: widget.no_alat,
                          lokasi_tujuan: widget.lokasi_tujuan,
                        ));
                      },
                      elevation: 0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(3.0),
                      ),
                    ),
                  );
                },
              )
            ],
          ))),
          floatingActionButton: FloatingActionButton(
              child: Icon(Icons.assignment),
              backgroundColor: Colors.orangeAccent,
              onPressed: () {
                Get.to(DocumentPdfViews(
                  no_apar: widget.no_apar,
                  status: "",
                ));
              }),
        ));
  }
}
