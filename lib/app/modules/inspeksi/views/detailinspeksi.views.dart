import 'package:apar/app/modules/inspeksi/controllers/detailinspeksi.controllers.dart';
import 'package:apar/app/modules/inspeksi/views/detailimage.views.dart';
import 'package:apar/app/modules/inspeksi/views/editinspeksi.views.dart';
import 'package:apar/ui/Uicolor.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';

class DetailInspeksiViews extends StatefulWidget {
  String no_apar;
  String status;
  String no_alat;
  String id_alat;
  String lokasi_tujuan;
  DetailInspeksiViews({required this.no_apar, required this.status, required this.no_alat, required this.id_alat, required this.lokasi_tujuan});

  @override
  DetailInspeksiViewsState createState() => DetailInspeksiViewsState();
}

class DetailInspeksiViewsState extends State<DetailInspeksiViews> {
  late DetailInspeksiControllers controllers;
  final DataGridController _controller = DataGridController();

  @override
  void initState() {
    super.initState();
    controllers = new DetailInspeksiControllers();
    controllers.init();
    controllers.getListData(widget.no_apar);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Detail Inspeksi APAR"),
        backgroundColor: Uicolor.hexToColor(Uicolor.greenPln),
        elevation: 0,
      ),
      body: WillPopScope(
        child: Container(
          color: Uicolor.hexToColor(Uicolor.whiteSmooth),
          child: SingleChildScrollView(
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: <
                    Widget>[
              Container(
                color: Uicolor.hexToColor(Uicolor.grey),
                alignment: Alignment.topLeft,
                padding: EdgeInsets.all(16),
                child: Text("Detail Inspeksi : APAR ${widget.no_apar}",
                    style: TextStyle(
                        color: Uicolor.hexToColor(Uicolor.black_young),
                        fontSize: 16,
                        fontWeight: FontWeight.bold)),
              ),
              GetBuilder<DetailInspeksiControllers>(
                init: controllers,
                builder: (params) {
                  if (!params.loadingProses) {
                    return Container(
                        margin: EdgeInsets.only(top: 16),
                        height: 150,
                        child: SfDataGrid(
                          source: params.inspeksiDataSource,
                          columnWidthMode: ColumnWidthMode.fill,
                          columns: <GridColumn>[
                            GridColumn(
                                columnName: 'id',
                                label: Container(
                                    padding: EdgeInsets.all(16.0),
                                    alignment: Alignment.center,
                                    child: Text(
                                      'No',
                                      style: TextStyle(fontSize: 12),
                                    ))),
                            GridColumn(
                                columnName: 'tanggal',
                                label: Container(
                                    padding: EdgeInsets.all(16.0),
                                    alignment: Alignment.center,
                                    child: Text(
                                      'Tanggal',
                                      style: TextStyle(fontSize: 12),
                                    ))),
                            GridColumn(
                                columnName: 'pembersihan',
                                label: Container(
                                    padding: EdgeInsets.all(8.0),
                                    alignment: Alignment.center,
                                    child: Text('Pembersihan', style: TextStyle(fontSize: 12),))),
                            GridColumn(
                                columnName: 'berat',
                                label: Container(
                                    padding: EdgeInsets.all(8.0),
                                    alignment: Alignment.center,
                                    child: Text(
                                      'Berat Apar',
                                      style: TextStyle(fontSize: 12),
                                    ))),
                            GridColumn(
                                columnName: 'tekanan',
                                label: Container(
                                    padding: EdgeInsets.all(8.0),
                                    alignment: Alignment.center,
                                    child: Text('Tekanan Tabung', style: TextStyle(fontSize: 12),))),
                          ],
                          controller: _controller,
                          selectionMode: SelectionMode.single,
                          onCellTap: (params) {
                            if (widget.status != 'APPROVED') {
                              controllers.edited =
                                  controllers.edited ? false : true;
                              controllers.update();
                            }
                            // print("PARAMS ${params.rowColumnIndex.rowIndex}");
                          },
                        ));
                  }

                  return Center(
                    child: CircularProgressIndicator(),
                  );
                },
              ),
               Container(
                    color: Uicolor.hexToColor(Uicolor.grey),
                    alignment: Alignment.topLeft,
                    padding: EdgeInsets.all(16),
                    child: Text("Data Kelengkapan Inspeksi",
                        style: TextStyle(
                            color: Uicolor.hexToColor(Uicolor.black_young),
                            fontSize: 16,
                            fontWeight: FontWeight.bold)),
                  ),
                  GetBuilder<DetailInspeksiControllers>(
                    init: controllers,
                    builder: (params) {
                      return Container(
                          margin: EdgeInsets.only(top: 16),
                          height: 150,
                          child: SfDataGrid(
                            source: params.kelengkapanDataSource,
                            columnWidthMode: ColumnWidthMode.fill,
                            columns: <GridColumn>[
                              GridColumn(
                                  columnName: 'id',
                                  label: Container(
                                      padding: EdgeInsets.all(16.0),
                                      alignment: Alignment.center,
                                      child: Text(
                                        'No',
                                        style: TextStyle(fontSize: 12),
                                        overflow: TextOverflow.fade,
                                      ))),
                              GridColumn(
                                  columnName: 'tanggal',
                                  label: Container(
                                      padding: EdgeInsets.all(16.0),
                                      alignment: Alignment.center,
                                      child: Text(
                                        'Tanggal',
                                        style: TextStyle(fontSize: 12),
                                        overflow: TextOverflow.fade,
                                      ))),
                              GridColumn(
                                  columnName: 'hanger',
                                  label: Container(
                                      padding: EdgeInsets.all(8.0),
                                      alignment: Alignment.center,
                                      child: Text(
                                        'Hanger',
                                        style: TextStyle(fontSize: 12),
                                        overflow: TextOverflow.fade,
                                      ))),
                              GridColumn(
                                  columnName: 'ketinggian apar',
                                  label: Container(
                                      padding: EdgeInsets.all(8.0),
                                      alignment: Alignment.center,
                                      child: Text(
                                        'Ketinggian Apar',
                                        style: TextStyle(fontSize: 12),
                                        overflow: TextOverflow.fade,
                                      ))),
                              GridColumn(
                                  columnName: 'segitiga apar',
                                  label: Container(
                                      padding: EdgeInsets.all(8.0),
                                      alignment: Alignment.center,
                                      child: Text(
                                        'Segitiga Apar',
                                        style: TextStyle(fontSize: 12),
                                        overflow: TextOverflow.fade,
                                      ))),
                              GridColumn(
                                  columnName: 'indikator tekanan',
                                  label: Container(
                                      padding: EdgeInsets.all(8.0),
                                      alignment: Alignment.center,
                                      child: Text(
                                        'Indikator Tekanan',
                                        style: TextStyle(fontSize: 12),
                                        overflow: TextOverflow.fade,
                                      ))),
                              GridColumn(
                                  columnName: 'hose',
                                  label: Container(
                                      padding: EdgeInsets.all(8.0),
                                      alignment: Alignment.center,
                                      child: Text(
                                        'Hose',
                                        style: TextStyle(fontSize: 12),
                                        overflow: TextOverflow.fade,
                                      ))),
                              GridColumn(
                                  columnName: 'tabung',
                                  label: Container(
                                      padding: EdgeInsets.all(8.0),
                                      alignment: Alignment.center,
                                      child: Text(
                                        'Tabung',
                                        style: TextStyle(fontSize: 12),
                                        overflow: TextOverflow.fade,
                                      ))),
                            ],
                          ));
                    },
                  ),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                alignment: Alignment.topLeft,
                child: Text("Keterangan",
                    style: TextStyle(
                        color: Uicolor.hexToColor(Uicolor.black_young),
                        fontSize: 14)),
              ),
              Container(
                  padding: EdgeInsets.only(left: 16, right: 16, top: 8),
                  height: 250,
                  child: Material(
                      borderRadius: BorderRadius.circular(3),
                      shadowColor: Colors.grey,
                      elevation: 0,
                      child: GetBuilder<DetailInspeksiControllers>(
                        init: controllers,
                        builder: (params) {
                          if (!params.loadingProses) {
                            return TextFormField(
                              initialValue: controllers.keterangan,
                              autocorrect: true,
                              keyboardType: TextInputType.multiline,
                              maxLines: null,
                              // controller: _controllerDua,
                              decoration: InputDecoration(
                                // prefixIcon: Icon(Icons.search),
                                hintText: 'Keterangan',
                                prefixIcon: Icon(
                                  Icons.book,
                                  color: Uicolor.hexToColor(Uicolor.greenPln),
                                ),
                                hintStyle: TextStyle(
                                    color: Uicolor.hexToColor(Uicolor.grey)),
                                filled: true,
                                fillColor: Uicolor.hexToColor(Uicolor.white),
                                //
                                enabledBorder: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(8.0)),
                                  borderSide:
                                      BorderSide(color: Colors.white, width: 1),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(8.0)),
                                  borderSide: BorderSide(color: Colors.white),
                                ),
                                contentPadding: new EdgeInsets.symmetric(
                                    vertical: 35.0, horizontal: 10.0),
                              ),
                              onFieldSubmitted: (value) async {},
                              onChanged: (value) {},
                              onTap: () async {},
                            );
                          }

                          return Center(
                            child: CircularProgressIndicator(),
                          );
                        },
                      ))),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                height: MediaQuery.of(context).size.height * 0.07,
                width: double.infinity,
                // height: 50,
                child: RaisedButton(
                  child: Text(
                    "SELANJUTNYA",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.orangeAccent,
                  onPressed: () async {
                    if (widget.status != "APPROVED") {
                      await controllers.ubahKeterangan(widget.no_apar);
                    }
                    Get.to(DetailImageViews(no_apar: widget.no_apar, no_alat: widget.no_alat, id_alat: widget.id_alat, lokasi_tujuan: widget.lokasi_tujuan,));
                  },
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(3.0),
                  ),
                ),
              ),
            ]),
          )),
          onWillPop: () async{
            Get.back(result: 0);
            return false;
          },
      ),
      floatingActionButton: GetBuilder<DetailInspeksiControllers>(
        init: controllers,
        builder: (params) {
          if (params.edited) {
            return FloatingActionButton(
                child: Icon(Icons.edit),
                backgroundColor: Colors.orangeAccent,
                onPressed: () async {
                  int selectedIndex = _controller.selectedIndex;
                  DataGridRow selectedRow = _controller.selectedRow!;
                  List<DataGridRow> selectedRows = _controller.selectedRows;

                  String id =
                      controllers.inspeksiData[selectedIndex].id.toString();
                  int result = await Get.to(
                      EditInspeksiViews(no_apar: widget.no_apar, id: id));

                  if (result == 1) {
                    Get.snackbar("Informasi", "Simpan Data Sukses",
                        backgroundColor: Colors.greenAccent,
                        colorText: Colors.white);
                    controllers.getListData(widget.no_apar);
                  } else {
                    Get.snackbar("Informasi", "Gagal Simpan Data",
                        backgroundColor: Colors.redAccent,
                        colorText: Colors.white);
                  }
                });
          }

          return Container();
        },
      ),
    );
  }
}
