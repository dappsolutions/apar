import 'package:apar/app/modules/inspeksi/controllers/documentpdf.controllers.dart';
import 'package:apar/app/modules/main/views/main.views.dart';
import 'package:apar/component/buttons.dart';
import 'package:apar/config/download.config.dart';
import 'package:apar/config/url.config.dart';
import 'package:apar/ui/Uicolor.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
import 'package:get/get.dart';

class DocumentPdfViews extends StatefulWidget {
  String no_apar;
  String status = "";

  DocumentPdfViews({required this.no_apar, required this.status});

  @override
  _DocumentPdfViewsState createState() => _DocumentPdfViewsState();
}

class _DocumentPdfViewsState extends State<DocumentPdfViews> {
  late DocumentPdfControllers controllers;
  final GlobalKey<SfPdfViewerState> _pdfViewerKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    controllers = new DocumentPdfControllers();
    controllers.getUserIdSession();
    print(
        "URL DOCUMENT ${UrlConfig.BASE_URL_PATH}files/berkas/dokumen_sidak/FORMULIRAPAR-${widget.no_apar}.pdf");
    print("STATUS ${widget.status}");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("DOKUMEN INSPEKSI APAR"),
        backgroundColor: Uicolor.hexToColor(Uicolor.greenPln),
        elevation: 0,
        centerTitle: true,
      ),
      body: Container(
          child: SingleChildScrollView(
              child: Column(
        children: [
          Container(
            height: 500,
            child: SfPdfViewer.network(
              "${UrlConfig.BASE_URL_PATH}files/berkas/dokumen_sidak/FORMULIRAPAR-${widget.no_apar}.pdf",
              key: _pdfViewerKey,
            ),
          ),
          widget.status == 'READY_APPROVED'
              ? Container(
                  child: ButtonComponent.ButtonFlat(context, () async {
                    int status = await controllers.approve(widget.no_apar);
                    if (status == 1) {
                      Get.snackbar("Informasi", "Data Berhasil Diapprove",
                          backgroundColor:
                              Uicolor.hexToColor(Uicolor.greenPln));
                      Future.delayed(Duration(seconds: 2), () {
                        Get.offAll(MainViews());
                      });
                    }
                  }, text: "APPROVE"),
                )
              : Container()
        ],
      ))),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.download),
          backgroundColor: Colors.orangeAccent,
          onPressed: () {
            // Navigator.pop(context);
            DownloadConfig.launchInApp(
                "${UrlConfig.BASE_URL_PATH}files/berkas/dokumen_sidak/FORMULIRAPAR-${widget.no_apar}.pdf");
          }),
    );
  }
}
