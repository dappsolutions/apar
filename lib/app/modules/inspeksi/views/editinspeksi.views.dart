import 'package:apar/app/modules/inspeksi/controllers/addinspeksi.controllers.dart';
import 'package:apar/ui/Uicolor.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

class EditInspeksiViews extends StatefulWidget {
  String no_apar;
  String id;
  EditInspeksiViews({required this.no_apar, required this.id});

  @override
  _EditInspeksiViewsState createState() => _EditInspeksiViewsState();
}

class _EditInspeksiViewsState extends State<EditInspeksiViews> {
  late AddInspeksiControllers controllers;

  @override
  void initState() {
    super.initState();
    controllers = new AddInspeksiControllers();
    controllers.getDetailData(widget.no_apar, widget.id);
    controllers.getDetailDataKelengkapan(widget.no_apar, widget.id);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Ubah Inspeksi APAR"),
        backgroundColor: Uicolor.hexToColor(Uicolor.greenPln),
        elevation: 0,
        // automaticallyImplyLeading: false,
      ),
      body: WillPopScope(
        child: Container(
          color: Uicolor.hexToColor(Uicolor.whiteSmooth),
          child: SingleChildScrollView(
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: <
                    Widget>[
              Container(
                color: Uicolor.hexToColor(Uicolor.grey),
                alignment: Alignment.topLeft,
                padding: EdgeInsets.all(16),
                child: Text("Form Inspeksi : APAR ${widget.no_apar}",
                    style: TextStyle(
                        color: Uicolor.hexToColor(Uicolor.black_young),
                        fontSize: 16,
                        fontWeight: FontWeight.bold)),
              ),
              GetBuilder<AddInspeksiControllers>(
                init: controllers,
                builder: (params) {
                  return Container(
                    child: GetBuilder<AddInspeksiControllers>(
                      init: controllers,
                      builder: (params) {
                        if (!params.loadingProses) {
                          return SfDateRangePicker(
                            onSelectionChanged: params.selectedDate,
                            selectionMode: DateRangePickerSelectionMode.range,
                            initialSelectedRange: PickerDateRange(
                                params.dateChoosed, params.dateChoosed),
                          );
                        }

                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      },
                    ),
                  );
                },
              ),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16),
                alignment: Alignment.topLeft,
                child: Text("Pembersihan",
                    style: TextStyle(
                        color: Uicolor.hexToColor(Uicolor.black_young),
                        fontSize: 14)),
              ),
              GetBuilder<AddInspeksiControllers>(
                    init: controllers,
                    builder: (params) {
                      return Container(
                          height: MediaQuery.of(context).size.height * 0.08,
                          // width: 300,
                          margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                          child: InputDecorator(
                            decoration: const InputDecoration(
                                border: OutlineInputBorder()),
                            child: DropdownButtonHideUnderline(
                                child: DropdownButton(
                              items: params.dataPembersihan.map((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                              value: params.defaultPembersihan,
                              hint: Text("Pilih Kondisi"),
                              onChanged: (val) {
                                // setState(() {
                                controllers.defaultPembersihan = val.toString();
                                controllers.pembersihan = val.toString();
                                controllers.update();
                                // });
                              },
                            )),
                          ));
                    },
                  ),
              // Container(
              //     padding: EdgeInsets.only(left: 16, right: 16, top: 8),
              //     height: MediaQuery.of(context).size.height * 0.08,
              //     child: Material(
              //       borderRadius: BorderRadius.circular(3),
              //       shadowColor: Colors.grey,
              //       elevation: 0,
              //       child: GetBuilder<AddInspeksiControllers>(
              //         init: controllers,
              //         builder: (params) {
              //           if (!params.loadingProses) {
              //             return TextFormField(
              //               initialValue: controllers.pembersihan,
              //               autocorrect: true,
              //               // controller: _controllerDua,
              //               decoration: InputDecoration(
              //                 // prefixIcon: Icon(Icons.search),
              //                 hintText: 'Pembersihan',
              //                 prefixIcon: Icon(
              //                   Icons.book,
              //                   color: Uicolor.hexToColor(Uicolor.greenPln),
              //                 ),
              //                 hintStyle: TextStyle(
              //                     color: Uicolor.hexToColor(Uicolor.grey)),
              //                 filled: true,
              //                 fillColor: Uicolor.hexToColor(Uicolor.white),
              //                 //
              //                 enabledBorder: OutlineInputBorder(
              //                   borderRadius:
              //                       BorderRadius.all(Radius.circular(8.0)),
              //                   borderSide:
              //                       BorderSide(color: Colors.white, width: 1),
              //                 ),
              //                 focusedBorder: OutlineInputBorder(
              //                   borderRadius:
              //                       BorderRadius.all(Radius.circular(8.0)),
              //                   borderSide: BorderSide(color: Colors.white),
              //                 ),
              //               ),
              //               onFieldSubmitted: (value) async {},
              //               onChanged: (value) {
              //                 controllers.pembersihan = value.toString();
              //                 controllers.update();
              //               },
              //               onTap: () async {},
              //             );
              //           }

              //           return Center(
              //             child: CircularProgressIndicator(),
              //           );
              //         },
              //       ),
              //     )),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                alignment: Alignment.topLeft,
                child: Text("Berat Apar",
                    style: TextStyle(
                        color: Uicolor.hexToColor(Uicolor.black_young),
                        fontSize: 14)),
              ),
              Container(
                  padding: EdgeInsets.only(left: 16, right: 16, top: 8),
                  height: MediaQuery.of(context).size.height * 0.08,
                  child: Material(
                    borderRadius: BorderRadius.circular(3),
                    shadowColor: Colors.grey,
                    elevation: 0,
                    child: GetBuilder<AddInspeksiControllers>(
                      init: controllers,
                      builder: (params) {
                        if (!params.loadingProses) {
                          return TextFormField(
                            initialValue: controllers.berat,
                            autocorrect: true,
                            keyboardType: TextInputType.number,
                            // controller: _controllerDua,
                            decoration: InputDecoration(
                              // prefixIcon: Icon(Icons.search),
                              hintText: 'Berat Apar',
                              prefixIcon: Icon(
                                Icons.book,
                                color: Uicolor.hexToColor(Uicolor.greenPln),
                              ),
                              hintStyle: TextStyle(
                                  color: Uicolor.hexToColor(Uicolor.grey)),
                              filled: true,
                              fillColor: Uicolor.hexToColor(Uicolor.white),
                              //
                              enabledBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8.0)),
                                borderSide:
                                    BorderSide(color: Colors.white, width: 1),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8.0)),
                                borderSide: BorderSide(color: Colors.white),
                              ),
                            ),
                            onFieldSubmitted: (value) async {},
                            onChanged: (value) {
                              controllers.berat = value.toString();
                              controllers.update();
                            },
                            onTap: () async {},
                          );
                        }

                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      },
                    ),
                  )),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                alignment: Alignment.topLeft,
                child: Text("Tekanan Tabung",
                    style: TextStyle(
                        color: Uicolor.hexToColor(Uicolor.black_young),
                        fontSize: 14)),
              ),
              Container(
                  padding: EdgeInsets.only(left: 16, right: 16, top: 8),
                  height: MediaQuery.of(context).size.height * 0.08,
                  child: Material(
                      borderRadius: BorderRadius.circular(3),
                      shadowColor: Colors.grey,
                      elevation: 0,
                      child: GetBuilder<AddInspeksiControllers>(
                        init: controllers,
                        builder: (params) {
                          if (!params.loadingProses) {
                            return TextFormField(
                              initialValue: controllers.tekanan,
                              autocorrect: true,
                              // controller: _controllerDua,
                              decoration: InputDecoration(
                                // prefixIcon: Icon(Icons.search),
                                hintText: 'Tekanan Tabung',
                                prefixIcon: Icon(
                                  Icons.book,
                                  color: Uicolor.hexToColor(Uicolor.greenPln),
                                ),
                                hintStyle: TextStyle(
                                    color: Uicolor.hexToColor(Uicolor.grey)),
                                filled: true,
                                fillColor: Uicolor.hexToColor(Uicolor.white),
                                //
                                enabledBorder: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(8.0)),
                                  borderSide:
                                      BorderSide(color: Colors.white, width: 1),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(8.0)),
                                  borderSide: BorderSide(color: Colors.white),
                                ),
                              ),
                              onFieldSubmitted: (value) async {},
                              onChanged: (value) {
                                controllers.tekanan = value.toString();
                                controllers.update();
                              },
                              onTap: () async {},
                            );
                          }

                          return Center(child: CircularProgressIndicator());
                        },
                      ))),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                alignment: Alignment.topLeft,
                child: Text("Hanger",
                    style: TextStyle(
                        color: Uicolor.hexToColor(Uicolor.black_young),
                        fontSize: 14)),
              ),
              GetBuilder<AddInspeksiControllers>(
                init: controllers,
                builder: (params) {
                  return Container(
                      height: MediaQuery.of(context).size.height * 0.08,
                      // width: 300,
                      margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                      child: InputDecorator(
                        decoration:
                            const InputDecoration(border: OutlineInputBorder()),
                        child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                          items: params.dataHanger.map((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                          value: params.defaultHanger,
                          hint: Text("Pilih Kondisi"),
                          onChanged: (val) {
                            // setState(() {
                            controllers.defaultHanger = val.toString();
                            controllers.hanger = val.toString();
                            controllers.update();
                            // });
                          },
                        )),
                      ));
                },
              ),

              // Container(
              //     padding: EdgeInsets.only(left: 16, right: 16, top: 8),
              //     height: MediaQuery.of(context).size.height * 0.08,
              //     child: Material(
              //         borderRadius: BorderRadius.circular(3),
              //         shadowColor: Colors.grey,
              //         elevation: 0,
              //         child: TextFormField(
              //           initialValue: "",
              //           autocorrect: true,
              //           // controller: _controllerDua,
              //           decoration: InputDecoration(
              //             // prefixIcon: Icon(Icons.search),
              //             hintText: 'Hanger',
              //             prefixIcon: Icon(
              //               Icons.book,
              //               color: Uicolor.hexToColor(Uicolor.greenPln),
              //             ),
              //             hintStyle: TextStyle(
              //                 color: Uicolor.hexToColor(Uicolor.grey)),
              //             filled: true,
              //             fillColor: Uicolor.hexToColor(Uicolor.white),
              //             //
              //             enabledBorder: OutlineInputBorder(
              //               borderRadius:
              //                   BorderRadius.all(Radius.circular(8.0)),
              //               borderSide:
              //                   BorderSide(color: Colors.white, width: 1),
              //             ),
              //             focusedBorder: OutlineInputBorder(
              //               borderRadius:
              //                   BorderRadius.all(Radius.circular(8.0)),
              //               borderSide: BorderSide(color: Colors.white),
              //             ),
              //           ),
              //           onFieldSubmitted: (value) async {},
              //           onChanged: (value) {
              //             controllers.hanger = value.toString();
              //             controllers.update();
              //           },
              //           onTap: () async {},
              //         ))),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                alignment: Alignment.topLeft,
                child: Text("Ketinggian Apar",
                    style: TextStyle(
                        color: Uicolor.hexToColor(Uicolor.black_young),
                        fontSize: 14)),
              ),
              GetBuilder<AddInspeksiControllers>(
                init: controllers,
                builder: (params) {
                  return Container(
                      height: MediaQuery.of(context).size.height * 0.08,
                      // width: 300,
                      margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                      child: InputDecorator(
                        decoration:
                            const InputDecoration(border: OutlineInputBorder()),
                        child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                          items: params.dataKetinggianApar.map((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                          value: params.defaultKetinggianApar,
                          hint: Text("Pilih Kondisi"),
                          onChanged: (val) {
                            // setState(() {
                            controllers.defaultKetinggianApar = val.toString();
                            controllers.ketinggian_apar = val.toString();
                            controllers.update();
                            // });
                          },
                        )),
                      ));
                },
              ),
              // Container(
              //     padding: EdgeInsets.only(left: 16, right: 16, top: 8),
              //     height: MediaQuery.of(context).size.height * 0.08,
              //     child: Material(
              //         borderRadius: BorderRadius.circular(3),
              //         shadowColor: Colors.grey,
              //         elevation: 0,
              //         child: TextFormField(
              //           initialValue: "",
              //           autocorrect: true,
              //           // controller: _controllerDua,
              //           decoration: InputDecoration(
              //             // prefixIcon: Icon(Icons.search),
              //             hintText: 'Ketinggian Apar',
              //             prefixIcon: Icon(
              //               Icons.book,
              //               color: Uicolor.hexToColor(Uicolor.greenPln),
              //             ),
              //             hintStyle: TextStyle(
              //                 color: Uicolor.hexToColor(Uicolor.grey)),
              //             filled: true,
              //             fillColor: Uicolor.hexToColor(Uicolor.white),
              //             //
              //             enabledBorder: OutlineInputBorder(
              //               borderRadius:
              //                   BorderRadius.all(Radius.circular(8.0)),
              //               borderSide:
              //                   BorderSide(color: Colors.white, width: 1),
              //             ),
              //             focusedBorder: OutlineInputBorder(
              //               borderRadius:
              //                   BorderRadius.all(Radius.circular(8.0)),
              //               borderSide: BorderSide(color: Colors.white),
              //             ),
              //           ),
              //           onFieldSubmitted: (value) async {},
              //           onChanged: (value) {
              //             controllers.ketinggian_apar = value.toString();
              //             controllers.update();
              //           },
              //           onTap: () async {},
              //         ))),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                alignment: Alignment.topLeft,
                child: Text("Segitiga Apar",
                    style: TextStyle(
                        color: Uicolor.hexToColor(Uicolor.black_young),
                        fontSize: 14)),
              ),
              GetBuilder<AddInspeksiControllers>(
                init: controllers,
                builder: (params) {
                  return Container(
                      height: MediaQuery.of(context).size.height * 0.08,
                      // width: 300,
                      margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                      child: InputDecorator(
                        decoration:
                            const InputDecoration(border: OutlineInputBorder()),
                        child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                          items: params.dataSegitigaApar.map((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                          value: params.defaultSegitigaApar,
                          hint: Text("Pilih Kondisi"),
                          onChanged: (val) {
                            // setState(() {
                            controllers.defaultSegitigaApar = val.toString();
                            controllers.segitiga_apar = val.toString();
                            controllers.update();
                            // });
                          },
                        )),
                      ));
                },
              ),
              // Container(
              //     padding: EdgeInsets.only(left: 16, right: 16, top: 8),
              //     height: MediaQuery.of(context).size.height * 0.08,
              //     child: Material(
              //         borderRadius: BorderRadius.circular(3),
              //         shadowColor: Colors.grey,
              //         elevation: 0,
              //         child: TextFormField(
              //           initialValue: "",
              //           autocorrect: true,
              //           // controller: _controllerDua,
              //           decoration: InputDecoration(
              //             // prefixIcon: Icon(Icons.search),
              //             hintText: 'Segitiga Apar',
              //             prefixIcon: Icon(
              //               Icons.book,
              //               color: Uicolor.hexToColor(Uicolor.greenPln),
              //             ),
              //             hintStyle: TextStyle(
              //                 color: Uicolor.hexToColor(Uicolor.grey)),
              //             filled: true,
              //             fillColor: Uicolor.hexToColor(Uicolor.white),
              //             //
              //             enabledBorder: OutlineInputBorder(
              //               borderRadius:
              //                   BorderRadius.all(Radius.circular(8.0)),
              //               borderSide:
              //                   BorderSide(color: Colors.white, width: 1),
              //             ),
              //             focusedBorder: OutlineInputBorder(
              //               borderRadius:
              //                   BorderRadius.all(Radius.circular(8.0)),
              //               borderSide: BorderSide(color: Colors.white),
              //             ),
              //           ),
              //           onFieldSubmitted: (value) async {},
              //           onChanged: (value) {
              //             controllers.segitiga_apar = value.toString();
              //             controllers.update();
              //           },
              //           onTap: () async {},
              //         ))),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                alignment: Alignment.topLeft,
                child: Text("Indikator Tekanan",
                    style: TextStyle(
                        color: Uicolor.hexToColor(Uicolor.black_young),
                        fontSize: 14)),
              ),
              GetBuilder<AddInspeksiControllers>(
                init: controllers,
                builder: (params) {
                  return Container(
                      height: MediaQuery.of(context).size.height * 0.08,
                      // width: 300,
                      margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                      child: InputDecorator(
                        decoration:
                            const InputDecoration(border: OutlineInputBorder()),
                        child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                          items:
                              params.dataIndikatorTekanan.map((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                          value: params.defaultIndikatorTekanan,
                          hint: Text("Pilih Kondisi"),
                          onChanged: (val) {
                            // setState(() {
                            controllers.defaultIndikatorTekanan =
                                val.toString();
                            controllers.indikator_tekanan = val.toString();
                            controllers.update();
                            // });
                          },
                        )),
                      ));
                },
              ),
              // Container(
              //     padding: EdgeInsets.only(left: 16, right: 16, top: 8),
              //     height: MediaQuery.of(context).size.height * 0.08,
              //     child: Material(
              //         borderRadius: BorderRadius.circular(3),
              //         shadowColor: Colors.grey,
              //         elevation: 0,
              //         child: TextFormField(
              //           initialValue: "",
              //           autocorrect: true,
              //           // controller: _controllerDua,
              //           decoration: InputDecoration(
              //             // prefixIcon: Icon(Icons.search),
              //             hintText: 'Indikator Tekanan',
              //             prefixIcon: Icon(
              //               Icons.book,
              //               color: Uicolor.hexToColor(Uicolor.greenPln),
              //             ),
              //             hintStyle: TextStyle(
              //                 color: Uicolor.hexToColor(Uicolor.grey)),
              //             filled: true,
              //             fillColor: Uicolor.hexToColor(Uicolor.white),
              //             //
              //             enabledBorder: OutlineInputBorder(
              //               borderRadius:
              //                   BorderRadius.all(Radius.circular(8.0)),
              //               borderSide:
              //                   BorderSide(color: Colors.white, width: 1),
              //             ),
              //             focusedBorder: OutlineInputBorder(
              //               borderRadius:
              //                   BorderRadius.all(Radius.circular(8.0)),
              //               borderSide: BorderSide(color: Colors.white),
              //             ),
              //           ),
              //           onFieldSubmitted: (value) async {},
              //           onChanged: (value) {
              //             controllers.indikator_tekanan = value.toString();
              //             controllers.update();
              //           },
              //           onTap: () async {},
              //         ))),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                alignment: Alignment.topLeft,
                child: Text("Hose",
                    style: TextStyle(
                        color: Uicolor.hexToColor(Uicolor.black_young),
                        fontSize: 14)),
              ),
              GetBuilder<AddInspeksiControllers>(
                init: controllers,
                builder: (params) {
                  return Container(
                      height: MediaQuery.of(context).size.height * 0.08,
                      // width: 300,
                      margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                      child: InputDecorator(
                        decoration:
                            const InputDecoration(border: OutlineInputBorder()),
                        child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                          items: params.dataHose.map((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                          value: params.defaultHose,
                          hint: Text("Pilih Kondisi"),
                          onChanged: (val) {
                            // setState(() {
                            controllers.defaultHose = val.toString();
                            controllers.hose = val.toString();
                            controllers.update();
                            // });
                          },
                        )),
                      ));
                },
              ),
              // Container(
              //     padding: EdgeInsets.only(left: 16, right: 16, top: 8),
              //     height: MediaQuery.of(context).size.height * 0.08,
              //     child: Material(
              //         borderRadius: BorderRadius.circular(3),
              //         shadowColor: Colors.grey,
              //         elevation: 0,
              //         child: TextFormField(
              //           initialValue: "",
              //           autocorrect: true,
              //           // controller: _controllerDua,
              //           decoration: InputDecoration(
              //             // prefixIcon: Icon(Icons.search),
              //             hintText: 'Hose',
              //             prefixIcon: Icon(
              //               Icons.book,
              //               color: Uicolor.hexToColor(Uicolor.greenPln),
              //             ),
              //             hintStyle: TextStyle(
              //                 color: Uicolor.hexToColor(Uicolor.grey)),
              //             filled: true,
              //             fillColor: Uicolor.hexToColor(Uicolor.white),
              //             //
              //             enabledBorder: OutlineInputBorder(
              //               borderRadius:
              //                   BorderRadius.all(Radius.circular(8.0)),
              //               borderSide:
              //                   BorderSide(color: Colors.white, width: 1),
              //             ),
              //             focusedBorder: OutlineInputBorder(
              //               borderRadius:
              //                   BorderRadius.all(Radius.circular(8.0)),
              //               borderSide: BorderSide(color: Colors.white),
              //             ),
              //           ),
              //           onFieldSubmitted: (value) async {},
              //           onChanged: (value) {
              //             controllers.hose = value.toString();
              //             controllers.update();
              //           },
              //           onTap: () async {},
              //         ))),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                alignment: Alignment.topLeft,
                child: Text("Tabung",
                    style: TextStyle(
                        color: Uicolor.hexToColor(Uicolor.black_young),
                        fontSize: 14)),
              ),
              GetBuilder<AddInspeksiControllers>(
                init: controllers,
                builder: (params) {
                  return Container(
                      height: MediaQuery.of(context).size.height * 0.08,
                      // width: 300,
                      margin: EdgeInsets.only(right: 16, left: 16, top: 8),
                      child: InputDecorator(
                        decoration:
                            const InputDecoration(border: OutlineInputBorder()),
                        child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                          items: params.dataTabung.map((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                          value: params.defaultTabung,
                          hint: Text("Pilih Kondisi"),
                          onChanged: (val) {
                            // setState(() {
                            controllers.defaultTabung = val.toString();
                            controllers.tabung = val.toString();
                            controllers.update();
                            // });
                          },
                        )),
                      ));
                },
              ),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                alignment: Alignment.topLeft,
                child: Text("Petugas",
                    style: TextStyle(
                        color: Uicolor.hexToColor(Uicolor.black_young),
                        fontSize: 14)),
              ),
              Container(
                  padding: EdgeInsets.only(left: 16, right: 16, top: 8),
                  height: MediaQuery.of(context).size.height * 0.08,
                  child: Material(
                      borderRadius: BorderRadius.circular(3),
                      shadowColor: Colors.grey,
                      elevation: 0,
                      child: GetBuilder<AddInspeksiControllers>(
                        init: controllers,
                        builder: (params) {
                          if (!params.loadingProses) {
                            return TextFormField(
                              initialValue: controllers.petugas,
                              autocorrect: true,
                              // controller: _controllerDua,
                              decoration: InputDecoration(
                                // prefixIcon: Icon(Icons.search),
                                hintText: 'Petugas',
                                prefixIcon: Icon(
                                  Icons.person,
                                  color: Uicolor.hexToColor(Uicolor.greenPln),
                                ),
                                hintStyle: TextStyle(
                                    color: Uicolor.hexToColor(Uicolor.grey)),
                                filled: true,
                                fillColor: Uicolor.hexToColor(Uicolor.white),
                                //
                                enabledBorder: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(8.0)),
                                  borderSide:
                                      BorderSide(color: Colors.white, width: 1),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(8.0)),
                                  borderSide: BorderSide(color: Colors.white),
                                ),
                              ),
                              onFieldSubmitted: (value) async {},
                              onChanged: (value) {
                                controllers.petugas = value.toString();
                                controllers.update();
                              },
                              onTap: () async {},
                            );
                          }

                          return Center(
                            child: CircularProgressIndicator(),
                          );
                        },
                      ))),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                height: MediaQuery.of(context).size.height * 0.07,
                width: double.infinity,
                // height: 50,
                child: RaisedButton(
                  child: Text(
                    "SIMPAN",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.orangeAccent,
                  onPressed: () async {
                    int status = await controllers.simpanPerubahan(widget.id);
                    Get.back(result: status);
                  },
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(3.0),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                height: MediaQuery.of(context).size.height * 0.07,
                width: double.infinity,
                // height: 50,
                child: RaisedButton(
                  child: Text(
                    "BATAL",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.grey[350],
                  onPressed: () async {
                    Get.back(result: 0);
                  },
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(3.0),
                  ),
                ),
              ),
            ]),
          )),
          onWillPop: () async{
            Get.back(result: 0);
            return false;
          },
      ),
    );
  }
}
