import 'dart:async';

import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class LokasiControllers extends GetxController {
  double currentLatitudePos = -3.105379;
  double currentLngitudePos = 119.851611;
  Completer<GoogleMapController> mapControl = Completer();
  Set<Marker> markersData = {};
  bool isLoadingProses = false;

  Future<Position> determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.
    // return await Geolocator.getCurrentPosition();
    Position posArea = await Geolocator.getCurrentPosition();
    return posArea;
  }

  Future<void> setInitialLocationWithMarker(context) async {
    this.isLoadingProses = true;
    this.update();
    // set the initial location by pulling the user's
    Position posArea = await this.determinePosition();
    this.currentLatitudePos = posArea.latitude;
    this.currentLngitudePos = posArea.longitude;

    this.markersData.clear();
    this.markersData.add(
          Marker(
            markerId: MarkerId("user_positions"),
            position: LatLng(this.currentLatitudePos, this.currentLngitudePos),
            icon: BitmapDescriptor.defaultMarker,
            onTap: () async {
              // showInfoWindowMapMarker(context, locations: "LOKASI USER SAAT INI");
            },
          ),
        );
    this.isLoadingProses = false;
    this.update();
  }
}
