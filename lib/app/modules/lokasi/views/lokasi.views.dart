import 'package:apar/app/modules/lokasi/controllers/lokasi.controllers.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class LokasiViews extends StatefulWidget {
  const LokasiViews({Key? key}) : super(key: key);

  @override
  _LokasiViewsState createState() => _LokasiViewsState();
}

class _LokasiViewsState extends State<LokasiViews> {
  late LokasiControllers controller;

  @override
  void initState() {
    super.initState();
    controller = new LokasiControllers();
    controller.setInitialLocationWithMarker(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Container(
            child: GetBuilder<LokasiControllers>(
          init: controller,
          builder: (params) {
            if (!params.isLoadingProses) {
              return GoogleMap(
                mapType: MapType.normal,
                markers: params.markersData,
                initialCameraPosition: CameraPosition(
                  target: LatLng(
                      params.currentLatitudePos, params.currentLngitudePos),
                  zoom: 14,
                ),
                // polylines: params.polylines,
                onMapCreated: (GoogleMapController googleMapController) {
                  params.mapControl.complete(googleMapController);
                },
              );
            }

            return Center(
              child: CircularProgressIndicator(),
            );
          },
        )),
      ),
    );
  }
}
