import 'dart:convert';

import 'package:apar/app/modules/main/models/document.models.dart';
import 'package:apar/app/modules/main/services/dashboard.services.dart';
import 'package:apar/app/modules/version/models/version.model.dart';
import 'package:apar/app/modules/version/services/version.services.dart';
import 'package:apar/config/api.config.dart';
import 'package:apar/config/modules.config.dart';
import 'package:apar/config/routes.config.dart';
import 'package:apar/config/session.config.dart';
import 'package:apar/config/url.config.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';

class DashboardControlers extends GetxController {
  List<DocumentModels> dataDocument = [];
  List<VersionModels> dataVersion = [];
  bool loadingProses = false;
  String dateNowApar =
      DateFormat("yyyy-MM-dd").format(DateTime.now()).toString();
  List<String> dataWilayahApar = [];

  String appVersion = '-';

  Future<void> getAppVersion() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String appVersion = packageInfo.version;
    String appName = packageInfo.appName;
    String buildNumber = packageInfo.buildNumber;
    String packageName = packageInfo.packageName;
    String buildSignature = packageInfo.buildSignature;
    this.appVersion = appVersion;
    this.update();

    this.getAppVersionApi(appVersion);
  }

  Future<void> getUserIdSession() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    SessionConfig.user_id = pref.getString("user_id")!;
    SessionConfig.upt_id = pref.getString("upt_id")!;
    SessionConfig.wilayah = pref.getString("wilayah")!;
    SessionConfig.id_tujuan = pref.getString("id_tujuan")!;
    SessionConfig.approval_id = pref.getString("approval_id")!;
    this.update();
  }

  Future<void> getData() async {
    this.loadingProses = true;
    update();

    await getUserIdSession();

    Map<String, dynamic> params = {};
    params["last_id"] = "";
    params["user_id"] = SessionConfig.user_id;
    params["upt_id"] = SessionConfig.upt_id;
    params["wilayah"] = SessionConfig.wilayah;
    List<DocumentModels> data =
        await DashboardServices.getData(params, this.dataDocument);
    this.dataDocument = data;
    this.loadingProses = false;
    this.update();
  }

  Future<void> getDataLoadMore() async {
    this.loadingProses = true;
    update();

    await getUserIdSession();

    Map<String, dynamic> params = {};
    params["last_id"] = this.dataDocument.length > 0
        ? this.dataDocument[this.dataDocument.length - 1].id
        : "";
    params["user_id"] = SessionConfig.user_id;
    params["upt_id"] = SessionConfig.upt_id;
    params["wilayah"] = SessionConfig.wilayah;

    List<DocumentModels> data =
        await DashboardServices.getDataLoadMore(params, this.dataDocument);
    this.dataDocument = data;
    this.loadingProses = false;
    this.update();
  }

  Future<void> getDataWilayahApar() async {
    await getUserIdSession();
    this.dataWilayahApar.clear();
    switch (SessionConfig.wilayah) {
      case 'UPT':
        this.dataWilayahApar.add('GARDU INDUK');
        this.dataWilayahApar.add('ULTG');
        this.dataWilayahApar.add('UPT/KANTOR');
        this.dataWilayahApar.add('GEDUNG');
        break;
      case 'GI':
        this.dataWilayahApar.add('GARDU INDUK');
        break;
      case 'ULTG':
        this.dataWilayahApar.add('GARDU INDUK');
        this.dataWilayahApar.add('ULTG');
        break;
      default:
        this.dataWilayahApar.add('GARDU INDUK');
        this.dataWilayahApar.add('ULTG');
        this.dataWilayahApar.add('UPT/KANTOR');
        this.dataWilayahApar.add('GEDUNG');
        break;
    }
    update();
  }

  Future<void> getAppVersionApi(String appVersion) async {
    Map<String, dynamic> params = {};

    List<VersionModels> data =
        await VersionServices.getAppVersion(params, this.dataVersion);
    this.dataVersion = data;
    if (data.isNotEmpty) {
      if (data[0].version != appVersion) {
        Get.dialog(
          AlertDialog(
            title: Text('Update Aplikasi'),
            content: Text(
                'Aplikasi Apar Versi ${appVersion} Sudah Usang, Silakan Update Aplikasi Sekarang'),
            actions: <Widget>[
              FlatButton(
                child: Text('Update Versi : ${data[0].version}'),
                onPressed: () {
                  this._launchInBrowser(UrlConfig.UPDATE_APP_URL);
                },
              ),
            ],
          ),
        );
      }
    }
    this.update();
  }

  Future<void> _launchInBrowser(String url) async {
    if (await canLaunch(url)) {
      await launch(
        url,
        forceSafariVC: false,
        forceWebView: false,
        headers: <String, String>{'my_header_key': 'my_header_value'},
      );
    } else {
      throw 'Could not launch $url';
    }
  }
}
