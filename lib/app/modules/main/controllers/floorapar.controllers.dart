import 'package:apar/app/modules/main/models/tempat.models.dart';
import 'package:apar/app/modules/main/services/floorapar.services.dart';
import 'package:apar/config/session.config.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FloorAparControllers extends GetxController {
  List<TempatModels> data = [];
  String dateNowApar =
      DateFormat("yyyy-MM").format(DateTime.now()).toString();

  bool loadingProses = false;
  Future<void> getUserIdSession() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    SessionConfig.user_id = pref.getString("user_id")!;
    SessionConfig.upt_id = pref.getString("upt_id")!;
    SessionConfig.wilayah = pref.getString("wilayah")!;
    this.update();
  }

  Future<void> getDataTempat(String wilayah, String id_lokasi) async {
    this.loadingProses = true;
    update();

    await getUserIdSession();

    Map<String, dynamic> params = {};
    params["user_id"] = SessionConfig.user_id;
    params["upt_id"] = SessionConfig.upt_id;
    params["wilayah"] = wilayah;
    params["id_lokasi"] = id_lokasi;
    print("PARAMS : ${params.toString()}");

    List<TempatModels> data =
        await FloorAparServices.getDataTempat(params, this.data);
    this.data = data;
    this.loadingProses = false;
    this.update();
  }
}
