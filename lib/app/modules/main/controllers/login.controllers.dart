import 'dart:convert';

import 'package:apar/app/modules/main/models/login.models.dart';
import 'package:apar/app/modules/main/services/login.services.dart';
import 'package:apar/app/modules/main/views/dashboard.views.dart';
import 'package:apar/app/modules/response/models/message.models.dart';
import 'package:apar/config/api.config.dart';
import 'package:apar/config/modules.config.dart';
import 'package:apar/config/routes.config.dart';
import 'package:apar/config/session.config.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class LoginControllers extends GetxController {
  bool loadingLogin = false;

  String appVersion = '-';

  Future<void> setAppVersion() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String appVersion = packageInfo.version;
    this.appVersion = appVersion;
    this.update();
  }

  Future<void> signIn(username, pass, context) async {
    this.loadingLogin = true;
    update();
    Map<String, dynamic> params = {};
    params["username"] = username;
    params["password"] = pass;
    var request = await LoginServices.signIn(params);

    if (request.statusCode == 200) {
      this.loadingLogin = false;
      update();

      var data = json.decode(request.body);
      MessageModel respMessage = MessageModel.fromJson(data);
      if (respMessage.is_valid == "1") {
        LoginModel loginModel = LoginModel.fromJson(data["data"]);
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString("user_id", loginModel.user);
        prefs.setString("email", loginModel.email);
        prefs.setString("username", loginModel.name);
        prefs.setString("upt_id", loginModel.upt_id);
        prefs.setString("wilayah", loginModel.nama_wilayah);
        prefs.setString("lokasi_tujuan", loginModel.lokasi_tujuan);
        prefs.setString("id_tujuan", loginModel.id_tujuan);
        prefs.setString("approval_id", loginModel.approval_id);

        SessionConfig.sessionSet(
            loginModel.user,
            loginModel.upt_id,
            loginModel.nama_wilayah,
            loginModel.lokasi_tujuan,
            loginModel.id_tujuan,
            loginModel.approval_id);
        Get.snackbar("Informasi", respMessage.message,
            backgroundColor: Colors.green, colorText: Colors.white);
        Navigator.of(context).pop();
        Get.to(DashboardViews());
      } else {
        Get.snackbar("Informasi", respMessage.message,
            backgroundColor: Colors.redAccent, colorText: Colors.white);
      }
    } else {
      this.loadingLogin = false;
      update();
      Get.snackbar("Informasi", "Gagal Memuat Data",
          backgroundColor: Colors.redAccent, colorText: Colors.white);
    }
  }

  Future<void> loginAsGuest(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("user_id", '');
    prefs.setString("email", '');
    prefs.setString("username", 'guest');
    prefs.setString("upt_id", '');
    prefs.setString("wilayah", '');
    prefs.setString("lokasi_tujuan", '');
    prefs.setString("id_tujuan", '');
    SessionConfig.sessionSet('', '', '', '', '', '');
    Navigator.of(context).pop();
    Get.to(DashboardViews());
  }
}
