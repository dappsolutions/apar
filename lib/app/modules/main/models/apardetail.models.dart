class AparDetailModels {
  late String id_alat;
  late String no_alat;
  late String nama_lokasi;
  late String tempat;
  late String nama_wilayah;
  late String id_lokasi;
  late String has_pemeliharaan;
  late String no_document;
  late String status;
  late String lokasi_tujuan;

  AparDetailModels(
      {required this.id_alat,
      required this.no_alat,
      required this.nama_lokasi,
      required this.tempat,
      required this.nama_wilayah,
      required this.id_lokasi,
      required this.has_pemeliharaan,
      required this.no_document,
      required this.status,
      required this.lokasi_tujuan});

  AparDetailModels.fromJson(Map<String, dynamic> json) {
    this.id_alat = json['id_alat'];
    this.no_alat = json['no_alat'];
    this.nama_lokasi = json['nama_lokasi'];
    this.tempat = json['tempat'];
    this.nama_wilayah = json['nama_wilayah'];
    this.id_lokasi = json['id_lokasi'];
    this.has_pemeliharaan = json['has_pemeliharaan'].toString();
    this.no_document = json['no_document'];
    this.status = json['status'];
    this.lokasi_tujuan = json['lokasi_tujuan'].toString();
  }
}
