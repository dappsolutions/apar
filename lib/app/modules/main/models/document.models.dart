


class DocumentModels{
  late String id;
  late String no_alat;
  late String nama_wilayah;
  late String nama_lokasi;
  late String tempat;
  late String period_start;
  late String nama_pegawai;
  late String createddate;
  late String no_document;
  late String status;

  DocumentModels({required this.id, required this.no_alat, required this.nama_wilayah, required this.nama_lokasi, required this.tempat, required this.period_start, required this.nama_pegawai, required this.createddate, required this.no_document, required this.status});

  DocumentModels.fromJson(Map<String, dynamic> json){
    this.id = json['id'].toString();
    this.no_alat = json['no_alat'];
    this.nama_wilayah = json['nama_wilayah'];
    this.nama_lokasi = json['nama_lokasi'];
    this.tempat = json['tempat'];
    this.period_start = json['period_start'];
    this.nama_pegawai = json['nama_pegawai'];
    this.createddate = json['createddate'];
    this.no_document = json['no_document'];
    this.status = json['status'];
  }
}