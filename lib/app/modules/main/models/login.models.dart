
class LoginModel{
  late String id;
  late String user;
  late String apps;
  late String createddate;
  late String name;
  late String email;
  late String upt_id;
  late String nama_wilayah;
  late String lokasi_tujuan;
  late String id_tujuan;
  late String approval_id;

  LoginModel({required this.id, required this.user, required this.apps, required this.createddate, required this.name, required this.email, required this.upt_id, required this.nama_wilayah, required this.lokasi_tujuan, required this.id_tujuan, required this.approval_id});

  LoginModel.fromJson(Map<String, dynamic> json){
    this.id = json['id'];
    this.user = json['user'];
    this.apps = json['apps'];
    this.createddate = json['createddate'];
    this.name = json['name'];
    this.email = json['email'];
    this.upt_id = json['upt_id'];
    this.nama_wilayah = json['nama_wilayah'];
    this.lokasi_tujuan = json['lokasi_tujuan'].toString();
    this.id_tujuan = json['id_tujuan'].toString();
    this.approval_id = json['approval_id'].toString();
  }
}