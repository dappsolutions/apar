import 'package:apar/app/modules/main/models/apardetail.models.dart';

class TempatModels {
  late String no_alat;
  late String nama_lokasi;
  late String tempat;
  late String nama_wilayah;
  late String id_lokasi;
  late String jumlah_item;
  List<AparDetailModels> item = [];
  late String lokasi_tujuan;
  late String jumlah_item_sudah_inspeksi;

  TempatModels(
      {required this.no_alat,
      required this.nama_lokasi,
      required this.tempat,
      required this.nama_wilayah,
      required this.id_lokasi,
      required this.jumlah_item,
      required this.item, required this.lokasi_tujuan, 
      required this.jumlah_item_sudah_inspeksi});

  TempatModels.fromJson(Map<String, dynamic> json) {
    this.no_alat = json['no_alat'];
    this.nama_lokasi = json['nama_lokasi'];
    this.tempat = json['tempat'];
    this.nama_wilayah = json['nama_wilayah'];
    this.id_lokasi = json['id_lokasi'];
    this.jumlah_item = json['jumlah_item'].toString();
    this.lokasi_tujuan = json['lokasi_tujuan'].toString();
    this.jumlah_item_sudah_inspeksi = json['jumlah_item_sudah_inspeksi'].toString();
    for (var val in json["item"]) {
      this.item.add(AparDetailModels.fromJson(val));
    }
  }
}
