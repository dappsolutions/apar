import 'dart:convert';

import 'package:apar/app/modules/main/models/document.models.dart';
import 'package:apar/config/api.config.dart';
import 'package:apar/config/modules.config.dart';
import 'package:apar/config/routes.config.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class DashboardServices {
  static Future<List<DocumentModels>> getData(
      Map<String, dynamic> params, List<DocumentModels> dataDocument) async {
    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_DOCUMENT][Routes.GETDATA]),
        body: params);

    if (request.statusCode == 200) {
      dataDocument.clear();
      var data = json.decode(request.body);
      for (var val in data["data"]) {
        dataDocument.add(DocumentModels.fromJson(val));
      }

      return dataDocument;
    } else {
      Get.snackbar("Informasi", "Gagal Memuat Data",
          backgroundColor: Colors.redAccent, colorText: Colors.white);
      return [];
    }
    return [];
  }

  static Future<List<DocumentModels>> getDataLoadMore(
      Map<String, dynamic> params, List<DocumentModels> dataDocument) async {
    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_DOCUMENT][Routes.GETDATA]),
        body: params);

        // print("DAT LOAD MORE ${request.body}");

    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      for (var val in data["data"]) {
        dataDocument.add(DocumentModels.fromJson(val));
      }

      return dataDocument;
    } else {
      Get.snackbar("Informasi", "Gagal Memuat Data",
          backgroundColor: Colors.redAccent, colorText: Colors.white);
    }

    return [];
  }
}
