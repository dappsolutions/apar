

import 'dart:convert';

import 'package:apar/app/modules/main/models/tempat.models.dart';
import 'package:apar/config/api.config.dart';
import 'package:apar/config/modules.config.dart';
import 'package:apar/config/routes.config.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class FloorAparServices {
  static Future<List<TempatModels>> getDataTempat(
      Map<String, dynamic> params, List<TempatModels> dataDocument) async {
    var request = await http.post(
        Uri.parse(Api.route[ModulesConfig.MODULE_ALAT][Routes.GETDATATEMPATLOKASI]),
        body: params);
    print(Api.route[ModulesConfig.MODULE_ALAT][Routes.GETDATATEMPATLOKASI]);
    print("DATa TEMPAT LOKASI : ${request.body}");
    if (request.statusCode == 200) {
      var data = json.decode(request.body);
      for (var val in data["data"]) {
        dataDocument.add(TempatModels.fromJson(val));
      }

      return dataDocument;
    } else {
      Get.snackbar("Informasi", "Gagal Memuat Data",
          backgroundColor: Colors.redAccent, colorText: Colors.white);
    }

    return [];
  }
}