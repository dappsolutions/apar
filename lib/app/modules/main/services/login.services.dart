
import 'package:apar/app/modules/response/models/message.models.dart';
import 'package:apar/config/api.config.dart';
import 'package:apar/config/modules.config.dart';
import 'package:apar/config/routes.config.dart';
import 'package:http/http.dart' as http;

class LoginServices{
  static Future<http.Response> signIn(Map<String, dynamic> params) async{
    var request = await http
        .post(Uri.parse(Api.route[ModulesConfig.MODULE_LOGIN][Routes.SIGN_IN]), body: params);
        print("REQUEST ${request.body}");
        return request;
  }
}