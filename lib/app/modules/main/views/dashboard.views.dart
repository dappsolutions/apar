import 'package:apar/app/modules/daftarapar/views/daftarapar.views.dart';
import 'package:apar/app/modules/daftarapar/views/daftaroutstandingapar.views.dart';
import 'package:apar/app/modules/daftarapar/views/daftarwilayahapar.views.dart';
import 'package:apar/app/modules/daftarapar/views/exportrabapar.views.dart';
import 'package:apar/app/modules/daftarapar/views/exportxlsapar.views.dart';
import 'package:apar/app/modules/daftarapar/views/notifikasiapar.views.dart';
import 'package:apar/app/modules/daftarapar/views/scanqrcodeapar.views.dart';
import 'package:apar/app/modules/inspeksi/views/addinspeksi.views.dart';
import 'package:apar/app/modules/inspeksi/views/detailinspeksi.views.dart';
import 'package:apar/app/modules/main/controllers/dashboard.controllers.dart';
import 'package:apar/app/modules/main/views/login.views.dart';
import 'package:apar/config/session.config.dart';
import 'package:apar/ui/Uicolor.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DashboardViews extends StatefulWidget {
  const DashboardViews({Key? key}) : super(key: key);

  @override
  _DashboardViewsState createState() => _DashboardViewsState();
}

class _DashboardViewsState extends State<DashboardViews> {
  late DashboardControlers controllers;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    controllers = new DashboardControlers();
    controllers.getAppVersion();
    controllers.getDataWilayahApar();
    OneSignal.shared.setNotificationWillShowInForegroundHandler(
        (OSNotificationReceivedEvent event) {
      // Will be called whenever a notification is received in foreground
      // Display Notification, pass null param for not displaying the notification
      event.complete(event.notification);
    });

    OneSignal.shared
        .setNotificationOpenedHandler((OSNotificationOpenedResult result) {
      // Will be called whenever a notification is opened/button pressed.
      Get.to(NotifikasiAparViews());
    });

    OneSignal.shared.setPermissionObserver((OSPermissionStateChanges changes) {
      // Will be called whenever the permission changes
      // (ie. user taps Allow on the permission prompt in iOS)
    });

    OneSignal.shared
        .setSubscriptionObserver((OSSubscriptionStateChanges changes) {
      // Will be called whenever the subscription changes
      // (ie. user gets registered with OneSignal and gets a user ID)
    });

    OneSignal.shared.setEmailSubscriptionObserver(
        (OSEmailSubscriptionStateChanges emailChanges) {
      // Will be called whenever then user's email subscription changes
      // (ie. OneSignal.setEmail(email) is called and the user gets registered
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Uicolor.hexToColor(Uicolor.whiteSmooth),
      drawer: Drawer(
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                color: Uicolor.hexToColor(Uicolor.greenPln),
              ),
              child: Text(
                'APAR APPLICATION',
                style: TextStyle(color: Colors.white),
              ),
            ),
            GetBuilder<DashboardControlers>(
              init: controllers,
              builder: (params) {
                if (SessionConfig.wilayah == "") {
                  return Container();
                }
                return ListTile(
                  title: Text('DAFTAR APAR'),
                  onTap: () {
                    Navigator.of(context).pop();
                    Get.to(DaftarAparViews());
                  },
                );
              },
            ),
            GetBuilder<DashboardControlers>(
              init: controllers,
              builder: (params) {
                if (SessionConfig.approval_id == "") {
                  return Container();
                }
                return ListTile(
                  title: Text('APPROVE INSPEKSI APAR'),
                  onTap: () {
                    Navigator.of(context).pop();
                    Get.to(DaftarOutStandingAparViews());
                  },
                );
              },
            ),
            GetBuilder<DashboardControlers>(
              init: controllers,
              builder: (params) {
                if (SessionConfig.wilayah == "") {
                  return Container();
                }
                return ListTile(
                  title: Text('EXPORT XLS APAR'),
                  onTap: () {
                    Navigator.of(context).pop();
                    Get.to(ExportXlsAparViews());
                  },
                );
              },
            ),
            GetBuilder<DashboardControlers>(
              init: controllers,
              builder: (params) {
                if (SessionConfig.wilayah == "UPT") {
                  return ListTile(
                    title: Text('EXPORT RAB APAR'),
                    onTap: () {
                      Navigator.of(context).pop();
                      Get.to(ExportRabAparViews());
                    },
                  );
                }

                return Container();
              },
            ),
            ListTile(
              title: Text('KELUAR APLIKASI'),
              onTap: () async {
                Navigator.of(context).pop();
                SharedPreferences prefs = await SharedPreferences.getInstance();
                prefs.clear();
                Future.delayed(Duration(seconds: 2));
                Get.offAll(LoginViews());
              },
            ),
          ],
        ),
      ),
      appBar: AppBar(
        title: Text(
          "DAFTAR INSPEKSI APAR",
          style: TextStyle(color: Uicolor.hexToColor(Uicolor.whiteSmooth)),
        ),
        backgroundColor: Uicolor.hexToColor(Uicolor.greenPln),
        elevation: 0,
        automaticallyImplyLeading: false,
        actions: <Widget>[
          IconButton(
              icon: Icon(
                Icons.dehaze,
                color: Uicolor.hexToColor(Uicolor.white),
              ),
              onPressed: () {
                _scaffoldKey.currentState!.openDrawer();
              }),
          GetBuilder<DashboardControlers>(
            init: controllers,
            builder: (params) {
              if(SessionConfig.wilayah == ""){
                return Container();
              }
              return IconButton(
                  icon: Icon(
                    Icons.notifications,
                    color: Uicolor.hexToColor(Uicolor.white),
                  ),
                  onPressed: () {
                    Get.to(NotifikasiAparViews());
                  });
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
          child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            // GetBuilder<DashboardControlers>(
            //   init: controllers,
            //   builder: (params) {
            //     return Container(
            //       alignment: Alignment.topLeft,
            //       margin:
            //           EdgeInsets.only(top: 16, left: 24, right: 24, bottom: 16),
            //       child: Text(
            //         "Hari Ini Tanggal : " +
            //             controllers.dateNowApar +
            //             ", Wilayah : ${SessionConfig.wilayah}, Id : ${SessionConfig.id_tujuan}, Approval ID : ${SessionConfig.approval_id}",
            //         style: TextStyle(
            //             fontWeight: FontWeight.bold,
            //             fontStyle: FontStyle.italic),
            //       ),
            //     );
            //   },
            // ),
            Container(
              alignment: Alignment.topLeft,
              padding: EdgeInsets.all(16),
              child: Text("LOKASI APAR", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),),
            ),
            RefreshIndicator(
              child: Container(
                  child: GetBuilder<DashboardControlers>(
                init: controllers,
                builder: (params) {
                  if (!params.loadingProses) {
                    if (params.dataWilayahApar.length > 0) {
                      return ListView.builder(
                        itemCount: params.dataWilayahApar.length,
                        shrinkWrap: true,
                        physics: const BouncingScrollPhysics(
                            parent: AlwaysScrollableScrollPhysics()),
                        itemBuilder: (context, index) {
                          var data = params.dataWilayahApar[index];

                          return Container(
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.all(6),
                              margin: EdgeInsets.only(left: 16, right: 16),
                              child: GestureDetector(
                                child: Card(
                                  elevation: 0,
                                  color: Colors.white,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Container(
                                        alignment: Alignment.topRight,
                                        padding: EdgeInsets.all(8),
                                        child: Text("Wilayah",
                                            style: TextStyle(
                                                color: Uicolor.hexToColor(
                                                    Uicolor.grey),
                                                fontWeight: FontWeight.bold,
                                                fontSize: 18)),
                                      ),
                                      Divider(),
                                      Container(
                                          padding: EdgeInsets.all(8),
                                          child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Container(
                                                  // width: 100,
                                                  // color: Colors.blue,
                                                  alignment:
                                                      Alignment.topCenter,
                                                  child: Text(
                                                    "${data}",
                                                    style: TextStyle(
                                                      color: Colors.green,
                                                      fontSize: 28,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  ),
                                                ),
                                                Divider(),
                                              ])),
                                    ],
                                  ),
                                ),
                                onTap: () {
                                  Get.to(DaftarWilayahAparViews(
                                    wilayah: data,
                                  ));
                                },
                              ));
                        },
                      );
                    } else {
                      return Center(
                        child: Text("Tidak ada data ditemukan"),
                      );
                    }
                  }

                  return Center(
                    child: CircularProgressIndicator(),
                  );
                },
              )),
              onRefresh: () async {
                controllers.getDataWilayahApar();
              },
            ),
            SizedBox(
              height: 8,
            ),
            Divider(),
            Container(
              alignment: Alignment.topLeft,
              padding: EdgeInsets.all(16),
              child: Text("PENGGUNAAN APAR", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),),
            ),
            Container(
              child: Image(image: AssetImage("APPAR.jpg"))
            )
          ],
        ),
      )),
    );
  }
}
