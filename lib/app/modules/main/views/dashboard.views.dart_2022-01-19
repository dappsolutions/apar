import 'package:apar/app/modules/daftarapar/views/daftarapar.views.dart';
import 'package:apar/app/modules/daftarapar/views/exportrabapar.views.dart';
import 'package:apar/app/modules/daftarapar/views/exportxlsapar.views.dart';
import 'package:apar/app/modules/daftarapar/views/notifikasiapar.views.dart';
import 'package:apar/app/modules/inspeksi/views/addinspeksi.views.dart';
import 'package:apar/app/modules/inspeksi/views/detailinspeksi.views.dart';
import 'package:apar/app/modules/main/controllers/dashboard.controllers.dart';
import 'package:apar/app/modules/main/views/login.views.dart';
import 'package:apar/config/session.config.dart';
import 'package:apar/ui/Uicolor.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DashboardViews extends StatefulWidget {
  const DashboardViews({Key? key}) : super(key: key);

  @override
  _DashboardViewsState createState() => _DashboardViewsState();
}

class _DashboardViewsState extends State<DashboardViews> {
  late DashboardControlers controllers;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    controllers = new DashboardControlers();
    controllers.getData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Uicolor.hexToColor(Uicolor.whiteSmooth),
      drawer: Drawer(
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                color: Uicolor.hexToColor(Uicolor.greenPln),
              ),
              child: Text(
                'APAR APPLICATION',
                style: TextStyle(color: Colors.white),
              ),
            ),
            ListTile(
              title: Text('DAFTAR APAR'),
              onTap: () {
                Navigator.of(context).pop();
                Get.to(DaftarAparViews());
              },
            ),
            ListTile(
              title: Text('EXPORT XLS APAR'),
              onTap: () {
                Navigator.of(context).pop();
                Get.to(ExportXlsAparViews());
              },
            ),
            GetBuilder<DashboardControlers>(
              init: controllers,
              builder: (params) {
                if (SessionConfig.wilayah == "UPT") {
                  return ListTile(
                    title: Text('EXPORT RAB APAR'),
                    onTap: () {
                      Navigator.of(context).pop();
                      Get.to(ExportRabAparViews());
                    },
                  );
                }

                return Container();
              },
            ),
            ListTile(
              title: Text('KELUAR APLIKASI'),
              onTap: () async {
                Navigator.of(context).pop();
                SharedPreferences prefs = await SharedPreferences.getInstance();
                prefs.clear();
                Future.delayed(Duration(seconds: 2));
                Get.offAll(LoginViews());
              },
            ),
          ],
        ),
      ),
      appBar: AppBar(
        title: Text(
          "DAFTAR INSPEKSI APAR",
          style: TextStyle(color: Uicolor.hexToColor(Uicolor.whiteSmooth)),
        ),
        backgroundColor: Uicolor.hexToColor(Uicolor.greenPln),
        elevation: 0,
        automaticallyImplyLeading: false,
        actions: <Widget>[
          IconButton(
              icon: Icon(
                Icons.dehaze,
                color: Uicolor.hexToColor(Uicolor.white),
              ),
              onPressed: () {
                _scaffoldKey.currentState!.openDrawer();
              }),
          IconButton(
              icon: Icon(
                Icons.notifications,
                color: Uicolor.hexToColor(Uicolor.white),
              ),
              onPressed: () {
                Get.to(NotifikasiAparViews());
              }),
        ],
      ),
      body: SingleChildScrollView(
          child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            GetBuilder<DashboardControlers>(
              init: controllers,
              builder: (params) {
                return Container(
                  alignment: Alignment.topLeft,
                  margin:
                      EdgeInsets.only(top: 16, left: 24, right: 24, bottom: 16),
                  child: Text(
                    "Hari Ini Tanggal : " +
                        controllers.dateNowApar +
                        ", Wilayah : ${SessionConfig.wilayah}",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontStyle: FontStyle.italic),
                  ),
                );
              },
            ),
            RefreshIndicator(
              child: Container(
                  child: GetBuilder<DashboardControlers>(
                init: controllers,
                builder: (params) {
                  if (!params.loadingProses) {
                    if (params.dataDocument.length > 0) {
                      return ListView.builder(
                        itemCount: params.dataDocument.length,
                        shrinkWrap: true,
                        physics: const BouncingScrollPhysics(
                            parent: AlwaysScrollableScrollPhysics()),
                        itemBuilder: (context, index) {
                          var data = params.dataDocument[index];

                          return Container(
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.all(6),
                              margin: EdgeInsets.only(left: 16, right: 16),
                              child: GestureDetector(
                                child: Card(
                                  elevation: 0,
                                  color: data.status == "APPROVED"
                                      ? Colors.green[100]
                                      : Colors.white,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Container(
                                        alignment: Alignment.topCenter,
                                        padding: EdgeInsets.all(8),
                                        child: Text("${data.no_document}",
                                            style: TextStyle(
                                                color: data.status == "APPROVED"
                                                    ? Colors.black54
                                                    : Uicolor.hexToColor(
                                                        Uicolor.grey),
                                                fontWeight: FontWeight.bold,
                                                fontSize: 18)),
                                      ),
                                      Divider(),
                                      Container(
                                          padding: EdgeInsets.all(8),
                                          child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Container(
                                                  // width: 100,
                                                  // color: Colors.blue,
                                                  child: Text(
                                                    "APAR : ${data.no_alat}",
                                                    style: TextStyle(
                                                        color: Colors.green,
                                                        fontSize: 28,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                ),
                                                Divider(),
                                                Container(
                                                  // color: Colors.red,
                                                  // width: 100,
                                                  alignment: Alignment.topLeft,
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment.end,
                                                    children: [
                                                      Text(
                                                        "${data.nama_pegawai}",
                                                        style: TextStyle(
                                                            color: data.status ==
                                                                    "APPROVED"
                                                                ? Colors.black54
                                                                : Uicolor
                                                                    .hexToColor(
                                                                        Uicolor
                                                                            .grey)),
                                                      ),
                                                      Text(
                                                        "${data.nama_lokasi}",
                                                        style: TextStyle(
                                                            color: data.status ==
                                                                    "APPROVED"
                                                                ? Colors.black54
                                                                : Uicolor
                                                                    .hexToColor(
                                                                        Uicolor
                                                                            .grey)),
                                                      ),
                                                      Text(
                                                        "${data.tempat}",
                                                        style: TextStyle(
                                                            color: data.status ==
                                                                    "APPROVED"
                                                                ? Colors.black54
                                                                : Uicolor
                                                                    .hexToColor(
                                                                        Uicolor
                                                                            .grey)),
                                                      ),
                                                      Text(
                                                        "${data.period_start}",
                                                        style: TextStyle(
                                                            color: data.status ==
                                                                    "APPROVED"
                                                                ? Colors.black54
                                                                : Uicolor
                                                                    .hexToColor(
                                                                        Uicolor
                                                                            .grey)),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ])),
                                    ],
                                  ),
                                ),
                                onTap: () {
                                  // goto deetail inspeksi
                                  Get.to(DetailInspeksiViews(
                                    no_apar: data.no_document,
                                    status: data.status,
                                  ));
                                },
                              ));
                        },
                      );
                    } else {
                      return Center(
                        child: Text("Tidak ada data ditemukan"),
                      );
                    }
                  }

                  return Center(
                    child: CircularProgressIndicator(),
                  );
                },
              )),
              onRefresh: () async {
                controllers.getData();
              },
            ),
            SizedBox(
              height: 8,
            ),
            Divider(),
          ],
        ),
      )),
      floatingActionButton: Container(
        width: MediaQuery.of(context).size.width,
        alignment: Alignment.bottomCenter,
        child: RaisedButton(
          // child: Icon(Icons.refresh, color: Colors.white,),
          child: Text(
            "Tampilkan Lebih Banyak",
            style: TextStyle(color: Colors.black),
          ),
          color: Uicolor.hexToColor(Uicolor.grey_young),
          onPressed: () {
            controllers.getDataLoadMore();
          },
          elevation: 0,
          // shape:CircleBorder(),
        ),
      ),
    );
  }
}
