import 'package:apar/app/modules/daftarapar/views/listapar.views.dart';
import 'package:apar/app/modules/daftarapar/views/scanqrcodeapar.views.dart';
import 'package:apar/app/modules/inspeksi/views/addinspeksi.views.dart';
import 'package:apar/app/modules/inspeksi/views/detailinspeksi.views.dart';
import 'package:apar/app/modules/lokasi/views/lokasi.views.dart';
import 'package:apar/app/modules/main/controllers/floorapar.controllers.dart';
import 'package:apar/app/modules/main/models/apardetail.models.dart';
import 'package:apar/config/session.config.dart';
import 'package:apar/ui/Uicolor.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_treemap/treemap.dart';

class FloorAparViews extends StatefulWidget {
  String wilayah;
  String nama_wilayah;
  String id_lokasi;
  FloorAparViews(
      {required this.wilayah,
      required this.nama_wilayah,
      required this.id_lokasi});

  @override
  _FloorAparViewsState createState() => _FloorAparViewsState();
}

class _FloorAparViewsState extends State<FloorAparViews> {
  late FloorAparControllers controllers;

  @override
  void initState() {
    super.initState();
    controllers = new FloorAparControllers();
    controllers.getDataTempat(widget.wilayah, widget.id_lokasi);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('DENAH APAR ${widget.nama_wilayah}'),
          backgroundColor: Uicolor.hexToColor(Uicolor.greenPln),
          elevation: 0,
        ),
        backgroundColor: Uicolor.hexToColor(Uicolor.whiteSmooth),
        body: SingleChildScrollView(
          child: Container(
            child: Column(
              children: [
                Container(
                  alignment: Alignment.topLeft,
                  height: 30,
                  margin:
                      EdgeInsets.only(top: 8, left: 16, right: 16, bottom: 8),
                  child: GetBuilder<FloorAparControllers>(
                    init: controllers,
                    builder: (params) {
                      return Text(
                        "Periode : ${params.dateNowApar}",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontStyle: FontStyle.italic,
                            fontSize: 22),
                      );
                    },
                  ),
                ),
                Container(
                    alignment: Alignment.topRight,
                    margin: EdgeInsets.only(right: 16),
                    child: ElevatedButton(
                      child: Text("Lokasi"),
                      onPressed: () {
                        Get.to(LokasiViews());
                      },
                      style: ElevatedButton.styleFrom(
                        primary: Colors.red, // background
                        onPrimary: Colors.white, // foreground
                      ),
                    )),
                Container(
                    height: MediaQuery.of(context).size.height,
                    child: GetBuilder<FloorAparControllers>(
                      init: controllers,
                      builder: (params) {
                        if (!params.loadingProses) {
                          return SfTreemap(
                            dataCount: params.data.length,
                            weightValueMapper: (int index) {
                              return 1.1;
                            },
                            levels: <TreemapLevel>[
                              TreemapLevel(
                                groupMapper: (int index) {
                                  return params.data[index].tempat;
                                },
                                color: Uicolor.hexToColor(Uicolor.grey_young),
                                labelBuilder:
                                    (BuildContext context, TreemapTile tile) {
                                  return Container(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                            padding: const EdgeInsets.all(8.5),
                                            child: Text(
                                              tile.group,
                                              style: const TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold),
                                            )),
                                      ],
                                    ),
                                  );
                                },
                                tooltipBuilder:
                                    (BuildContext context, TreemapTile tile) {
                                  return Container(
                                      color: Colors.white70,
                                      padding: EdgeInsets.all(6),
                                      child: Text(
                                          '''Lokasi Tempat          : ${tile.group}\n''',
                                          style: const TextStyle(
                                              color: Colors.black54)));
                                },
                              ),
                              TreemapLevel(
                                groupMapper: (int index) {
                                  // return params.data[index].jumlah_item;
                                  return index.toString();
                                },
                                color: Uicolor.hexToColor(Uicolor.grey_young),
                                labelBuilder:
                                    (BuildContext context, TreemapTile tile) {
                                  return Container(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                            padding: const EdgeInsets.only(
                                                left: 8.5),
                                            child: Text(
                                              "Jumlah Apar : ${params.data[int.parse(tile.group)].jumlah_item}\nSudah Inspeksi : ${params.data[int.parse(tile.group)].jumlah_item_sudah_inspeksi}",
                                              style: const TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 9,
                                                  fontStyle: FontStyle.italic),
                                            )),
                                        Divider()
                                      ],
                                    ),
                                  );
                                },
                              ),
                              // TreemapLevel(
                              //   groupMapper: (int index) {
                              //     return index.toString();
                              //   },
                              //   color: Uicolor.hexToColor(Uicolor.grey_young),
                              //   labelBuilder:
                              //       (BuildContext context, TreemapTile tile) {
                              //     return Container(
                              //       child: Container(
                              //           padding: const EdgeInsets.all(8.5),
                              //           child: ListView.builder(
                              //             itemCount: params
                              //                 .data[int.parse(tile.group)]
                              //                 .item
                              //                 .length,
                              //             shrinkWrap: true,
                              //             physics: ClampingScrollPhysics(),
                              //             itemBuilder: (context, index) {
                              //               var data = params
                              //                   .data[int.parse(tile.group)]
                              //                   .item[index];
                              //               return Container(
                              //                   color: data.has_pemeliharaan ==
                              //                           '1'
                              //                       ? Colors.green[200]
                              //                       : data.has_pemeliharaan ==
                              //                               '-1'
                              //                           ? Colors.yellow[50]
                              //                           : Colors.orange[200],
                              //                   alignment: Alignment.topCenter,
                              //                   margin: EdgeInsets.only(
                              //                       top: 10,
                              //                       left: 16,
                              //                       right: 16),
                              //                   height: 35,
                              //                   child: GestureDetector(
                              //                     child: Container(
                              //                       padding: EdgeInsets.all(6),
                              //                       margin:
                              //                           EdgeInsets.only(top: 3),
                              //                       child: Text(
                              //                         "APAR : ${data.no_alat}",
                              //                         style: TextStyle(
                              //                             fontWeight:
                              //                                 FontWeight.bold,
                              //                             fontSize: 12),
                              //                       ),
                              //                     ),
                              //                     onTap: () {
                              //                       if (SessionConfig.wilayah ==
                              //                           "") {
                              //                             Get.snackbar('Informasi', "Anda Tidak Punya Akses untuk Fitur Ini");
                              //                       } else {
                              //                         if (data.no_document !=
                              //                             '') {
                              //                           // goto deetail inspeksi
                              //                           Get.to(DetailInspeksiViews(
                              //                               no_apar: data
                              //                                   .no_document,
                              //                               status: data.status,
                              //                               no_alat:
                              //                                   data.no_alat,
                              //                               id_alat:
                              //                                   data.id_alat,
                              //                               lokasi_tujuan: data
                              //                                   .lokasi_tujuan));
                              //                         } else {
                              //                           Get.to(AddInspeksiViews(
                              //                             no_apar: data.no_alat,
                              //                             id_apar: data.id_alat,
                              //                             lokasi_tujuan: data
                              //                                 .lokasi_tujuan,
                              //                           ));
                              //                         }
                              //                       }
                              //                     },
                              //                   ));
                              //             },
                              //           )),
                              //     );
                              //   },
                              // ),
                              TreemapLevel(
                                groupMapper: (int index) {
                                  return index.toString();
                                },
                                color: Uicolor.hexToColor(Uicolor.grey_young),
                                labelBuilder:
                                    (BuildContext context, TreemapTile tile) {
                                  return Container(
                                      child: Container(
                                          child: GestureDetector(
                                    child: Container(
                                      height: 50,
                                      child: RaisedButton(
                                          color: Colors.orange[200],
                                          child: Text("Lihat Detail",
                                            style: TextStyle(
                                                fontSize: 8,
                                                color: Uicolor.hexToColor(
                                                    Uicolor.black)),
                                          ),
                                          onPressed: () {
                                            Get.to(ListDetailApar(
                                              nama_wilayah: widget.nama_wilayah,
                                              tempat: params.data[int.parse(tile.group)].tempat,
                                              wilayah: SessionConfig.wilayah,
                                                data: params.data[
                                                    int.parse(tile.group)].item));
                                          }),
                                    ),
                                    onTap: () {
                                      Get.to(ListDetailApar(
                                        nama_wilayah: widget.nama_wilayah,
                                        tempat: params.data[int.parse(tile.group)].tempat,
                                        wilayah: SessionConfig.wilayah,
                                          data: params
                                              .data[int.parse(tile.group)].item));
                                    },
                                  )));
                                },
                              ),
                            ],
                          );
                        }

                        return Container(
                          child: Center(
                            child: CircularProgressIndicator(),
                          ),
                        );
                      },
                    ))
              ],
            ),
          ),
        ),
        floatingActionButton: GetBuilder<FloorAparControllers>(
          init: controllers,
          builder: (params) {
            if (SessionConfig.wilayah == "") {
              return Container();
            }
            return FloatingActionButton(
                child: Icon(Icons.center_focus_weak),
                backgroundColor: Colors.orange[300],
                onPressed: () {
                  Get.to(ScanQrCodeAparViews(
                    wilayah: widget.nama_wilayah,
                    id_wilayah: widget.id_lokasi,
                  ));
                });
          },
        ));
  }
}
