import 'dart:async';

import 'package:apar/app/modules/connection/views/lostconnect.views.dart';
import 'package:apar/app/modules/main/controllers/main.controllers.dart';
import 'package:apar/app/modules/main/views/dashboard.views.dart';
import 'package:apar/app/modules/main/views/login.views.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class MainViews extends StatefulWidget {
  const MainViews({Key? key}) : super(key: key);

  @override
  _MainViewsState createState() => _MainViewsState();
}

class _MainViewsState extends State<MainViews> {
  late MainControllers controllers;
  String _connectionStatus = 'Unknown';
  final Connectivity _connectivity = Connectivity();
  late StreamSubscription<ConnectivityResult> _connectivitySubscription;

// Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result = ConnectivityResult.none;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
      case ConnectivityResult.mobile:
      case ConnectivityResult.none:
        setState(() => _connectionStatus = result.toString());
        break;
      default:
        setState(() => _connectionStatus = 'Failed to get connectivity.');
        break;
    }
  }

  @override
  void initState() {
    super.initState();
    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    controllers = new MainControllers();
    controllers.checkSudahLogin();
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GetBuilder<MainControllers>(
        init: controllers,
        builder: (params) {
          if (params.hasLogin) {
            return _connectionStatus.toLowerCase() == 'connectivityresult.none' || _connectionStatus.toLowerCase() == 'failed to get connectivity.' ? LostConnectionViews() : DashboardViews();
          }
          return _connectionStatus.toLowerCase() == 'connectivityresult.none' || _connectionStatus.toLowerCase() == 'failed to get connectivity.' ? LostConnectionViews() : LoginViews();
        },
      ),
    );
  }
}
