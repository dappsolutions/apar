

class MessageModel{
  late String is_valid;
  late String message;
  MessageModel({required this.is_valid, required this.message});

  MessageModel.fromJson(Map<String, dynamic> json){
    this.is_valid = json['is_valid'];
    this.message = json['message'];
  }
}