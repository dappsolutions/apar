
import 'package:apar/app/modules/inspeksi/models/inspeksi.models.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';

/// An object to set the employee collection data source to the datagrid. This
/// is used to map the employee data to the datagrid widget.
class InspeksiDataSource extends DataGridSource {
  /// Creates the employee data source class with required details.
  InspeksiDataSource({required List<InspeksiModels> sourceData}) {
    _inspeksiData = sourceData
        .map<DataGridRow>((e) => DataGridRow(cells: [
              DataGridCell<int>(columnName: 'id', value: e.id),
              DataGridCell<String>(columnName: 'tanggal', value: e.tanggal),
              DataGridCell<String>(
                  columnName: 'pembersihan', value: e.pembersihan),
              DataGridCell<String>(columnName: 'berat', value: e.berat),
              DataGridCell<String>(columnName: 'tekanan', value: e.tekanan),
            ]))
        .toList();
  }

  List<DataGridRow> _inspeksiData = [];

  @override
  List<DataGridRow> get rows => _inspeksiData;

  @override
  DataGridRowAdapter buildRow(DataGridRow row) {
    return DataGridRowAdapter(
        cells: row.getCells().map<Widget>((e) {
      return Container(
        alignment: Alignment.center,
        padding: EdgeInsets.all(8.0),
        child: Text(e.value.toString()),
      );
    }).toList());
  }
}