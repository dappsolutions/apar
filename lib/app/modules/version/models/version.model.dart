




class VersionModels{
  late String app;
  late String version;

  VersionModels({required this.app, required this.version});

  VersionModels.fromJson(Map<String, dynamic> json){
    this.app = json['app'].toString();
    this.version = json['version'] == null ? '-' : json['version'].toString();    
  }
}