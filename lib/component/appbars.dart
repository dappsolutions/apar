import 'package:flutter/material.dart';
import 'package:apar/component/textlabel.dart';
import 'package:apar/ui/Uicolor.dart';

class AppBarComponent {
  static AppBar build(BuildContext context, String? title) {
    return AppBar(
      title: Text(title ?? '-'),
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.search),
          onPressed: () {
            // Navigator.pushNamed(context, '/search');
          },
        ),
        IconButton(
          icon: Icon(Icons.settings),
          onPressed: () {
            // Navigator.pushNamed(context, '/settings');
          },
        ),
      ],
    );
  }

  static AppBar buildNormal(
      BuildContext context, String? title, Function callback) {
    return AppBar(
      title: Text(
        title ?? '-',
        style: TextStyle(
            color: Uicolor.hexToColor(Uicolor.black_young),
            fontWeight: FontWeight.bold,
            fontSize: 20),
      ),
      backgroundColor: Uicolor.hexToColor(Uicolor.white),
      elevation: 0,
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back,
          color: Uicolor.hexToColor(Uicolor.black),
        ),
        onPressed: () {
          callback();
        },
      ),
    );
  }

  static AppBar buildWithNotification(String? title, Function callback, {String? counterNotif}) {
    return AppBar(
      title: Text(
        title ?? '-',
        style: TextStyle(
            color: Uicolor.hexToColor(Uicolor.black_young),
            fontWeight: FontWeight.bold,
            fontSize: 20),
      ),
      backgroundColor: Uicolor.hexToColor(Uicolor.white),
      elevation: 0,
      automaticallyImplyLeading: false,
      actions: [
        IconButton(
            icon: Icon(
              Icons.notifications,
              color: Uicolor.hexToColor(Uicolor.grey),
            ),
            onPressed: () {
              callback();
            }),

            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.only(right: 8),
                    child: Text(counterNotif ?? '-', style: TextStyle(color: Colors.orange[400], fontSize: 18, fontWeight: FontWeight.w900),),
                  )
                ],
              )
            )
      ],
    );
  }
}
