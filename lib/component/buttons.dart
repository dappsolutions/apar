import 'package:flutter/material.dart';
import 'package:apar/component/textlabel.dart';
import 'package:apar/ui/Uicolor.dart';

class ButtonComponent {
  static Widget ButtonFlat(BuildContext context, Function callback,
      {String? text, double? elevation, double? width, double? height, Color? backgroundColor, Color? textColor}) {
    return Container(
        margin: EdgeInsets.only(left: 16, right: 16, top: 16),
        height: height ?? MediaQuery.of(context).size.height * 0.07,
        width: width ?? double.infinity,
        child: RaisedButton(
          child: Text(
            text ?? '-',
            style: TextStyle(color: textColor ?? Colors.white),
          ),
          color: backgroundColor ?? Uicolor.hexToColor(Uicolor.greenPln),
          onPressed: () {
            callback();
          },
          elevation: elevation ?? 0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(3.0),
          ),
        ));
  }

  static Widget ButtonOutline(BuildContext context, Function callback, {String? title}) {
    return GestureDetector(
      child: Container(
          height: 50,
          width: MediaQuery.of(context).size.width,
          margin: EdgeInsets.only(left: 16, right: 16, top: 16),
          padding: EdgeInsets.all(16),
          decoration: BoxDecoration(
              border: Border.all(
                color: Uicolor.hexToColor(Uicolor.greenPln),
              ),
              borderRadius: BorderRadius.all(Radius.circular(5))),
          child: Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width * 0.5,
                  child: TextLabelComponent.show(
                      text: title ?? "-",
                      fontWeight: FontWeight.bold,
                      fontSize: 12,
                      color: Uicolor.hexToColor(Uicolor.greenPln),
                      textAlign: TextAlign.right),
                ),
                Container(
                  width: 30,
                  child: Icon(
                    Icons.keyboard_arrow_right,
                    color: Uicolor.hexToColor(Uicolor.greenPln),
                  ),
                ),
              ],
            ),
          )),
      onTap: () {
        callback();
      },
    );
  }
}
