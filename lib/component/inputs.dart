import 'package:flutter/material.dart';
import 'package:apar/ui/Uicolor.dart';
import 'package:get/get.dart';
import 'package:apar/utils/datepickers.dart';

class InputComponent {
  static Widget textDatePicker(Function callback,
      {required BuildContext context,
      String? hintText,
      String? initVal,
      bool? hoursFormat = false}) {
    return Container(
        padding: EdgeInsets.only(left: 16, right: 16),
        height: MediaQuery.of(context).size.height * 0.09,
        child: Material(
            borderRadius: BorderRadius.circular(3),
            shadowColor: Colors.grey,
            elevation: 0,
            child: TextFormField(
              autocorrect: true,
              initialValue: initVal ?? "",
              decoration: InputDecoration(
                // prefixIcon: Icon(Icons.search)
                // ,
                hintText: hintText ?? '',
                prefixIcon: GestureDetector(
                  onTap: () {
                    DatePickers.show(context, (date) {
                      if (hoursFormat == false) {
                        List<String> dataDate = date.toString().split(' ');
                        // print("TES ${dataDate[0].trim().replaceAll('0000-00-00', '')}");
                        callback(
                            dataDate[0].trim().replaceAll('0000-00-00', ''));
                      } else {
                        callback(date);
                      }
                    });
                  },
                  child: Icon(
                    Icons.date_range,
                    color: Uicolor.hexToColor(Uicolor.greenPln),
                  ),
                ),
                hintStyle:
                    TextStyle(color: Uicolor.hexToColor(Uicolor.greenPln)),
                filled: true,
                fillColor: Uicolor.hexToColor(Uicolor.white),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  borderSide: BorderSide(color: Colors.white, width: 1),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  borderSide: BorderSide(color: Colors.white),
                ),
              ),
              onFieldSubmitted: (value) async {},
              onChanged: (value) {},
              onTap: () {
                DatePickers.show(context, (date) {
                  if (hoursFormat == false) {
                    List<String> dataDate = date.toString().split(' ');
                    // print("TES ${dataDate[0].trim().replaceAll('0000-00-00', '')}");
                    callback(dataDate[0].trim().replaceAll('0000-00-00', ''));
                  } else {
                    callback(date);
                  }
                });
              },
            )));
  }

  static Widget text(Function callback,
      {required BuildContext context,
      String? hintText,
      String? initVal,
      bool? isPassword}) {
    return Container(
        padding: EdgeInsets.only(left: 16, right: 16),
        height: MediaQuery.of(context).size.height * 0.09,
        child: Material(
            borderRadius: BorderRadius.circular(3),
            shadowColor: Colors.grey,
            elevation: 0,
            child: TextFormField(
              autocorrect: true,
              initialValue: initVal ?? "",
              decoration: InputDecoration(
                // prefixIcon: Icon(Icons.search)
                // ,
                hintText: hintText ?? '',
                prefixIcon: GestureDetector(
                  onTap: () {},
                  child: Icon(
                    Icons.description_outlined,
                    color: Uicolor.hexToColor(Uicolor.greenPln),
                  ),
                ),
                hintStyle:
                    TextStyle(color: Uicolor.hexToColor(Uicolor.greenPln)),
                filled: true,
                fillColor: Uicolor.hexToColor(Uicolor.white),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  borderSide: BorderSide(color: Colors.white, width: 1),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  borderSide: BorderSide(color: Colors.white),
                ),
              ),
              obscureText: isPassword ?? false,
              onFieldSubmitted: (value) async {},
              onChanged: (value) {
                callback(value);
              },
            )));
  }

  static Widget textNumber(Function callback,
      {required BuildContext context,
      String? hintText,
      String? initVal,
      bool? isPassword}) {
    return Container(
        padding: EdgeInsets.only(left: 16, right: 16),
        height: MediaQuery.of(context).size.height * 0.09,
        child: Material(
            borderRadius: BorderRadius.circular(3),
            shadowColor: Colors.grey,
            elevation: 0,
            child: TextFormField(
              autocorrect: true,
              initialValue: initVal ?? "",
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                // prefixIcon: Icon(Icons.search)
                // ,
                hintText: hintText ?? '',
                prefixIcon: GestureDetector(
                  onTap: () {},
                  child: Icon(
                    Icons.description_outlined,
                    color: Uicolor.hexToColor(Uicolor.greenPln),
                  ),
                ),
                hintStyle:
                    TextStyle(color: Uicolor.hexToColor(Uicolor.greenPln)),
                filled: true,
                fillColor: Uicolor.hexToColor(Uicolor.white),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  borderSide: BorderSide(color: Colors.white, width: 1),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  borderSide: BorderSide(color: Colors.white),
                ),
              ),
              obscureText: isPassword ?? false,
              onFieldSubmitted: (value) async {},
              onChanged: (value) {
                callback(value);
              },
            )));
  }

  static Widget textArea(Function callback,
      {required BuildContext context,
      String? hintText,
      String? initVal,
      bool? isPassword}) {
    return Container(
        padding: EdgeInsets.only(left: 16, right: 16),
        height: MediaQuery.of(context).size.height * 0.09,
        child: Material(
            borderRadius: BorderRadius.circular(3),
            shadowColor: Colors.grey,
            // color: Colors.red,
            elevation: 0,
            child: TextFormField(
              autocorrect: true,
              initialValue: initVal ?? "",
              keyboardType: TextInputType.multiline,
              minLines: null, //Normal textInputField will be displayed
              maxLines: null,
              expands: true,
              decoration: InputDecoration(
                // prefixIcon: Icon(Icons.search)
                // ,
                hintText: hintText ?? '',
                prefixIcon: GestureDetector(
                  onTap: () {},
                  child: Icon(
                    Icons.description_outlined,
                    color: Uicolor.hexToColor(Uicolor.greenPln),
                  ),
                ),
                hintStyle:
                    TextStyle(color: Uicolor.hexToColor(Uicolor.greenPln)),
                filled: true,
                fillColor: Uicolor.hexToColor(Uicolor.white),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  borderSide: BorderSide(color: Colors.white, width: 1),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  borderSide: BorderSide(color: Colors.white),
                ),
              ),
              obscureText: isPassword ?? false,
              onFieldSubmitted: (value) async {},
              onChanged: (value) {
                callback(value);
              },
            )));
  }

  static Widget selectBox(
    Function callback,
    BuildContext context, {
    String? initVal,
    String? hintText,
    List<String>? options,
  }) {
    return Container(
        height: MediaQuery.of(context).size.height * 0.08,
        // width: 300,
        margin: EdgeInsets.only(right: 16, left: 16, top: 8),
        child: InputDecorator(
          decoration: const InputDecoration(border: OutlineInputBorder()),
          child: DropdownButtonHideUnderline(
              child: DropdownButton(
            items: options?.map((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
            value: initVal,
            hint: Text(hintText ?? 'Pilih'),
            onChanged: (val) {
              callback(val);
            },
          )),
        ));
  }
}
