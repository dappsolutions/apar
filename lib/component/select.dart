import 'package:flutter/material.dart';
import 'package:apar/component/appbars.dart';
import 'package:apar/component/buttons.dart';
import 'package:apar/component/textlabel.dart';
import 'package:apar/ui/Uicolor.dart';
import 'package:get/get.dart';

class SelectData extends StatelessWidget {
  String? tooltip;
  String? title;
  String? titleContent;
  List<SelectModels> data;
  SelectData({this.tooltip, this.title, this.titleContent, required this.data});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarComponent.buildNormal(context, title ?? 'Data', () {
        Get.back(result: "");
      }),
      body: ListView.builder(
        itemCount: data.length,
        itemBuilder: (context, index) {
          final SelectModels person = data[index];
          return Container(
            padding: EdgeInsets.all(8),
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: Container(
                          child: TextLabelComponent.show(
                              text: "${person.title.toUpperCase()}",
                              fontSize: 14),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  alignment: Alignment.topRight,
                  child: ButtonComponent.ButtonFlat(context, () {
                    Get.back(result: person.id+" - "+person.title);
                  }, width: 100, height: 22, text: "Pilih"),
                ),
                SizedBox(
                  height: 10,
                ),
                Divider()
              ],
            ),
          );
        },
      ),
      // floatingActionButton: FloatingActionButton(
      //   tooltip: tooltip ?? 'Cari',
      //   backgroundColor: Uicolor.hexToColor(Uicolor.blueYoung),
      //   onPressed: () => showSearch(
      //     context: context,
      //     delegate: SearchPage<SelectModels>(
      //         onQueryUpdate: (s) => print(s),
      //         items: people,
      //         searchLabel: tooltip ?? 'Cari',
      //         suggestion: Center(
      //           child: Text(titleContent ?? 'Cari Data'),
      //         ),
      //         barTheme: Theme.of(context).copyWith(
      //           primaryColor: Colors.white,
      //           primaryIconTheme: Theme.of(context).primaryIconTheme.copyWith(color: Colors.black),
      //           // primaryColorBrightness: Brightness.dark,
      //           textTheme: Theme.of(context).textTheme.copyWith(
      //             headline1: TextStyle(fontWeight: FontWeight.normal),
      //           ),
      //           // these ↓ do not work ☹️
      //           appBarTheme: Theme.of(context).appBarTheme.copyWith(color: Colors.black12, elevation: 0, ),
      //           // inputDecorationTheme: theme.inputDecorationTheme.copyWith(border: UnderlineInputBorder()),
      //         ),
      //         failure: NotFoundViews(),
      //         filter: (person) => [
      //               person.id,
      //               person.title,
      //               person.content.toString(),
      //             ],
      //         builder: (person) => Container(
      //               padding: EdgeInsets.all(16),
      //               child: Column(
      //                 mainAxisAlignment: MainAxisAlignment.start,
      //                 children: [
      //                   Container(
      //                     child: Row(
      //                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
      //                       children: [
      //                         Container(
      //                           child: TextLabelComponent.show(
      //                               text: "${person.title}"),
      //                         ),
      //                         Container(
      //                           child: ButtonComponent.ButtonFlat(
      //                               context, () {
      //                                 Navigator.pop(context);
      //                                 // Get.back(result: person.title.toString());
      //                               },
      //                               width: 100, height: 22, text: "Pilih"),
      //                         ),
      //                       ],
      //                     ),
      //                   ),
      //                   SizedBox(
      //                     height: 10,
      //                   ),
      //                   Divider()
      //                 ],
      //               ),
      //             )),
      //   ),
      //   child: Icon(
      //     Icons.search,
      //   ),
      // ),
    );
  }
}

class SelectModels {
  late String id, title;
  late String content;

  SelectModels(this.id, this.title, this.content);

  SelectModels.fromJson(Map<String, dynamic> json) {
    this.id = json['id'].toString();
    this.title = json['title'].toString();
    this.content = json['content'].toString();
  }
}
