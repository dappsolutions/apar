import 'package:flutter/widgets.dart';
import 'package:apar/ui/Uicolor.dart';

class TextLabelComponent {
  static Widget show(
      {Color? color,
      required String text,
      double? fontSize,
      FontWeight? fontWeight,
      TextAlign? textAlign}) {
    return Container(
      child: Text(
        text,
        style: TextStyle(
            fontSize: fontSize ?? 16,
            fontWeight: fontWeight ?? FontWeight.normal,
            color: color ?? Uicolor.hexToColor(Uicolor.black)),
        textAlign: textAlign ?? TextAlign.left,
      ),
    );
  }
}
