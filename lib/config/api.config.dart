
import 'package:apar/config/modules.config.dart';
import 'package:apar/config/routes.config.dart';
import 'package:apar/config/url.config.dart';

class Api{
  static Map route = {
    ModulesConfig.MODULE_LOGIN: {
      Routes.SIGN_IN:"${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_LOGIN}/${Routes.SIGN_IN}"
    },
    ModulesConfig.MODULE_PERMIT: {
      Routes.GETDATA:"${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_PERMIT}/${Routes.GETDATA}",
      Routes.GETDATASEARCH:"${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_PERMIT}/${Routes.GETDATASEARCH}",
      Routes.GET_DETAIL_DATA:"${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_PERMIT}/${Routes.GET_DETAIL_DATA}",
      Routes.GET_LIST_SIDAK:"${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_PERMIT}/${Routes.GET_LIST_SIDAK}",
      Routes.GET_LIST_SIDAK_SEARCH:"${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_PERMIT}/${Routes.GET_LIST_SIDAK_SEARCH}",
    },
    ModulesConfig.MODULE_DOCUMENT: {
      Routes.GETDATA:"${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_DOCUMENT}/${Routes.GETDATA}",
    },
    ModulesConfig.MODULE_ALAT: {
      Routes.GETDATA:"${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_ALAT}/${Routes.GETDATA}",
      Routes.GETDATAFILTER:"${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_ALAT}/${Routes.GETDATAFILTER}",
      Routes.GETDATAFILTERRAB:"${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_ALAT}/${Routes.GETDATAFILTERRAB}",
      Routes.GETDATANOTIF:"${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_ALAT}/${Routes.GETDATANOTIF}",
      Routes.GETDATAWILAYAH:"${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_ALAT}/${Routes.GETDATAWILAYAH}",
      Routes.GETDATATEMPATLOKASI:"${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_ALAT}/${Routes.GETDATATEMPATLOKASI}",
      Routes.GETDATADETAILTEMPATLOKASI:"${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_ALAT}/${Routes.GETDATADETAILTEMPATLOKASI}",
      Routes.GETDATAAPPROVAL:"${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_ALAT}/${Routes.GETDATAAPPROVAL}",
    },
    ModulesConfig.MODULE_SIDAK: {
      Routes.SIMPAN:"${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_SIDAK}/${Routes.SIMPAN}",
      Routes.SIMPANALL:"${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_SIDAK}/${Routes.SIMPANALL}",
      Routes.BATAL:"${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_SIDAK}/${Routes.BATAL}",
      Routes.SIMPANGAMBAR:"${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_SIDAK}/${Routes.SIMPANGAMBAR}",
      Routes.SIMPANPERUBAHAN:"${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_SIDAK}/${Routes.SIMPANPERUBAHAN}",
      Routes.UBAHKETERANGAN:"${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_SIDAK}/${Routes.UBAHKETERANGAN}",
      Routes.SELESAIPROSES:"${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_SIDAK}/${Routes.SELESAIPROSES}",
      Routes.CEKSCORE:"${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_SIDAK}/${Routes.CEKSCORE}",
      Routes.GET_DETAIL_DATA:"${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_SIDAK}/${Routes.GET_DETAIL_DATA}",
      Routes.GET_DETAIL_IMAGE:"${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_SIDAK}/${Routes.GET_DETAIL_IMAGE}",
      Routes.GET_LIST_DATA:"${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_SIDAK}/${Routes.GET_LIST_DATA}",
      Routes.APPROVE:"${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_SIDAK}/${Routes.APPROVE}",
    },
     ModulesConfig.MODULE_APPS: {
      Routes.GETVERSION:"${UrlConfig.BASE_URL}/${ModulesConfig.MODULE_APPS}/${Routes.GETVERSION}",
    },
  };
}