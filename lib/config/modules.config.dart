
class ModulesConfig {
  static String MODULE_LOGIN = "login";
  static String MODULE_PERMIT = "permit";
  static String MODULE_DOCUMENT = "document";
  static String MODULE_APPS = "apps";
  static String MODULE_ALAT = "alat";
  static String MODULE_SIDAK = "sidak";
}