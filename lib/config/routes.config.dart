
class Routes{
  static String SIGN_IN = "signIn";
  static String GETDATA = "getData";
  static String GETDATAAPPROVAL = "getDataApprovalData";
  static String GETVERSION = "getAppVersion";
  static String GETDATAFILTER = "getDataFilter";
  static String GETDATAFILTERRAB = "getDataFilterRabApar";
  static String GETDATAWILAYAH = "getDataAlatByWilayah";
  static String GETDATATEMPATLOKASI = "getDataTempatLokasiApar";
  static String GETDATADETAILTEMPATLOKASI = "getDataDetailTempatLokasiApar";
  static String GETDATANOTIF = "getDataAllAparNotifikasiBelumInspeksi";
  static String GETDATASEARCH = "getDataSearch";
  static String GET_DETAIL_DATA = "getDetailData";
  static String SIMPAN = "simpan";
  static String SIMPANPERUBAHAN = "simpanPerubahan";
  static String UBAHKETERANGAN = "ubahKeterangan";
  static String APPROVE = "approve";
  static String CEKSCORE = "cekHasilPenilaianScoreSafety";
  static String GET_LIST_SIDAK = "getListWpSudahSidak";
  static String GET_LIST_DATA = "getListDetailData";
  static String GET_LIST_SIDAK_SEARCH = "getListWpSudahSidakSearch";
  static String GET_DETAIL_IMAGE = "getDetailImage";
  static String SIMPANALL = "simpanAll";
  static String BATAL = "batalPengajuan";
  static String SIMPANGAMBAR = "simpanGambar";
  static String SELESAIPROSES = "selesaiProses";
}