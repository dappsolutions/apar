class SessionConfig {
  static String user_id = "";
  static String upt_id = "";
  static String wilayah = "";
  static String lokasi_tujuan = "";
  static String id_tujuan = "";
  static String approval_id = "";

  static sessionSet(user, String upt, String wilayah, String lokasi_tujuan,
      String id_tujuan, String approval_id) {
    user_id = user;
    upt_id = upt;
    wilayah = wilayah;
    lokasi_tujuan = lokasi_tujuan;
    id_tujuan = id_tujuan;
    approval_id = approval_id;
  }

  static sessionClear() {
    user_id = "";
    upt_id = "";
    wilayah = "";
    lokasi_tujuan = "";
    id_tujuan = "";
    approval_id = "";
  }
}
