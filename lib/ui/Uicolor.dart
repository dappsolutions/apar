


import 'package:flutter/material.dart';

class Uicolor {
  static String green = "#51bb77";
  static String greenPln = "#107ba3";
  static String grey_young = "#e6e6e6";
  static String black = "#06140d";
  static String black_young = "#595959";
  static String white_grey = "#f2f2f2";
  static String grey = "#c7c7c7";
  static String white = "#ffffff";
  static String whiteSmooth = "#f7f7f7";
  static String yellowSmooth = "#f4f07e";
  static String green_smooth = "#cee5d1";
  static String blueSmooth = "#0b749f";


  static Color hexToColor(String code) {
    return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }
}