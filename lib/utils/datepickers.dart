import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:apar/ui/Uicolor.dart';

class DatePickers {
  static Future<void> show(BuildContext context, Function callback) async {
    DatePicker.showDatePicker(context,
        showTitleActions: true,
        // minTime: DateTime(2018, 3, 5),
        // maxTime: DateTime(2019, 6, 7),
        theme: DatePickerTheme(
            headerColor: Uicolor.hexToColor(Uicolor.greenPln),
            backgroundColor: Colors.white,
            itemStyle: TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold, fontSize: 18),
            doneStyle: TextStyle(color: Colors.white, fontSize: 16)),
        onChanged: (date) {
      // print('change $date in time zone ' +
      //     date.timeZoneOffset.inHours.toString());
      return callback(date);
    }, onConfirm: (date) {
      // print('confirm $date');
      return callback(date);
    }, currentTime: DateTime.now(), locale: LocaleType.en);
  }
}
